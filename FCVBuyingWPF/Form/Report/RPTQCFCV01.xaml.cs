﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for ฑญธ๐ฉโฉฮ01.xaml
    /// </summary>
    public partial class RPTQCFCV01 : Page
    {
        public RPTQCFCV01()
        {
            InitializeComponent();
            resultDateDatePicker.SelectedDate = DateTime.Now;
        }

        public void ReloadReport()
        {
            try
            {
                if (resultDateDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("โปรดเลือกวันที่บันทึกข้อมูล",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                ReportDataSource datasource = new ReportDataSource();
                datasource.Value = Helper.BuyingHelper
                    .GetByMoistureResultDate(Convert.ToDateTime(resultDateDatePicker.SelectedDate));
                datasource.Name = "vm_Buying";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(datasource);
                ReportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTQCFCV01.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void resultDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
