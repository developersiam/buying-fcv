﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Helper;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTTR02.xaml
    /// </summary>
    public partial class RPTTR02 : Window
    {
        public RPTTR02(TransportationDocument document)
        {
            InitializeComponent();
            RenderReport(document.TransportationCode);
        }

        private void RenderReport(string transportCode)
        {
            try
            {
                var buyingList = BusinessLayerService.BuyingBL()
                    .GetByTransportationDocument(transportCode);
                var transport = new List<TransportationDocument>();
                transport.Add(BusinessLayerService.TransportationDocumentBL()
                    .GetSingle(transportCode));
                ReportDataSource transportDatasoruce = new ReportDataSource();
                ReportDataSource buyingDatasoruce = new ReportDataSource();
                transportDatasoruce.Value = transport;
                transportDatasoruce.Name = "TransportationDocumentDataSet";
                buyingDatasoruce.Value = buyingList;
                buyingDatasoruce.Name = "BuyingDataSet";
                reportViewer.Reset();
                reportViewer.LocalReport.DataSources.Add(transportDatasoruce);
                reportViewer.LocalReport.DataSources.Add(buyingDatasoruce);
                reportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTTR02.rdlc";
                reportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
