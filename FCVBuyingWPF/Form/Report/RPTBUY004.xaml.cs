﻿using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTBUY004.xaml
    /// </summary>
    public partial class RPTBUY004 : Window
    {
        BuyingDocument _document;
        string _extensionAgentCode;

        public RPTBUY004(BuyingDocument document, string extensionAgenCode)
        {
            InitializeComponent();

            _document = new BuyingDocument();
            _document = document;
            _extensionAgentCode = extensionAgenCode;

            ReloadReport();
        }

        public void ReloadReport()
        {
            try
            {
                if (_document == null)
                {
                    MessageBox.Show("ไม่พบ document ที่ต้องการประมวลผล โปรดติดต่อผู้ดูแลระบบ",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                /// Becuase the reporting service RDLC must be binding with the list of data only.
                /// 
                List<vm_BuyingDocument> buyingDocumentList = new List<vm_BuyingDocument>();
                buyingDocumentList.Add(Helper.BuyingDocumentHelper
                    .GetSingle(_document.Crop,
                    _document.FarmerCode,
                    _document.StationCode,
                    _document.DocumentNumber));

                List<vm_Buying> buyingList = new List<vm_Buying>();
                buyingList = Helper.BuyingHelper
                    .GetByDocument(_document.Crop,
                    _document.FarmerCode,
                    _document.StationCode,
                    _document.DocumentNumber);

                List<vm_CurerSummary> curerSummaryList = new List<vm_CurerSummary>();
                curerSummaryList = Helper.CurerSummaryHelper
                    .GetByCurer(user_setting.Crop.Crop1, _extensionAgentCode);

                ReportDataSource datasource1 = new ReportDataSource();
                datasource1.Value = buyingDocumentList;
                datasource1.Name = "vm_BuyingDocument";

                ReportDataSource datasource2 = new ReportDataSource();
                datasource2.Value = buyingList;
                datasource2.Name = "vm_Buying";

                ReportDataSource datasource3 = new ReportDataSource();
                datasource3.Value = curerSummaryList;
                datasource3.Name = "vm_CurerSummary";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(datasource1);
                ReportViewer.LocalReport.DataSources.Add(datasource2);
                ReportViewer.LocalReport.DataSources.Add(datasource3);
                ReportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTBUY004.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
