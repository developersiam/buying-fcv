﻿using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for BuyingVoucher.xaml
    /// </summary>
    public partial class RPTBUY001 : Window
    {
        BuyingDocument _docuemnt;
        public RPTBUY001(BuyingDocument model)
        {
            InitializeComponent();
            _docuemnt = new BuyingDocument();
            _docuemnt = model;
            ReloadReport();
        }

        public void ReloadReport()
        {
            try
            {
                if (_docuemnt == null)
                {
                    MessageBox.Show("ไม่พบ document ที่ต้องการประมวลผล โปรดติดต่อผู้ดูแลระบบ", 
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                /// Becuase the reporting service RDLC must be binding with the list of data only.
                /// 
                List<vm_BuyingDocument> buyingDocumetnList = new List<vm_BuyingDocument>();
                buyingDocumetnList.Add(Helper.BuyingDocumentHelper
                    .GetSingle(_docuemnt.Crop,
                    _docuemnt.FarmerCode,
                    _docuemnt.StationCode,
                    _docuemnt.DocumentNumber));

                ReportDataSource datasource1 = new ReportDataSource();
                datasource1.Value = buyingDocumetnList;
                datasource1.Name = "vm_BuyingDocument";

                ReportDataSource datasource2 = new ReportDataSource();
                datasource2.Value = Helper.BuyingHelper
                    .GetByDocument(_docuemnt.Crop,
                    _docuemnt.FarmerCode,
                    _docuemnt.StationCode,
                    _docuemnt.DocumentNumber);
                datasource2.Name = "vm_Buying";

                /// Becuase the reporting service RDLC must be binding with the list of data only.
                /// 
                List<vm_FarmerProfile> farmerProfileList = new List<vm_FarmerProfile>();
                farmerProfileList.Add(Helper.FarmerProfileHelper
                    .GetSingleByCrop(_docuemnt.Crop,
                    _docuemnt.FarmerCode));

                ReportDataSource datasource3 = new ReportDataSource();
                datasource3.Value = farmerProfileList;
                datasource3.Name = "vm_FarmerProfile";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(datasource1);
                ReportViewer.LocalReport.DataSources.Add(datasource2);
                ReportViewer.LocalReport.DataSources.Add(datasource3);
                ReportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTBUY001.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
