﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTQCFCV02.xaml
    /// </summary>
    public partial class RPTQCFCV02 : Page
    {
        public RPTQCFCV02()
        {
            InitializeComponent();
        }

        public void ReloadReport()
        {
            try
            {
                if (fromDatePicker.SelectedDate == null)
                {
                    //MessageBox.Show("โปรดเลือก from date",
                    //    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (toDatePicker.SelectedDate == null)
                {
                    //MessageBox.Show("โปรดเลือก to date",
                    //    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ReportDataSource datasource = new ReportDataSource();
                datasource.Value = Helper.BuyingHelper
                    .GetMoistureResultByDateRange(Convert.ToDateTime(fromDatePicker.SelectedDate),
                    Convert.ToDateTime(toDatePicker.SelectedDate));
                datasource.Name = "vm_Buying";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(datasource);
                ReportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTQCFCV02.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void fromDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }

        private void toDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
