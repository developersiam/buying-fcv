﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTBUY003.xaml
    /// </summary>
    public partial class RPTBUY003 : Window
    {

        public RPTBUY003()
        {
            InitializeComponent();

            List<BuyingStation> stationList = new List<BuyingStation>();

            foreach (var item in BusinessLayerService.UserAreaBL()
            .GetByUser(user_setting.User.Username))
            {
                stationList.AddRange(Helper.BuyingStationHelper
                    .GetBuyingStationByArea(item.AreaCode));
            }

            stationComboBox.ItemsSource = null;
            stationComboBox.ItemsSource = stationList.OrderBy(x => x.StationCode);

            List<ExtensionAgent> list = new List<ExtensionAgent>();

            foreach (var item in BusinessLayerService.UserAreaBL()
                .GetByUser(user_setting.User.Username))
            {
                list.AddRange(BusinessLayerService.ExtensionAgentBL()
                    .GetByArea(item.AreaCode));
            }

            extensionAgentComboBox.ItemsSource = null;
            extensionAgentComboBox.ItemsSource = list.OrderBy(x => x.ExtensionAgentCode);

            createDateDatePicker.SelectedDate = DateTime.Now;

            ReloadReport();
        }

        private void ReloadReport()
        {
            try
            {
                if (createDateDatePicker.SelectedDate == null)
                    return;

                if (extensionAgentComboBox.SelectedIndex < 0)
                    return;

                if (stationComboBox.SelectedIndex < 0)
                    return;


                /// Becuase the reporting service RDLC must be binding with the list of data only.
                /// 

                List<vm_Buying> buyingList = new List<vm_Buying>();
                buyingList = Helper.BuyingHelper
                    .GetByDate(Convert.ToDateTime(createDateDatePicker.SelectedDate),
                    stationComboBox.SelectedValue.ToString(),
                    extensionAgentComboBox.SelectedValue.ToString());

                ReportDataSource datasource = new ReportDataSource();
                datasource.Value = buyingList;
                datasource.Name = "vm_Buying";

                ReportViewer.Reset();
                ReportViewer.LocalReport.DataSources.Add(datasource);
                ReportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTBUY003.rdlc";
                ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }

        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }

        private void buyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadReport();
        }
    }
}
