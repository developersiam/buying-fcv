﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Report
{
    /// <summary>
    /// Interaction logic for RPTTR01.xaml
    /// </summary>
    public partial class RPTTR01 : Window
    {
        public RPTTR01(TransportationDocument transportationDocument)
        {
            try
            {
                InitializeComponent();

                List<vm_TransportationDocument> list = new List<vm_TransportationDocument>();
                list.Add(Helper.TransportationHelper.GetSingle(transportationDocument.TransportationCode));

                ReportDataSource reportDataSource1 = new ReportDataSource();
                reportDataSource1.Value = list;
                reportDataSource1.Name = "vm_TransportationDocument";

                var list1 = Helper.BuyingHelper.GetByTransportationCode(transportationDocument.TransportationCode);
                var extensionAgentCode = BusinessLayerService.FarmerBL().GetSingle(list1.FirstOrDefault().FarmerCode).ExtensionAgentCode;
                var list2 = BusinessLayerService.FarmerQuotaBL().GetByExtensionAgent(user_setting.Crop.Crop1, extensionAgentCode);

                var resultList = (from a in list1
                                  from b in list2
                                  where a.FarmerCode == b.FarmerCode
                                  select new vm_Buying
                                  {
                                      Crop = a.Crop,
                                      FarmerCode = a.FarmerCode,
                                      StationCode = a.StationCode,
                                      DocumentNumber = a.DocumentNumber,
                                      BaleBarcode = a.BaleBarcode,
                                      BaleTypeName = a.BaleTypeName,
                                      BaleTypeID = a.BaleTypeID,
                                      BaleNumber = a.BaleNumber,
                                      Weight = a.Weight,
                                      WeightDate = a.WeightDate,
                                      WeightUser = a.WeightUser,
                                      WeightModifiedDate = a.WeightModifiedDate,
                                      WeightModifiedUser = a.WeightModifiedUser,
                                      Grade = a.Grade,
                                      GradeDate = a.GradeDate,
                                      GradeGroupCode = a.GradeGroupCode,
                                      GradeModifiedDate = a.GradeModifiedDate,
                                      GradeModifiedUser = a.GradeModifiedUser,
                                      GradeUser = a.GradeUser,
                                      LoadBaleToTruckDate = a.LoadBaleToTruckDate,
                                      LoadBaleToTruckUser = a.LoadBaleToTruckUser,
                                      FarmerName = b.Farmer.Person.FirstName + " " + b.Farmer.Person.LastName,
                                      GeneralCode = b.GeneralCode
                                      //RejectDate = a.RejectDate,
                                      //RejectType = a.RejectType,
                                      //RejectTypeID = a.RejectTypeID,
                                      //RejectTypeName = a.RejectTypeID == null ? null : a.RejectType.RejectTypeName,
                                      //Price = a.Grade == null ? 0 : a.Weight * a.BuyingGrade.UnitPrice,
                                      //UnitPrice = a.Grade == null ? 0 : 
                                      //a.FarmerCode.Substring(0, 2) == "CR" || a.FarmerCode.Substring(0, 2) == "PR" ?
                                      //a.BuyingGrade.UnitPrice1 : a.BuyingGrade.UnitPrice,
                                      //Quality = a.Grade == null ? null : a.BuyingGrade.Quality
                                  }).ToList();

                ReportDataSource reportDataSource2 = new ReportDataSource();
                reportDataSource2.Value = resultList; //Helper.BuyingHelper.GetByTransportationCode(transportationDocument.TransportationCode);
                reportDataSource2.Name = "vm_Buying";

                reportViewer.Reset();
                reportViewer.LocalReport.DataSources.Add(reportDataSource1);
                reportViewer.LocalReport.DataSources.Add(reportDataSource2);
                reportViewer.LocalReport.ReportEmbeddedResource = "FCVBuyingWPF.Form.Report.RDLC.RPTTR01.rdlc";
                reportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
