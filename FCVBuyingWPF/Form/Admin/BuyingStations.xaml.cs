﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for BuyingStations.xaml
    /// </summary>
    public partial class BuyingStations : Page
    {
        BuyingStation _station;

        public BuyingStations()
        {
            InitializeComponent();
            _station = new BuyingStation();

            areaComboBox.ItemsSource = null;
            areaComboBox.ItemsSource = BusinessLayerService.AreaBL()
                .GetAll().OrderBy(x => x.AreaCode);

            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                stationCodeTextBox.Text = "";
                stationNameTextBox.Text = "";
                stationCodeTextBox.Focus();
                stationCodeTextBox.IsEnabled = true;
                editButton.IsEnabled = false;
                addButton.IsEnabled = true;
                areaComboBox.IsEnabled = true;

                BindingBuyingStationDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingBuyingStationDataGrid()
        {
            if (areaComboBox.SelectedIndex < 0)
                return;

            stationDataGrid.ItemsSource = null;
            stationDataGrid.ItemsSource = BusinessLayerService.BuyingStationBL()
                .GetAll()
                .Where(x => x.AreaCode == areaComboBox.SelectedValue.ToString())
                .OrderBy(x => x.StationCode);
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (stationDataGrid.SelectedIndex < 0)
                    return;

                var model = (BuyingStation)stationDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingStationBL().Delete(model.StationCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (stationDataGrid.SelectedIndex < 0)
                    return;

                _station = (BuyingStation)stationDataGrid.SelectedItem;

                stationCodeTextBox.Text = _station.StationCode;
                stationCodeTextBox.IsEnabled = false;
                stationNameTextBox.Text = _station.StationName;
                stationNameTextBox.Focus();
                areaComboBox.IsEnabled = false;
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (stationCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก staion code", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationCodeTextBox.Focus();
                    return;
                }
                if (stationNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก staion name", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationNameTextBox.Focus();
                    return;
                }

                if (areaComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Area", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    areaComboBox.Focus();
                    return;
                }

                var list = BusinessLayerService.BuyingStationBL().GetAll();

                if (list.Where(x => x.StationName == stationNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("staion name นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationNameTextBox.Focus();
                    return;
                }
                if (list.Where(x => x.StationCode == stationCodeTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("staion code นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationCodeTextBox.Focus();
                    return;
                }

                BusinessLayerService.BuyingStationBL().Add(new BuyingStation
                {
                    StationCode = stationCodeTextBox.Text,
                    StationName = stationNameTextBox.Text,
                    AreaCode = areaComboBox.SelectedValue.ToString(),
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _station.StationName = stationNameTextBox.Text;
                _station.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingStationBL().Edit(_station);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void areaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindingBuyingStationDataGrid();
        }
    }
}
