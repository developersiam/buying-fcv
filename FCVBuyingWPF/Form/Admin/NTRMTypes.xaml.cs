﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for NTRMTypes.xaml
    /// </summary>
    /// 
    public struct NTRMCategory
    {
        public string Category { get; set; }
    }
    public partial class NTRMTypes : Page
    {
        NTRMType _ntrmType;

        public NTRMTypes()
        {
            InitializeComponent();
            _ntrmType = new NTRMType();
            ClearForm();

            categoryComboBox.ItemsSource = null;
            categoryComboBox.ItemsSource = new List<NTRMCategory>
            {
                new NTRMCategory { Category = "Cat1A" },
                new NTRMCategory { Category = "Cat1B" },
                new NTRMCategory { Category = "Lights" },
                new NTRMCategory { Category = "Heavy" },
            };
        }

        private void ClearForm()
        {
            try
            {
                ntrmTypeNameTextBox.Text = "";
                ntrmTypeNameTextBox.Focus();
                ntrmTypeCodeTextBox.Text = "";
                ntrmTypeCodeTextBox.IsEnabled = true;
                editButton.IsEnabled = false;
                addButton.IsEnabled = true;

                BindingNTRMTypeDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingNTRMTypeDataGrid()
        {
            ntrmTypeDataGrid.ItemsSource = null;
            ntrmTypeDataGrid.ItemsSource = BusinessLayerService.NTRMTypeBL()
                .GetNtrmTypes()
                .OrderBy(x => x.NTRMTypeCode);
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (categoryComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก ntrm category", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    categoryComboBox.Focus();
                    return;
                }
                if (ntrmTypeNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก ntrm type", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ntrmTypeNameTextBox.Focus();
                    return;
                }

                var list = BusinessLayerService.NTRMTypeBL().GetNtrmTypes();

                if (list.Where(x => x.NTRMTypeName == ntrmTypeNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("ntrm type นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ntrmTypeNameTextBox.Focus();
                    return;
                }

                BusinessLayerService.NTRMTypeBL().Add(new NTRMType
                {
                    NTRMTypeCode = ntrmTypeCodeTextBox.Text,
                    NTRMCategory = categoryComboBox.SelectedValue.ToString(),
                    NTRMTypeName = ntrmTypeNameTextBox.Text,
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _ntrmType.NTRMCategory = categoryComboBox.SelectedValue.ToString();
                _ntrmType.NTRMTypeName = ntrmTypeNameTextBox.Text;
                _ntrmType.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.NTRMTypeBL().Edit(_ntrmType);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ntrmTypeDataGrid.SelectedIndex < 0)
                    return;

                _ntrmType = (NTRMType)ntrmTypeDataGrid.SelectedItem;

                categoryComboBox.SelectedValue = _ntrmType.NTRMCategory;
                ntrmTypeCodeTextBox.Text = _ntrmType.NTRMTypeCode;
                ntrmTypeNameTextBox.Text = _ntrmType.NTRMTypeName;
                ntrmTypeCodeTextBox.IsEnabled = false;
                ntrmTypeNameTextBox.Focus();
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ntrmTypeDataGrid.SelectedIndex < 0)
                    return;

                var model = (NTRMType)ntrmTypeDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.NTRMTypeBL().Delete(model);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
