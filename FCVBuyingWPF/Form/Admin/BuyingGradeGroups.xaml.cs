﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for BuyingGradeGroups.xaml
    /// </summary>
    public partial class BuyingGradeGroups : Page
    {
        BuyingGradeGroup _gradeGroup;
        public BuyingGradeGroups()
        {
            try
            {
                InitializeComponent();
                _gradeGroup = new BuyingGradeGroup();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                defaultCropTextBox.Text = user_setting.Crop.Crop1.ToString();

                var list = BusinessLayerService.BuyingGradeGroupBL()
                    .GetByCrop(user_setting.Crop.Crop1);

                if (list.Count <= 0)
                    gradeGroupCodeTextBox.Text = user_setting.Crop.Crop1 + "-1";
                else
                    gradeGroupCodeTextBox.Text = user_setting.Crop.Crop1 + "-" + (list.Count + 1);

                gradeGroupCodeTextBox.IsReadOnly = true;
                assignDateDatePicker.SelectedDate = DateTime.Now;

                BindingGradeGroupDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void BindingGradeGroupDataGrid()
        {
            try
            {
                gradeGroupDataGrid.ItemsSource = null;
                gradeGroupDataGrid.ItemsSource = BusinessLayerService.BuyingGradeGroupBL()
                    .GetByCrop(user_setting.Crop.Crop1)
                    .OrderByDescending(x => x.Crop);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BusinessLayerService.BuyingGradeGroupBL()
                    .Add(Convert.ToDateTime(assignDateDatePicker.SelectedDate),
                    Convert.ToBoolean(defaultStatusCheckBox.IsChecked),
                    user_setting.User.Username);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                if (defaultStatusCheckBox.IsChecked == true)
                    BusinessLayerService.BuyingGradeGroupBL()
                        .SetDefaultStatus(_gradeGroup.GradeGroupCode,
                        user_setting.User.Username);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gradeGroupDataGrid.SelectedIndex < 0)
                    return;

                _gradeGroup = (BuyingGradeGroup)gradeGroupDataGrid.SelectedItem;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingGradeGroupBL()
                    .SetDefaultStatus(_gradeGroup.GradeGroupCode,
                    user_setting.User.Username);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gradeGroupDataGrid.SelectedIndex < 0)
                    return;

                var model = (BuyingGradeGroup)gradeGroupDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingGradeGroupBL().Delete(model.GradeGroupCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingGradeDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gradeGroupDataGrid.SelectedIndex < 0)
                    return;

                var model = (BuyingGradeGroup)gradeGroupDataGrid.SelectedItem;

                BuyingGrades window = new BuyingGrades(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
