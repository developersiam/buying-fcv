﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : Page
    {
        public Users()
        {
            InitializeComponent();
            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                usernameTextBox.Text = "";
                passwordTextBox.Password = "";
                usernameTextBox.Focus();

                BindingUserDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingUserDataGrid()
        {
            try
            {
                userDataGrid.ItemsSource = null;
                userDataGrid.ItemsSource = BusinessLayerService.UserBL()
                    .GetAll().OrderBy(x => x.Username);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(usernameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก username", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    usernameTextBox.Focus();
                    return;
                }

                if (passwordTextBox.Password == "")
                {
                    MessageBox.Show("โปรดกรอก password", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    passwordTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.UserBL().GetById(usernameTextBox.Text) != null)
                {
                    MessageBox.Show("username นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    usernameTextBox.Focus();
                    return;
                }

                var passEncode = Helper.EncodeAndDecode.Base64Encode(passwordTextBox.Password);

                BusinessLayerService.UserBL().Add(new User
                {
                    Username = usernameTextBox.Text,
                    Password = passEncode,
                    CreateUser = "Admin"
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (userDataGrid.SelectedIndex < 0)
                    return;

                var model = (User)userDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if(BusinessLayerService.UserRoleBL().GetByUser(model.Username).Count > 0)
                {
                    MessageBox.Show("มีข้อมูล role เชื่อมโยงกับ username นี้อยู่ ไม่สามารถลบได้", 
                        "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BusinessLayerService.UserBL().Delete(model);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void manageRoleDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (userDataGrid.SelectedIndex < 0)
                    return;

                var model = (User)userDataGrid.SelectedItem;
                UserRoles window = new UserRoles(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void manageUnderExtensionAgentDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (userDataGrid.SelectedIndex < 0)
                    return;

                var model = (User)userDataGrid.SelectedItem;
                UserUnderExtensionAgents window = new UserUnderExtensionAgents(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void manageUserAreaDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (userDataGrid.SelectedIndex < 0)
                    return;

                var model = (User)userDataGrid.SelectedItem;
                UserAreas window = new UserAreas(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
