﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for FarmerQuotas.xaml
    /// </summary>
    public partial class FarmerQuotas : Window
    {
        string _farmerCode;
        short _crop;
        FarmerQuota _farmerQuota;
        public FarmerQuotas(short crop, string farmerCode)
        {
            InitializeComponent();
            _farmerQuota = new FarmerQuota();

            _crop = crop;
            _farmerCode = farmerCode;
            farmerCodeTextBox.Text = _farmerCode;
            contractCodeTextBox.Text = _crop + "-" + _farmerCode;
            registerDateDatePicker.SelectedDate = DateTime.Now.Date;
            Clear();
            quotaTextBox.Focus();
        }
        private void ClearForm()
        {
            Clear();
        }
        private void FarmerQuotaBinding()
        {
            try
            {
                _farmerQuota = BusinessLayerService.FarmerQuotaBL()
                    .GetSingle(_crop, _farmerCode);

                if (_farmerQuota == null)
                {
                    editButton.IsEnabled = false;
                    addButton.IsEnabled = true;
                    deleteButton.IsEnabled = false;
                    return;
                }

                farmerCodeTextBox.Text = _farmerQuota.FarmerCode;
                generalCodeTextBox.Text = _farmerQuota.GeneralCode;
                contractCodeTextBox.Text = _farmerQuota.ContractCode;
                quotaTextBox.Text = _farmerQuota.Quota.ToString();
                registerDateDatePicker.SelectedDate = _farmerQuota.RegisterDate;

                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
                deleteButton.IsEnabled = true;

                contractCodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_farmerQuota == null)
                    return;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.FarmerQuotaBL()
                    .Delete(_farmerQuota.Crop, _farmerQuota.FarmerCode);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (contractCodeTextBox.Text != "" &&
                    Helper.RegularExpressionHelper.
                    IsContractCodeCorrectFormat(contractCodeTextBox.Text) == false)
                {
                    MessageBox.Show("เลขที่สัญญาจะต้องเป็นตัวเลข หรือ เครื่องหมายขีดกลาง หรือ ตัวอักษรภาษาอังกฤษ เท่านั้น", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    contractCodeTextBox.Focus();
                    return;
                }
                if (quotaTextBox.Text != "" &&
                    Helper.RegularExpressionHelper.
                    IsNumericCharacter(quotaTextBox.Text) == false)
                {
                    MessageBox.Show("quota จะต้องเป็นตัวเลขเท่านั้น", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }
                if (contractCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก Contract Code", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    contractCodeTextBox.Focus();
                    return;
                }
                if (quotaTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก quota", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }
                if (Convert.ToInt32(quotaTextBox.Text) < 1)
                {
                    MessageBox.Show("quota จะต้องมากกว่า 0", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.FarmerQuotaBL()
                    .GetByCrop(_crop)
                    .Where(x => x.ContractCode == contractCodeTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("เลขที่สัญญานี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    contractCodeTextBox.Focus();
                    return;
                }

                BusinessLayerService.FarmerQuotaBL().Add(new FarmerQuota
                {
                    Crop = _crop,
                    FarmerCode = _farmerCode,
                    GeneralCode = generalCodeTextBox.Text,
                    Quota = Convert.ToInt32(quotaTextBox.Text),
                    ContractCode = contractCodeTextBox.Text,
                    RegisterDate = Convert.ToDateTime(registerDateDatePicker.SelectedDate),
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = user_setting.User.Username,
                    RegisterUser = user_setting.User.Username
                });

                ClearForm();

                MessageBox.Show("บันทึกสำเร็จ", "Successfully", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (contractCodeTextBox.Text != "" &&
                    Helper.RegularExpressionHelper.
                    IsContractCodeCorrectFormat(contractCodeTextBox.Text) == false)
                {
                    MessageBox.Show("เลขที่สัญญาจะต้องเป็นตัวเลข หรือ เครื่องหมายขีดกลาง หรือ ตัวอักษรภาษาอังกฤษ เท่านั้น", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    contractCodeTextBox.Focus();
                    return;
                }
                if (quotaTextBox.Text != "" &&
                    Helper.RegularExpressionHelper.
                    IsNumericCharacter(quotaTextBox.Text) == false)
                {
                    MessageBox.Show("quota จะต้องเป็นตัวเลขเท่านั้น", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }

                if (contractCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก Contract Code", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    contractCodeTextBox.Focus();
                    return;
                }
                if (quotaTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก quota", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }
                if (Convert.ToInt32(quotaTextBox.Text) < 1)
                {
                    MessageBox.Show("quota จะต้องมากกว่า 0", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    quotaTextBox.Focus();
                    return;
                }

                _farmerQuota.GeneralCode = generalCodeTextBox.Text;
                _farmerQuota.ContractCode = contractCodeTextBox.Text;
                _farmerQuota.Quota = Convert.ToInt32(quotaTextBox.Text);
                _farmerQuota.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.FarmerQuotaBL().Edit(_farmerQuota);
                ClearForm();

                MessageBox.Show("บันทึกสำเร็จ", "Successfully", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }
        private void Clear()
        {
            try
            {
                contractCodeTextBox.Text = _crop + "-" + _farmerCode;
                farmerCodeTextBox.Text = _farmerQuota == null ? "" : _farmerCode;
                generalCodeTextBox.Text = _farmerQuota == null ? "" : _farmerQuota.GeneralCode;
                contractCodeTextBox.Focus();
                quotaTextBox.Clear();
                registerDateDatePicker.SelectedDate = DateTime.Now;
                addButton.IsEnabled = true;
                editButton.IsEnabled = false;
                FarmerQuotaBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
    }
}
