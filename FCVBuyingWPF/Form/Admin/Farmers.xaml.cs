﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for Farmers.xaml
    /// </summary>
    public partial class Farmers : Page
    {
        public Farmers()
        {
            InitializeComponent();

            cropTextBox.Text = user_setting.Crop.Crop1.ToString();

            areaComboBox.ItemsSource = null;
            areaComboBox.ItemsSource = BusinessLayerService.AreaBL().GetAll().OrderBy(x => x.AreaCode);
        }

        private void extensionAgentBinding()
        {
            try
            {
                if (areaComboBox.SelectedIndex < 0)
                    return;

                extensionAgentComboBox.ItemsSource = null;
                extensionAgentComboBox.ItemsSource = BusinessLayerService.ExtensionAgentBL()
                    .GetByArea(areaComboBox.SelectedValue.ToString())
                    .OrderBy(x => x.ExtensionAgentCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerDataGridBinding()
        {
            try
            {
                if (areaComboBox.SelectedIndex < 0)
                    return;

                if (extensionAgentComboBox.SelectedIndex < 0)
                    return;

                var farmerList = Helper.FarmerProfileHelper
                    .GetByExtensionAgent(user_setting.Crop.Crop1,
                    extensionAgentComboBox.SelectedValue.ToString());

                farmerDataGrid.ItemsSource = null;
                farmerDataGrid.ItemsSource = farmerList;
                totalRecordTextBox.Text = farmerList.Count.ToString("N0");
                totalQuotaTextBox.Text = farmerList.Sum(x=>x.Quota).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (areaComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุ Area", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (extensionAgentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุตัวแทน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                AddFarmer window = new AddFarmer(user_setting.Crop.Crop1, extensionAgentComboBox.SelectedValue.ToString());
                window.ShowDialog();
                farmerDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            farmerDataGridBinding();
        }

        private void farmerQuotaGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (farmerDataGrid.SelectedIndex < 0)
                    return;

                var model = (vm_FarmerProfile)farmerDataGrid.SelectedItem;

                FarmerQuotas window = new FarmerQuotas(model.Crop, model.FarmerCode);
                window.ShowDialog();

                farmerDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void areaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            extensionAgentBinding();
            farmerDataGridBinding();
        }

        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            farmerDataGridBinding();
        }
    }
}
