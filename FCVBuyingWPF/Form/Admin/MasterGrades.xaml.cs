﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for MasterGrades.xaml
    /// </summary>
    public partial class MasterGrades : Window
    {
        public MasterGrades()
        {
            try
            {
                InitializeComponent();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void DataGridBinding()
        {
            try
            {
                masterGradeDataGrid.ItemsSource = null;
                masterGradeDataGrid.ItemsSource = BusinessLayerService.MasterGradeBL()
                    .GetMasterGrades()
                    .OrderBy(x => x.Grade);

                totalRecordTextBox.Text = masterGradeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearForm()
        {
            gradeNameTextBox.Clear();
            gradeNameTextBox.Focus();
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (masterGradeDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var model = (MasterGrade)masterGradeDataGrid.SelectedItem;
                BusinessLayerService.MasterGradeBL().Delete(model.Grade);

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (gradeNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกชื่อเกรด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BusinessLayerService.MasterGradeBL().Add(new MasterGrade
                {
                    Grade = gradeNameTextBox.Text,
                    CreateDate = DateTime.Now,
                    CreateUser = user_setting.User.Username,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = user_setting.User.Username
                });

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
