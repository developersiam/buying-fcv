﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for BuyingGrades.xaml
    /// </summary>
    /// 

    public struct GradeQuality
    {
        public string Quality { get; set; }
    }

    public partial class BuyingGrades : Window
    {
        BuyingGradeGroup _gradeGroup;
        BuyingGrade _grade;
        public BuyingGrades(BuyingGradeGroup gradeGroupCode)
        {
            InitializeComponent();

            _gradeGroup = new BuyingGradeGroup();
            _grade = new BuyingGrade();

            _gradeGroup = gradeGroupCode;

            qualityComboBox.ItemsSource = null;
            qualityComboBox.ItemsSource = new List<GradeQuality> {
                new GradeQuality { Quality = "H"},
                new GradeQuality { Quality = "M"},
                new GradeQuality { Quality = "L"},
            };
            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                gradeComboBox.ItemsSource = null;
                gradeComboBox.ItemsSource = BusinessLayerService.MasterGradeBL()
                    .GetMasterGrades()
                    .OrderBy(x => x.Grade);

                gradeGroupCodeTextBox.Text = _gradeGroup.GradeGroupCode;
                unitPriceTextBox.Clear();
                unitPriceNPITextBox.Clear();
                addButton.IsEnabled = true;
                editButton.IsEnabled = false;
                gradeComboBox.IsEnabled = true;

                BindingGradeDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingGradeDataGrid()
        {
            try
            {
                buyingGradeDataGrid.ItemsSource = null;
                buyingGradeDataGrid.ItemsSource = BusinessLayerService.BuyingGradeBL()
                    .GetByGradeGroup(_gradeGroup.GradeGroupCode)
                    .OrderBy(x => x.Grade);

                totalRecordTextBox.Text = buyingGradeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (qualityComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุคุณภาพเกรด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (gradeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือกเกรด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (unitPriceTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุราคา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                };

                BusinessLayerService.BuyingGradeBL().Add(new BuyingGrade
                {
                    GradeGroupCode = _gradeGroup.GradeGroupCode,
                    Grade = gradeComboBox.SelectedValue.ToString(),
                    Quality = qualityComboBox.SelectedValue.ToString(),
                    UnitPrice = Convert.ToDecimal(unitPriceTextBox.Text),
                    UnitPrice1 = Convert.ToDecimal(unitPriceNPITextBox.Text),
                    ActiveStatus = Convert.ToBoolean(activeStatusCheckBox.IsChecked),
                    CreateUser = user_setting.User.Username,
                    CreateDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _grade.Quality = qualityComboBox.SelectedValue.ToString();
                _grade.UnitPrice = Convert.ToDecimal(unitPriceTextBox.Text);
                _grade.UnitPrice1 = Convert.ToDecimal(unitPriceNPITextBox.Text);
                _grade.ModifiedUser = user_setting.User.Username;
                _grade.ModifiedDate = DateTime.Now;

                BusinessLayerService.BuyingGradeBL().Edit(_grade);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingGradeDataGrid.SelectedIndex < 0)
                    return;

                _grade = (BuyingGrade)buyingGradeDataGrid.SelectedItem;

                gradeComboBox.SelectedValue = _grade.Grade;
                qualityComboBox.SelectedValue = _grade.Quality;
                activeStatusCheckBox.IsChecked = _grade.ActiveStatus;
                unitPriceTextBox.Text = _grade.UnitPrice.ToString("N2");
                unitPriceNPITextBox.Text = _grade.UnitPrice1.ToString("N2");
                unitPriceTextBox.Focus();

                gradeComboBox.IsEnabled = false;
                addButton.IsEnabled = false;
                editButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingGradeDataGrid.SelectedIndex < 0)
                    return;

                _grade = (BuyingGrade)buyingGradeDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้หรือไม่?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingGradeBL()
                    .Delete(_grade.GradeGroupCode, _grade.Grade);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addMasterGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MasterGrades window = new MasterGrades();
                window.ShowDialog();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
