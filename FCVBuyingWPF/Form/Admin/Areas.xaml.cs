﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for Areas.xaml
    /// </summary>
    public partial class Areas : Page
    {
        Area _area;
        public Areas()
        {
            InitializeComponent();
            _area = new Area();
            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                areaCodeTextBox.Text = "";
                areaNameTextBox.Text = "";
                areaCodeTextBox.Focus();

                addButton.IsEnabled = true;
                editButton.IsEnabled = false;
                areaCodeTextBox.IsEnabled = true;

                BindingAreaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingAreaDataGrid()
        {
            try
            {
                areaDataGrid.ItemsSource = null;
                areaDataGrid.ItemsSource = BusinessLayerService.AreaBL()
                    .GetAll().OrderBy(x => x.AreaCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (areaCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก area code", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    areaCodeTextBox.Focus();
                    return;
                }

                if (areaNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก area name", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    areaNameTextBox.Focus();
                    return;
                }

                BusinessLayerService.AreaBL().Add(new Area
                {
                    AreaCode = areaCodeTextBox.Text,
                    AreaName =  areaNameTextBox.Text,
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _area.AreaName = areaNameTextBox.Text;
                _area.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.AreaBL().Edit(_area);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (areaDataGrid.SelectedIndex < 0)
                    return;

                _area = (Area)areaDataGrid.SelectedItem;

                areaCodeTextBox.Text = _area.AreaCode;
                areaNameTextBox.Text = _area.AreaName;
                
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
                areaCodeTextBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (areaDataGrid.SelectedIndex < 0)
                    return;

                var model = (Area)areaDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.AreaBL().Delete(model);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
