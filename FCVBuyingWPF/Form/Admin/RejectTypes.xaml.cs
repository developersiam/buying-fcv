﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for RejectTypes.xaml
    /// </summary>
    public partial class RejectTypes : Page
    {
        RejectType _rejectType;
        public RejectTypes()
        {
            InitializeComponent();
            _rejectType = new RejectType();
            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                rejectNameTextBox.Text = "";
                rejectNameTextBox.Focus();
                editButton.IsEnabled = false;
                addButton.IsEnabled = true;

                BindingRejectTypeDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingRejectTypeDataGrid()
        {
            rejectTypeDataGrid.ItemsSource = null;
            rejectTypeDataGrid.ItemsSource = BusinessLayerService.RejectTypeBL()
                .GetRejectTypes()
                .OrderBy(x => x.RejectTypeName);
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rejectTypeDataGrid.SelectedIndex < 0)
                    return;

                var model = (RejectType)rejectTypeDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.RejectTypeBL().Delete(model.RejectTypeID);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _rejectType.RejectTypeName = rejectNameTextBox.Text;
                _rejectType.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.RejectTypeBL().Edit(_rejectType);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                if (rejectNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก reject type", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    rejectNameTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.NTRMTypeBL().GetNtrmTypes().Where(x => x.NTRMTypeName == rejectNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("ntrm type นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    rejectNameTextBox.Focus();
                    return;
                }

                BusinessLayerService.RejectTypeBL().Add(new RejectType
                {
                    RejectTypeID = Guid.NewGuid(),
                    RejectTypeName = rejectNameTextBox.Text,
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rejectTypeDataGrid.SelectedIndex < 0)
                    return;

                _rejectType = (RejectType)rejectTypeDataGrid.SelectedItem;

                rejectNameTextBox.Text = _rejectType.RejectTypeName;
                rejectNameTextBox.Focus();
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
