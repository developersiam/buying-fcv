﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using PCSC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ThaiNationalIDCard;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for AddFarmer.xaml
    /// </summary>
    public partial class AddFarmer : Window
    {
        Personal _personal;
        string _extensionAgentCode;
        short _crop;
        public static Farmer _farmer;

        public AddFarmer(short crop, string extensionAgentCode)
        {
            InitializeComponent();

            _personal = new Personal();
            _farmer = new Farmer();

            _crop = crop;
            _extensionAgentCode = extensionAgentCode;
            farmerProfileGrid.Visibility = Visibility.Collapsed;
        }

        private void FarmerProfileDataBinding(Farmer model)
        {
            try
            {
                citizenIDTextBox.Text = model.Person.CitizenID;
                prefixNameTextBox.Text = model.Person.Prefix;
                firstNameTextBox.Text = model.Person.FirstName;
                lastNameTextBox.Text = model.Person.LastName;
                birthDateDatePicker.SelectedDate = model.Person.BirthDate;
                mooTextBox.Text = model.Person.Village;
                homeNumberTextBox.Text = model.Person.HouseNumber;
                subDistrictTextBox.Text = model.Person.Tumbon;
                districtTextBox.Text = model.Person.Amphur;
                provinceTextBox.Text = model.Person.Province;
                phoneNumberTextBox.Text = model.Person.TelephoneNumber;

                farmerCodeTextBox.Text = model.FarmerCode;
                registerDateTextBox.Text = model.CreateDate.ToShortDateString();
                extensionAgentCodeTextBox.Text = model.ExtensionAgentCode;

                farmerProfileGrid.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void readIDCardButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SCardContext cardContext = new SCardContext();
                cardContext.Establish(SCardScope.System);

                string[] scReader = cardContext.GetReaders();
                if (scReader.Length <= 0)
                {
                    throw new PCSCException(SCardError.NoReadersAvailable, "ไม่พบเครื่องอ่านบัตรสมาร์ทการ์ดที่เชื่อมต่อเข้ากับเครื่องคอมพิวเตอร์ของท่าน.");
                }

                ThaiIDCard thaiIDCard = new ThaiIDCard();
                _personal = thaiIDCard.readAllPhoto();

                if (_personal == null)
                {
                    MessageBox.Show("ไม่สามารถอ่านข้อมูลจากบัตรได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                citizenIDTextBox.Text = _personal.Citizenid;
                prefixNameTextBox.Text = _personal.Th_Prefix;
                firstNameTextBox.Text = _personal.Th_Firstname;
                lastNameTextBox.Text = _personal.Th_Lastname;
                birthDateDatePicker.Text = Convert.ToDateTime(_personal.Birthday).ToShortDateString();
                homeNumberTextBox.Text = _personal.addrHouseNo;
                mooTextBox.Text = _personal.addrVillageNo;
                subDistrictTextBox.Text = _personal.addrTambol;
                districtTextBox.Text = _personal.addrAmphur;
                provinceTextBox.Text = _personal.addrProvince;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (citizenIDTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกรหัสประจำตัวประชาชน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (prefixNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกคำนำหน้านาม", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (firstNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอชื่อ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (lastNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอนามสกุล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (birthDateDatePicker.Text == "")
                {
                    MessageBox.Show("โปรดกรอกวันเดือนปีเกิด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (homeNumberTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกบ้านเลขที่", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (mooTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรกหหมู่", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (subDistrictTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกตำบล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (districtTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกอำเภอ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (provinceTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกจังหวัด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (phoneNumberTextBox.Text != "" &&
                    Helper.RegularExpressionHelper.IsNumericCharacter(phoneNumberTextBox.Text) == false)
                {
                    MessageBox.Show("ช่องหมายเลขโทรศัพท์จะต้องประกอบด้วยตัวเลขเท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                Person _person = new Person
                {
                    CitizenID = citizenIDTextBox.Text,
                    Prefix = prefixNameTextBox.Text,
                    FirstName = firstNameTextBox.Text,
                    LastName = lastNameTextBox.Text,
                    BirthDate = Convert.ToDateTime(birthDateDatePicker.Text),
                    HouseNumber = homeNumberTextBox.Text,
                    Village = mooTextBox.Text,
                    Tumbon = subDistrictTextBox.Text,
                    Amphur = districtTextBox.Text,
                    Province = provinceTextBox.Text,
                    TelephoneNumber = phoneNumberTextBox.Text == "" ? null : phoneNumberTextBox.Text
                };

                /// ตรวจสอบข้อมูลชาวไร่ว่ามีในระบบหรือไม่ ถ้าไม่มีให้ทำการเพิ่มข้อมูลชาวไร่เข้าในระบบ (Add a new farmer)
                var model = BusinessLayerService.FarmerBL()
                    .GetByCitizenID(_person.CitizenID)
                    .SingleOrDefault(x => x.CitizenID == _person.CitizenID &&
                    x.ExtensionAgentCode == _extensionAgentCode);

                /// สร้าง object ใหม่เพื่อรอรับข้อมูลภายหลังจากบันทึกข้อมูลชาวไร่ใหม่เพื่อนำไปใช้แสดงบนหน้าจอ
                Farmer newFarmer = new Farmer();

                if (model == null)
                    newFarmer = BusinessLayerService.FarmerBL()
                         .Add(_person, _extensionAgentCode, user_setting.User.Username);
                else
                {
                    MessageBox.Show("ชาวไร่รายนี้มีประวัติอยู่ในระบบก่อนหน้านี้แล้ว สามารถบันทึกข้อมูลโควต้าได้เลย",
                           "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                farmerCodeTextBox.Text = newFarmer.FarmerCode;
                extensionAgentCodeTextBox.Text = newFarmer.ExtensionAgentCode;
                registerDateTextBox.Text = newFarmer.CreateDate.ToShortDateString();
                farmerProfileGrid.Visibility = Visibility.Visible;

                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Done", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                citizenIDTextBox.Text = "";
                prefixNameTextBox.Text = "";
                firstNameTextBox.Text = "";
                lastNameTextBox.Text = "";
                birthDateDatePicker.Text = null;
                homeNumberTextBox.Text = "";
                mooTextBox.Text = "";
                subDistrictTextBox.Text = "";
                districtTextBox.Text = "";
                provinceTextBox.Text = "";
                phoneNumberTextBox.Text = "";
                citizenIDTextBox.Focus();

                farmerProfileGrid.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void checkProfileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (citizenIDTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกรหัสประจำตัวประชาชน เพื่อใช้ในการตรวจสอบ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ///Check a farmer profile in the our system.
                var model = BusinessLayerService.FarmerBL()
                    .GetByCitizenID(citizenIDTextBox.Text);

                if (model.Count() <= 0)
                {
                    MessageBox.Show("ไม่พบข้อมูลชาวไร่รหัสประจำตัวประชาชน " + citizenIDTextBox.Text + " ในระบบ",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    citizenIDTextBox.Focus();
                    return;
                }

                var person = model.FirstOrDefault().Person;

                citizenIDTextBox.Text = person.CitizenID;
                prefixNameTextBox.Text = person.Prefix;
                firstNameTextBox.Text = person.FirstName;
                lastNameTextBox.Text = person.LastName;
                birthDateDatePicker.SelectedDate = person.BirthDate;
                mooTextBox.Text = person.Village;
                homeNumberTextBox.Text = person.HouseNumber;
                subDistrictTextBox.Text = person.Tumbon;
                districtTextBox.Text = person.Amphur;
                provinceTextBox.Text = person.Province;
                phoneNumberTextBox.Text = person.TelephoneNumber;

                var farmer = model.SingleOrDefault(x => x.ExtensionAgentCode == _extensionAgentCode);
                if (farmer != null)
                    FarmerProfileDataBinding(farmer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerQuotaButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FarmerQuotas window = new FarmerQuotas(_crop, farmerCodeTextBox.Text);
                window.ShowDialog();

                FarmerProfileDataBinding(BusinessLayerService.FarmerBL()
                    .GetSingle(farmerCodeTextBox.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            citizenIDTextBox.Clear();
            citizenIDTextBox.Focus();
        }
    }
}
