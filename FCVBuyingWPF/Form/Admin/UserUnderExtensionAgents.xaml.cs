﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for UserUnderExtensionAgents.xaml
    /// </summary>
    /// 
    public struct UserUnderExtensionAgentDataGrid
    {
        public string ExtensionAgentCode { get; set; }
        public bool AssignStatus { get; set; }
    }
    public partial class UserUnderExtensionAgents : Window
    {
        User _user;
        public UserUnderExtensionAgents(User user)
        {
            InitializeComponent();

            _user = new User();
            _user = user;

            areaComboBox.ItemsSource = null;
            areaComboBox.ItemsSource = BusinessLayerService.AreaBL().GetAll()
                .OrderBy(x => x.AreaCode);

            areaComboBox.SelectedIndex = 0;

            //BindingUserUnderExtensionAgentDataGrid();
        }

        private void BindingUserUnderExtensionAgentDataGrid()
        {
            try
            {
                if (areaComboBox.SelectedIndex < 0)
                    return;

                var list = from r in BusinessLayerService.ExtensionAgentBL().GetByArea(areaComboBox.SelectedValue.ToString())
                           join u in BusinessLayerService.UserUnderExtensionAgentBL().GetByUser(_user.Username)
                           on r.ExtensionAgentCode equals u.ExtensionAgentCode into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserUnderExtensionAgentDataGrid
                           {
                               ExtensionAgentCode = r.ExtensionAgentCode,
                               AssignStatus = rs == null ? false : true
                           };

                userUnderExtemsionAgentDataGrid.ItemsSource = null;
                userUnderExtemsionAgentDataGrid.ItemsSource = list.OrderBy(x => x.ExtensionAgentCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void userUnderExtemsionAgentDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (userUnderExtemsionAgentDataGrid.SelectedIndex <= -1)
                    return;

                var model = (UserUnderExtensionAgentDataGrid)userUnderExtemsionAgentDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerService.UserUnderExtensionAgentBL()
                        .Add(new UserUnderExtensionAgent
                    {
                        Username = _user.Username,
                        ExtensionAgentCode = model.ExtensionAgentCode,
                        CreateDate = DateTime.Now,
                        CreateUser = "Admin"
                    });
                else
                    BusinessLayerService.UserUnderExtensionAgentBL()
                        .Delete(BusinessLayerService.UserUnderExtensionAgentBL()
                        .GetById(_user.Username, model.ExtensionAgentCode));

                BindingUserUnderExtensionAgentDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void areaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BindingUserUnderExtensionAgentDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
