﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for Roles.xaml
    /// </summary>
    public partial class Roles : Page
    {
        Role _role;
        public Roles()
        {
            InitializeComponent();
            _role = new Role();
            ClearForm();
        }
        private void ClearForm()
        {
            try
            {
                roleNameTextBox.Text = "";
                roleNameTextBox.Focus();

                addButton.IsEnabled = true;
                editButton.IsEnabled = false;

                BindingRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingRoleDataGrid()
        {
            try
            {
                roleDataGrid.ItemsSource = null;
                roleDataGrid.ItemsSource = BusinessLayerService.RoleBL()
                    .GetAll().OrderBy(x => x.RoleName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (roleDataGrid.SelectedIndex < 0)
                    return;

                _role = (Role)roleDataGrid.SelectedItem;

                roleNameTextBox.Text = _role.RoleName;
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (roleNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก role name", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    roleNameTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.RoleBL()
                    .GetAll().Where(x=>x.RoleName == roleNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("Role name นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    roleNameTextBox.Focus();
                    return;
                }
                
                BusinessLayerService.RoleBL().Add(new Role
                {
                    RoleID = Guid.NewGuid(),
                    RoleName = roleNameTextBox.Text,
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (roleDataGrid.SelectedIndex < 0)
                    return;

                var model = (Role)roleDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.RoleBL().Delete(model);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _role.RoleName = roleNameTextBox.Text;
                _role.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.RoleBL().Edit(_role);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
