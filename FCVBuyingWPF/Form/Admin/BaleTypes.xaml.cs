﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for BaleTypes.xaml
    /// </summary>
    public partial class BaleTypes : Page
    {
        BaleType _baleType;

        public BaleTypes()
        {
            InitializeComponent();
            _baleType = new BaleType();
            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                baleTypeNameTextBox.Text = "";
                baleTypeNameTextBox.Focus();
                editButton.IsEnabled = false;
                addButton.IsEnabled = true;

                BindingBaleTypeDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingBaleTypeDataGrid()
        {
            baleTypeDataGrid.ItemsSource = null;
            baleTypeDataGrid.ItemsSource = BusinessLayerService.BaleTypeBL()
                .GetBaleTypes()
                .OrderBy(x => x.BaleTypeName);
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleTypeDataGrid.SelectedIndex < 0)
                    return;

                _baleType = (BaleType)baleTypeDataGrid.SelectedItem;

                baleTypeNameTextBox.Text = _baleType.BaleTypeName;
                baleTypeNameTextBox.Focus();
                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleTypeDataGrid.SelectedIndex < 0)
                    return;

                var model = (BaleType)baleTypeDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BaleTypeBL().Delete(model.BaleTypeID);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _baleType.BaleTypeName = baleTypeNameTextBox.Text;
                _baleType.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BaleTypeBL().Edit(_baleType);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleTypeNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก bale type", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleTypeNameTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.BaleTypeBL().GetBaleTypes()
                    .Where(x => x.BaleTypeName == baleTypeNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("bale type นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleTypeNameTextBox.Focus();
                    return;
                }

                BusinessLayerService.BaleTypeBL().Add(new BaleType
                {
                    BaleTypeName = baleTypeNameTextBox.Text,
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
