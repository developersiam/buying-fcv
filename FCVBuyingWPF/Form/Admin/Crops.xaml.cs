﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for Crops.xaml
    /// </summary>
    public partial class Crops : Page
    {
        Crop _crop;
        public Crops()
        {
            InitializeComponent();

            _crop = new Crop();
            ClearForm();
        }
        private void ClearForm()
        {
            try
            {
                cropTextBox.Text = "";
                cropTextBox.IsEnabled = true;
                cropTextBox.Focus();
                defaultStatusCheckBox.IsChecked = false;

                BindingUserDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingUserDataGrid()
        {
            try
            {
                cropDataGrid.ItemsSource = null;
                cropDataGrid.ItemsSource = BusinessLayerService.CropBL()
                    .GetAll()
                    .OrderByDescending(x => x.Crop1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _crop.DefaultStatus = Convert.ToBoolean(defaultStatusCheckBox.IsChecked);
                _crop.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.CropBL().Edit(_crop);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cropDataGrid.SelectedIndex < 0)
                    return;

                _crop = (Crop)cropDataGrid.SelectedItem;

                BusinessLayerService.CropBL().SetDefault(_crop.Crop1);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cropDataGrid.SelectedIndex < 0)
                    return;

                var model = (Crop)cropDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.CropBL().Delete(model.Crop1);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cropTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก crop", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cropTextBox.Focus();
                    return;
                }

                if (BusinessLayerService.CropBL().GetSingle(Convert.ToInt16(cropTextBox.Text)) != null)
                {
                    MessageBox.Show("crop นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    cropTextBox.Focus();
                    return;
                }

                BusinessLayerService.CropBL().Add(new Crop
                {
                    Crop1 = Convert.ToInt16(cropTextBox.Text),
                    DefaultStatus = Convert.ToBoolean(defaultStatusCheckBox.IsChecked),
                    CreateUser = user_setting.User.Username
                });
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
