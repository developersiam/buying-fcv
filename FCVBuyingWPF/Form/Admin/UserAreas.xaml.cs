﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for UserAreas.xaml
    /// </summary>
    /// 
    public struct UserAreaDataGrid
    {
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class UserAreas : Window
    {
        User _user;
        public UserAreas(User user)
        {
            InitializeComponent();

            _user = new User();
            _user = user;

            BindingUserUnderAreaDataGrid();
        }

        private void BindingUserUnderAreaDataGrid()
        {
            try
            {
                var list = from r in BusinessLayerService.AreaBL().GetAll()
                           join u in BusinessLayerService.UserAreaBL().GetByUser(_user.Username)
                           on r.AreaCode equals u.AreaCode into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserAreaDataGrid
                           {
                               AreaCode = r.AreaCode,
                               AreaName = r.AreaName,
                               AssignStatus = rs == null ? false : true
                           };

                userAreaDataGrid.ItemsSource = null;
                userAreaDataGrid.ItemsSource = list.OrderBy(x => x.AreaName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void userAreaDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (userAreaDataGrid.SelectedIndex <= -1)
                    return;

                var model = (UserAreaDataGrid)userAreaDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerService.UserAreaBL()
                        .Add(new UserArea
                        {
                            Username = _user.Username,
                            AreaCode = model.AreaCode,
                            CreateDate = DateTime.Now,
                            CreateUser = user_setting.User.Username
                        });
                else
                    BusinessLayerService.UserAreaBL()
                        .Delete(BusinessLayerService.UserAreaBL()
                        .GetById(_user.Username, model.AreaCode));

                BindingUserUnderAreaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
