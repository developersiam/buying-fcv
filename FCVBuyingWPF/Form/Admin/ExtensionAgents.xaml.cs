﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for ExtensionAgents.xaml
    /// </summary>
    public partial class ExtensionAgents : Page
    {
        ExtensionAgent _agent;
        public ExtensionAgents()
        {
            InitializeComponent();

            _agent = new ExtensionAgent();

            areaComboBox.ItemsSource = null;
            areaComboBox.ItemsSource = BusinessLayerService.AreaBL()
                .GetAll().OrderBy(x => x.AreaCode);

            ClearForm();
        }

        private void ClearForm()
        {
            try
            {
                extensionAgentCodeTextBox.Text = "";
                extensionAgentCodeTextBox.Focus();
                extensionAgentNameTextBox.Text = "";

                addButton.IsEnabled = true;
                editButton.IsEnabled = false;
                extensionAgentCodeTextBox.IsEnabled = true;
                farmerCodePrefixTextBox.IsEnabled = true;
                areaComboBox.IsEnabled = true;

                BindingDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindingDataGrid()
        {
            try
            {
                if (areaComboBox.SelectedIndex < 0)
                    return;

                extensionAgentDataGrid.ItemsSource = null;
                extensionAgentDataGrid.ItemsSource = BusinessLayerService.ExtensionAgentBL()
                    .GetByArea(areaComboBox.SelectedValue.ToString())
                    .OrderBy(x => x.ExtensionAgentCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _agent.ExtensionAgentName = extensionAgentNameTextBox.Text;
                _agent.ModifiedUser = user_setting.User.Username;

                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                _agent.ModifiedUser = user_setting.User.Username;

                BusinessLayerService.ExtensionAgentBL()
                    .Edit(_agent.ExtensionAgentCode,
                    extensionAgentNameTextBox.Text,
                    user_setting.User.Username);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (extensionAgentDataGrid.SelectedIndex < 0)
                    return;

                _agent = (ExtensionAgent)extensionAgentDataGrid.SelectedItem;

                extensionAgentCodeTextBox.Text = _agent.ExtensionAgentCode;
                extensionAgentNameTextBox.Text = _agent.ExtensionAgentName;
                areaComboBox.SelectedValue = _agent.AreaCode;
                farmerCodePrefixTextBox.Text = _agent.Prefix;

                editButton.IsEnabled = true;
                addButton.IsEnabled = false;
                extensionAgentCodeTextBox.IsEnabled = false;
                farmerCodePrefixTextBox.IsEnabled = false;
                areaComboBox.IsEnabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (extensionAgentDataGrid.SelectedIndex < 0)
                    return;

                var model = (ExtensionAgent)extensionAgentDataGrid.SelectedItem;
                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning.",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var farmerIsUnder = BusinessLayerService.FarmerBL()
                    .GetByExtensionAgent(model.ExtensionAgentCode);

                if (farmerIsUnder.Count > 0)
                {
                    MessageBox.Show("มีจำนวนชาวไร่ในสังกัดอยู่ " + farmerIsUnder.Count + " ราย ไม่สามารถลบข้อมูลนี้ได้",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BusinessLayerService.ExtensionAgentBL().Delete(model.ExtensionAgentCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (extensionAgentCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก extension agent code", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    extensionAgentCodeTextBox.Focus();
                    return;
                }

                if (extensionAgentNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก extension agent name", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    extensionAgentNameTextBox.Focus();
                    return;
                }

                if (areaComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Area", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    areaComboBox.Focus();
                    return;
                }

                var list = BusinessLayerService.ExtensionAgentBL()
                    .GetAll();

                if (list.Where(x => x.ExtensionAgentCode == extensionAgentCodeTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("extension agent code นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    extensionAgentCodeTextBox.Focus();
                    return;
                }

                if (list.Where(x => x.ExtensionAgentName == extensionAgentNameTextBox.Text).Count() > 0)
                {
                    MessageBox.Show("extension agent name นี้มีอยู่แล้วในระบบ", "warning.", MessageBoxButton.OK, MessageBoxImage.Warning);
                    extensionAgentCodeTextBox.Focus();
                    return;
                }

                BusinessLayerService.ExtensionAgentBL().Add(new ExtensionAgent
                {
                    ExtensionAgentCode = extensionAgentCodeTextBox.Text,
                    ExtensionAgentName = extensionAgentNameTextBox.Text,
                    Prefix = farmerCodePrefixTextBox.Text,
                    AreaCode = areaComboBox.SelectedValue.ToString(),
                    CreateUser = user_setting.User.Username
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void areaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BindingDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingAreaDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (extensionAgentDataGrid.SelectedIndex < 0)
                    return;

                var model = (ExtensionAgent)extensionAgentDataGrid.SelectedItem;

                ExtensionAgentBuyingAreas window = new ExtensionAgentBuyingAreas(model.ExtensionAgentCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
