﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for ExtensionAgentBuyingAreas.xaml
    /// </summary>
    public struct ExtensionAgentBuyingAreaDataGrid
    {
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class ExtensionAgentBuyingAreas : Window
    {
        string _extensionAgentCode;
        public ExtensionAgentBuyingAreas(string extensionAgentCode)
        {
            InitializeComponent();
            _extensionAgentCode = extensionAgentCode;
            

            extensionAgentComboBox.ItemsSource = null;
            extensionAgentComboBox.ItemsSource = BusinessLayerService.ExtensionAgentBL()
                .GetAll()
                .OrderBy(x => x.AreaCode);

            extensionAgentComboBox.SelectedValue = _extensionAgentCode;
        }

        private void BindingExtensionAgentBuyingAreaDataGrid()
        {
            try
            {
                if (extensionAgentComboBox.SelectedIndex < 0)
                    return;

                var list = from a in BusinessLayerService.AreaBL().GetAll()
                           join b in BusinessLayerService.ExtensionAgentBuyingAreaBL().GetByExtensionAgent(_extensionAgentCode)
                           on a.AreaCode equals b.AreaCode into ab
                           from rs in ab.DefaultIfEmpty()
                           select new ExtensionAgentBuyingAreaDataGrid
                           {
                               AreaCode = a.AreaCode,
                               AreaName = "(" + a.AreaCode + ")" + " " + a.AreaName,
                               AssignStatus = rs == null ? false : true
                           };

                extensionAgentBuyingAreaDataGrid.ItemsSource = null;
                extensionAgentBuyingAreaDataGrid.ItemsSource = list.OrderBy(x => x.AreaName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                BindingExtensionAgentBuyingAreaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void extensionAgentBuyingAreaDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (extensionAgentBuyingAreaDataGrid.SelectedIndex <= -1)
                    return;

                var model = (ExtensionAgentBuyingAreaDataGrid)extensionAgentBuyingAreaDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerService.ExtensionAgentBuyingAreaBL()
                        .Add(new ExtensionAgentBuyingArea
                        {
                            AreaCode = model.AreaCode,
                            ExtensionAgentCode =_extensionAgentCode,
                            CreateDate = DateTime.Now,
                            CreateUser = "Admin"
                        });
                else
                    BusinessLayerService.ExtensionAgentBuyingAreaBL()
                        .Delete(BusinessLayerService.ExtensionAgentBuyingAreaBL()
                        .GetSingle(model.AreaCode, _extensionAgentCode));

                BindingExtensionAgentBuyingAreaDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
