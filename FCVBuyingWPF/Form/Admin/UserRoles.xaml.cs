﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.Admin
{
    /// <summary>
    /// Interaction logic for UserRoles.xaml
    /// </summary>
    /// 

    public struct UserRoleDataGrid
    {
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
        public bool AssignStatus { get; set; }
    }

    public partial class UserRoles : Window
    {
        User _user;
        public UserRoles(User user)
        {
            InitializeComponent();
            _user = new User();
            _user = user;

            BindingUserRoleDataGrid();
        }

        private void BindingUserRoleDataGrid()
        {
            try
            {
                var list = from r in BusinessLayerService.RoleBL().GetAll()
                           join u in BusinessLayerService.UserRoleBL().GetByUser(_user.Username)
                           on r.RoleID equals u.RoleID into ur
                           from rs in ur.DefaultIfEmpty()
                           select new UserRoleDataGrid
                           {
                               RoleID = r.RoleID,
                               RoleName = r.RoleName,
                               AssignStatus = rs == null ? false : true
                           };

                userRoleDataGrid.ItemsSource = null;
                userRoleDataGrid.ItemsSource = list.OrderBy(x => x.RoleName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void userRoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (userRoleDataGrid.SelectedIndex <= -1)
                    return;

                var model = (UserRoleDataGrid)userRoleDataGrid.SelectedItem;
                if (!model.AssignStatus)
                    BusinessLayerService.UserRoleBL().Add(new UserRole
                    {
                        Username = _user.Username,
                        RoleID = model.RoleID,
                        CreateDate = DateTime.Now,
                        CreateUser = "Admin"
                    });
                else
                    BusinessLayerService.UserRoleBL()
                        .Delete(BusinessLayerService.UserRoleBL()
                        .GetById(_user.Username, model.RoleID));

                BindingUserRoleDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
