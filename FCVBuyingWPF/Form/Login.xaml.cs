﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
            UsernameTextBox.Focus();
            UsernameTextBox.Clear();
            PasswordTextBox.Clear();
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var model = BusinessLayerService.UserBL().GetById(UsernameTextBox.Text);

                if (model == null)
                {
                    MessageBox.Show("username ไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Helper.EncodeAndDecode.Base64Decode(model.Password) != PasswordTextBox.Password)
                {
                    MessageBox.Show("รหัสผ่านไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                /// store login information.
                /// 
                
                Model.user_setting.User = model;
                Model.user_setting.UserRoles = BusinessLayerService.UserRoleBL()
                    .GetByUser(model.Username)
                    .Select(x => new Role
                    {
                        RoleID = x.RoleID,
                        RoleName = x.Role.RoleName
                    }).ToList();

                this.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                LoginButton.IsEnabled = false;
                UsernameTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }
    }
}
