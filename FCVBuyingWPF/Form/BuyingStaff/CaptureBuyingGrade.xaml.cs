﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CaptureBuyingGrade.xaml
    /// </summary>
    public partial class CaptureBuyingGrade : Page
    {
        Buying _buying;
        List<BuyingGrade> _buyingGradeList;
        string _gradeGroupCode;

        public CaptureBuyingGrade()
        {
            InitializeComponent();

            _buying = new Buying();
            _buyingGradeList = new List<BuyingGrade>();

            var defaultGradeGroup = BusinessLayerService.BuyingGradeGroupBL()
                     .GetByCrop(user_setting.Crop.Crop1)
                     .Where(x => x.DefaultStatus == true);
            if (defaultGradeGroup.Count() == 0)
                throw new ArgumentException("ระบบยังไม่ได้ระบุ grade group default โปรดติดต่อผู้ดูแลระบบเพื่อดำเนินการ");

            if (defaultGradeGroup.Count() > 1)
                throw new ArgumentException("มีการตั้งค่า default grade group มากกว่า 1 " +
                    " โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบข้อมูล");

            _gradeGroupCode = defaultGradeGroup.SingleOrDefault().GradeGroupCode;
            _buyingGradeList = BusinessLayerService.BuyingGradeBL().GetByGradeGroup(_gradeGroupCode);
            ClearFrom();
        }

        private void ClearFrom()
        {
            try
            {
                buyingDocumentCodeTextBox.Text = "";
                farmerNameTextBox.Text = "";
                farmerQuotaTextBox.Text = "";
                totalSoldTextBox.Text = "";
                balanceTextBox.Text = "";
                RequstStatusCheckBox.IsChecked = false;
                FinishStatusCheckBox.IsChecked = false;
                baleNumberTextBox.Text = "";
                weightTextBox.Text = "";
                weightDateTextBox.Text = "";
                gradeTextBox.Text = "";
                gradeDateTextBox.Text = "";
                rejectInFoStackPanel.Visibility = Visibility.Collapsed;
                baleBarcodeTextBox.Text = "";
                baleBarcodeTextBox.Focus();

                _buying = null;
                buyingGradeBinding("X");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingGradeBinding(string gradePrefix)
        {
            try
            {
                if (string.IsNullOrEmpty(_gradeGroupCode))
                    throw new ArgumentException("โปรดระบุ group group code");

                buyingGradeItemsControl.ItemsSource = null;
                if (gradePrefix == "Other")
                {
                    List<BuyingGrade> fullList = new List<BuyingGrade>();
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "X").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "C").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "B").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "T").ToList());
                    List<BuyingGrade> ortherList = new List<BuyingGrade>();
                    ortherList.AddRange(_buyingGradeList.Except(fullList));
                    buyingGradeItemsControl.ItemsSource = ortherList
                    .OrderBy(x => x.Grade);
                }
                else
                {
                    buyingGradeItemsControl.ItemsSource = _buyingGradeList
                        .Where(x => x.Grade.Substring(0, 1) == gradePrefix)
                        .OrderBy(x => x.Grade);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDetailBinding(string baleBarcode)
        {
            try
            {
                _buying = BusinessLayerService.BuyingBL().GetSingle(baleBarcode);

                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยานี้ในระบบ");

                buyingDocumentCodeTextBox.Text = _buying.Crop + "-" +
                    _buying.FarmerCode + "-" +
                    _buying.StationCode + "-" +
                    _buying.DocumentNumber;

                createDateTextBox.Text = _buying.BuyingDocument.CreateDate.ToShortDateString();

                var _farmerProfile = Helper.FarmerProfileHelper
                    .GetSingleByCrop(user_setting.Crop.Crop1, _buying.FarmerCode);

                farmerNameTextBox.Text = _farmerProfile.Prefix +
                    _farmerProfile.FirstName + " " +
                    _farmerProfile.LastName;

                generalCodeTextBox.Text = _farmerProfile.GeneralCode;
                RequstStatusCheckBox.IsChecked = _buying.BuyingDocument.RequestFinishStatus;
                FinishStatusCheckBox.IsChecked = _buying.BuyingDocument.FinishStatus;

                baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                weightTextBox.Text = _buying.Weight.ToString("N1");
                weightDateTextBox.Text = _buying.WeightDate.ToShortDateString();
                gradeTextBox.Text = _buying.Grade;
                gradeDateTextBox.Text = _buying.GradeDate.ToString();

                if (_buying.RejectTypeID != null)
                {
                    rejectInFoStackPanel.Visibility = Visibility.Visible;
                    rejectTypeNameTextBox.Text = _buying.RejectType.RejectTypeName;
                }
                else
                    rejectInFoStackPanel.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูล โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง ถ้ายังไม่ได้ ให้ติดต่อผู้ดแลระบบ");

                if (_buying.BuyingDocument.FinishStatus == true)
                    throw new ArgumentException("buying document นี้ถูกปิดการซื้อขายแล้ว ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้");

                if (_buying.BuyingDocument.RequestFinishStatus == true)
                    throw new ArgumentException("buying document นี้อยู่ในระหว่างรอการยืนยันจากผู้รับผิดชอบ ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้");

                if (_buying.Grade != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ผ่านการบันทึกเกรดมาแล้วโดยเกรดเดิมคือ " + _buying.Grade + " ท่านต้องการที่จะแก้ไขเกรดซื้อใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                BusinessLayerService.BuyingBL()
                    .RemoveBuyingGrade(_buying.BaleBarcode,
                    user_setting.User.Username);

                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearFrom();
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buying == null)
                    throw new ArgumentException("โปรดลองสแกนบาร์โค้ตใหม่อีกครั้งเพื่อให้แสดง buying document ที่ต้องการดูรายละเอียดอีกครั้ง");

                var _extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(_buying.FarmerCode).ExtensionAgentCode;

                if (_extensionAgentCode.Contains("NPI"))
                {
                    BuyingDocumentDetailNPI window1 = new BuyingDocumentDetailNPI(
                        new vm_BuyingDocument
                        {
                            Crop = _buying.Crop,
                            FarmerCode = _buying.FarmerCode,
                            StationCode = _buying.StationCode,
                            DocumentNumber = _buying.DocumentNumber
                        });

                    window1.ShowDialog();
                }
                else
                {
                    BuyingDocumentDetailTW window2 = new BuyingDocumentDetailTW(
                        new vm_BuyingDocument
                        {
                            Crop = _buying.Crop,
                            FarmerCode = _buying.FarmerCode,
                            StationCode = _buying.StationCode,
                            DocumentNumber = _buying.DocumentNumber
                        });
                    window2.ShowDialog();
                }

                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมายเลขบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                BuyingDetailBinding(baleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = new Button();
                btn = (Button)sender;

                if (btn.Content.ToString() == "")
                {
                    MessageBox.Show("ไม่พบเกรดที่ท่านจะบันทึก โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบระบบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง ถ้ายังไม่ได้ ให้ติดต่อผู้ดแลระบบ",
                       "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying.BuyingDocument.FinishStatus == true)
                {
                    MessageBox.Show("buying document นี้ถูกปิดการซื้อขายแล้ว ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.BuyingDocument.RequestFinishStatus == true)
                {
                    MessageBox.Show("buying document นี้อยู่ในระหว่างรอการยืนยันจากผู้รับผิดชอบ ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.Weight.Equals(null))
                {
                    MessageBox.Show("ยาห่อนี้ยังไม่มีข้อมูลน้ำหนักซื้อ ไม่สามารถบันทึกเกรดได้ โปรดตรวจสอบ",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.RejectTypeID != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ผ่านการทำ reject มา ท่านต้องการที่จะบันทึกเกรดซื้อใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                if (_buying.Grade != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ผ่านการบันทึกเกรดมาแล้วโดยเกรดเดิมคือ " +
                        _buying.Grade + " ท่านต้องการที่จะแก้ไขเกรดซื้อใช่หรือไม่?",
                         "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                BusinessLayerService.BuyingBL().CaptureBuyingGrade(_buying.BaleBarcode,
                    _gradeGroupCode,
                    btn.Content.ToString(),
                    user_setting.User.Username);

                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void rejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูล โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง ถ้ายังไม่ได้ ให้ติดต่อผู้ดแลระบบ");

                if (_buying.TransportationCode != null)
                {
                    MessageBox.Show("ยาห่อนี้ถูกบันทึกข้อมูลในใบนำส่งแล้ว ไม่สามารถ reject ได้ " + Environment.NewLine +
                        "ท่านจะต้องลบข้อมูลยาห่อนี้ออกจากรถบรรทุกรหัสใบนำส่ง " + _buying.TransportationCode + " ก่อน",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.BuyingDocument.FinishStatus == true)
                    throw new ArgumentException("buying document นี้ถูกปิดการซื้อขายแล้ว ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้");

                if (_buying.BuyingDocument.RequestFinishStatus == true)
                    throw new ArgumentException("buying document นี้อยู่ในระหว่างรอการยืนยันจากผู้รับผิดชอบ ไม่สามารถแก้ไขข้อมูลห่อยานี้ซึ่งอยู่ใน buying document นี้ได้");

                if (MessageBox.Show("ท่านต้องการ reject ห่อยานี้ใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (_buying.RejectTypeID != null)
                {
                    if (MessageBox.Show("ห่อยานี้ถูกบันทึก reject แล้วด้วยสาเหตุ "
                        + _buying.RejectType.RejectTypeName
                        + " ท่านต้องการแก้ไขข้อมูลการ reject ใช่หรือไม่?",
                        "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                var returnModel = PopupRejectTypes.ReturnRejectType();
                if (returnModel.RejectTypeName == null)
                    return;

                BusinessLayerService.BuyingBL()
                    .RejectBale(_buying.BaleBarcode
                    , returnModel.RejectTypeID,
                    user_setting.User.Username);

                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void cancelRejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการยกเลิกข้อมูบการ reject ห่อยานี้ใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .CencelRejectBale(_buying.BaleBarcode,
                    user_setting.User.Username);

                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void gradeGroupXButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("X");
        }

        private void gradeGroupCButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("C");
        }

        private void gradeGroupBButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("B");
        }

        private void gradeGroupTButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("T");
        }

        private void gradeGroupOtherButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("Other");
        }
    }
}
