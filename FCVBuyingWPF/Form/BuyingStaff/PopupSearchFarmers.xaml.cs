﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for SearchFarmers.xaml
    /// </summary>
    public partial class PopupSearchFarmers : Window
    {
        static string _returnFarmerCode;
        public PopupSearchFarmers(string extentionAgentcode)
        {
            InitializeComponent();

            extensionAgentCodeTextBox.Text = extentionAgentcode;
            farmerDataGridBinding();
        }
        public static string ReturnFarmerCode(string extensionAgentCode)
        {
            PopupSearchFarmers window = new PopupSearchFarmers(extensionAgentCode);
            window.ShowDialog();
            return _returnFarmerCode;
        }
        private void farmerDataGridBinding()
        {
            try
            {
                if (user_setting.Crop == null)
                {
                    MessageBox.Show("ระบบไม่ได้ตั้งค่า default crop โปรดแจ้งผู้ดูแลระบบ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (extensionAgentCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุตัวแทน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                farmerDataGrid.ItemsSource = null;
                farmerDataGrid.ItemsSource = Helper.FarmerProfileHelper.
                    GetByExtensionAgent(user_setting.Crop.Crop1, extensionAgentCodeTextBox.Text);

                TotalRecordTextBox.Text = farmerDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            farmerDataGridBinding();
        }
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            farmerDataGridBinding();
        }
        private void selectedFarmerGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (farmerDataGrid.SelectedIndex < 0)
                    return;

                var model = (vm_FarmerProfile)farmerDataGrid.SelectedItem;
                _returnFarmerCode = model.FarmerCode;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
