﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CaptureWeight.xaml
    /// </summary>
    public partial class CaptureBuyingWeight : Window
    {
        vm_BuyingDocument _document;
        FCVBuyingEntities.FarmerQuota _farmerQuota;
        Farmer _farmerProfile;
        List<Buying> _buyingList;
        Buying _buying;
        BaleType _baleType;

        private delegate void preventCrossThreading(string str);
        private preventCrossThreading accessControlFromCentralThread;

        public CaptureBuyingWeight(vm_BuyingDocument model)
        {
            InitializeComponent();

            _baleType = new BaleType();
            _document = new vm_BuyingDocument();
            _buyingList = new List<Buying>();
            _buying = new Buying();
            _document = Helper.BuyingDocumentHelper
                .GetSingle(model.Crop, model.FarmerCode, model.StationCode, model.DocumentNumber);

            baleTypeComboBox.ItemsSource = null;
            baleTypeComboBox.ItemsSource = BusinessLayerService.BaleTypeBL()
                .GetBaleTypes()
                .OrderBy(x => x.BaleTypeName);

            baleTypeComboBox.SelectedIndex = 1;
            _baleType = (BaleType)baleTypeComboBox.SelectedItem;
            RefreshForm();
            ClearForm();
        }

        private void ClearForm()
        {
            baleBarcodeTextBox.Text = "";
            baleNumberTextBox.Text = "";
            weightTextBox.Text = "";
            baleBarcodeTextBox.Focus();

            _buying = null;

            if (useDigitalScaleCheckBox.IsChecked == false)
                weightTextBox.IsReadOnly = false;
            else
                weightTextBox.IsReadOnly = true;

            numpadPopup.IsOpen = false;
        }

        private void RefreshForm()
        {
            FarmerDetailsBinding();
            BuyingDetailsBinding();
            _buying = null;
        }

        private void FarmerDetailsBinding()
        {
            try
            {
                if (_document == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบซื้อนี้ในระบบ โปรดแจ้งผู้ดูแลระบบ");

                buyingDocumentCodeTextBox.Text = _document.BuyingDocumentCode;

                _farmerProfile = BusinessLayerService.FarmerBL().GetSingle(_document.FarmerCode);
                _farmerQuota = BusinessLayerService.FarmerQuotaBL()
                    .GetSingle(_document.Crop, _document.FarmerCode);

                farmerCodeTextBox.Text = _farmerProfile.Person.CitizenID;
                generalCodeTextBox.Text = _farmerQuota.GeneralCode;
                farmerNameTextBox.Text = _farmerProfile.Person.Prefix +
                    _farmerProfile.Person.FirstName + " " +
                    _farmerProfile.Person.LastName;
                farmerQuotaTextBox.Text = _farmerQuota.Quota.ToString("N1");
                RequestStatusCheckBox.IsChecked = _document.RequestFinishStatus;
                FinishStatusCheckBox.IsChecked = _document.FinishStatus;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDetailsBinding()
        {
            try
            {
                _buyingList = BusinessLayerService.BuyingBL()
                    .GetByDocument(_document.Crop,
                    _document.StationCode,
                    _document.FarmerCode,
                    _document.DocumentNumber)
                    .ToList();

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = _buyingList.OrderByDescending(x => x.WeightDate);
                totalBaleTextBox.Text = _buyingList.Count().ToString();
                totalWeightTextBox.Text = _buyingList.Sum(x => x.Weight).ToString("N1");

                var _totalSold = BusinessLayerService.BuyingBL()
                    .GetByFarmer(_document.Crop, _document.FarmerCode)
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight);

                totalSoldTextBox.Text = _totalSold.ToString("N1");
                balanceTextBox.Text = (Convert.ToDecimal(_farmerQuota.Quota) - _totalSold).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeBinding(string baleBarcode)
        {
            try
            {
                _buying = BusinessLayerService.BuyingBL().
                    GetSingle(baleBarcode);

                if (_buying != null)
                {
                    /// Is mean call recode for editting.
                    if (_buying.Crop == _document.Crop &&
                        _buying.FarmerCode == _document.FarmerCode &&
                        _buying.StationCode == _document.StationCode &&
                        _buying.DocumentNumber == _document.DocumentNumber)
                    {
                        baleTypeComboBox.SelectedValue = _buying.BaleTypeID;
                        baleBarcodeTextBox.Text = _buying.BaleBarcode;
                        baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                        weightTextBox.Text = _buying.Weight.ToString("N1");
                        baleNumberTextBox.Focus();
                    }
                    else /// this bale is wrong owner.
                    {
                        MessageBox.Show("ห่อยานี้ไม่ใช้ของชาวไร่รายนี้ ระบบไม่อนุญาตให้ดำเนินการใดๆ (รายละเอียดเกี่ยวกับห่อยาแสดงไว้ด้านล่าง)" + Environment.NewLine +
                            "bale barcode: " + _buying.BaleBarcode + Environment.NewLine +
                            "bale number: " + _buying.BaleNumber + Environment.NewLine +
                            "farmer code: " + _buying.FarmerCode + Environment.NewLine +
                            "farmer name: " + _buying.BuyingDocument.FarmerQuota.Farmer.Person.Prefix +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.FirstName + " " +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.LastName + Environment.NewLine +
                            "document code: " + _buying.DocumentNumber + Environment.NewLine +
                            "create date: " + _buying.BuyingDocument.CreateDate + Environment.NewLine +
                            "weight date: " + _buying.WeightDate + Environment.NewLine
                            , "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save()
        {
            try
            {
                if (_document.RequestFinishStatus == true)
                    throw new ArgumentException("Buying document นี้อยู่ในสถานะรอการยืนยัน (request finish ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (_document.FinishStatus == true)
                    throw new ArgumentException("Buying document นี้อยู่ในสถานะถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (baleTypeComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก ประเภทห่อยา (Bale type)");

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนหรือกรอกหมายเลขบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }
                if (baleBarcodeTextBox.Text.Length != 17)
                {
                    MessageBox.Show("รหัสบาร์โค้ตจะต้องมีจำนวน 17 หลัก", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Clear();
                    baleBarcodeTextBox.Focus();
                    return;
                }
                if (baleBarcodeTextBox.Text.Substring(0, 2) != "06")
                {
                    MessageBox.Show("รหัสบาร์โค้ตจะต้องขึ้นต้นด้วยรหัส 06 เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Clear();
                    baleBarcodeTextBox.Focus();
                    return;
                }
                if (baleNumberTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกหมายเลขห่อยา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                    return;
                }
                if (weightTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกน้ำหนักห่อยา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    weightTextBox.Text = "";
                    weightTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsNumericCharacter(baleNumberTextBox.Text))
                {
                    MessageBox.Show("ระบบอนุญาตให้หมายเลขห่อยาประกอบด้วย ตัวเลข เท่านั้น",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Clear();
                    baleNumberTextBox.Focus();
                    return;
                }

                if (baleNumberTextBox.Text.Length > 4)
                {
                    MessageBox.Show("หมายเลขห่อยา ไม่ควรเกิน 4 หลัก",
                       "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                }

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(baleBarcodeTextBox.Text))
                {
                    MessageBox.Show("ระบบอนุญาตให้รหัสบาร์โค้ตประกอบด้วยตัวเลข, เครื่องหมายขีดกลาง (-) และตัวอักษรภาษาอังกฤษ เท่านั้น",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsDecimalCharacter(weightTextBox.Text))
                {
                    MessageBox.Show("ระบบอนุญาตให้ช่องน้ำหนักประกอบด้วยตัวเลขและจุดทศนิยม เท่านั้น",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    weightTextBox.Text = "";
                    weightTextBox.Focus();
                    return;
                }

                double _weight = Convert.ToDouble(weightTextBox.Text);
                if (_weight < 1)
                {
                    MessageBox.Show("น้ำหนักใบยาจะต้องมากกว่า 0", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    weightTextBox.Text = "";
                    weightTextBox.Focus();
                    return;
                }

                if (_weight >= 100)
                    if (MessageBox.Show("ห่อยานี้มีน้ำหนักเกิน 100 กก. ท่านต้องการบันทึกข้อมูลนี้จริงใช่หรือไม่?",
                        "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question)
                        == MessageBoxResult.No)
                        return;

                if (_buying == null) /// Adding function.
                {
                    BusinessLayerService.BuyingBL().CaptureBuyingWeight(
                        baleBarcodeTextBox.Text,
                        Convert.ToInt32(baleNumberTextBox.Text),
                        _document.Crop,
                        _document.FarmerCode,
                        _document.StationCode,
                        _document.DocumentNumber,
                        Convert.ToInt32(baleTypeComboBox.SelectedValue),
                        Convert.ToDecimal(_weight),
                        user_setting.User.Username);
                }
                else /// editing function.
                {
                    if (_buying.Crop != _document.Crop ||
                            _buying.FarmerCode != _document.FarmerCode ||
                            _buying.StationCode != _document.StationCode ||
                            _buying.DocumentNumber != _document.DocumentNumber)
                    {
                        MessageBox.Show("ห่อยานี้ไม่ใช้ของชาวไร่รายนี้ ระบบไม่อนุญาตให้ดำเนินการใดๆ (รายละเอียดเกี่ยวกับห่อยาแสดงไว้ด้านล่าง)" + Environment.NewLine +
                            "bale barcode: " + _buying.BaleBarcode + Environment.NewLine +
                            "bale number: " + _buying.BaleNumber + Environment.NewLine +
                            "farmer code: " + _buying.FarmerCode + Environment.NewLine +
                            "farmer name: " + _buying.BuyingDocument.FarmerQuota.Farmer.Person.Prefix +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.FirstName + " " +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.LastName + Environment.NewLine +
                            "document code: " + _buying.DocumentNumber + Environment.NewLine +
                            "create date: " + _buying.BuyingDocument.CreateDate + Environment.NewLine +
                            "weight date: " + _buying.WeightDate + Environment.NewLine
                            , "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                    if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "warning",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;

                    BusinessLayerService.BuyingBL()
                        .ChangeBuyingWeightInfo(
                        _buying.BaleBarcode,
                        Convert.ToInt32(baleNumberTextBox.Text),
                        Convert.ToInt32(baleTypeComboBox.SelectedValue),
                        Convert.ToDecimal(_weight),
                        user_setting.User.Username);
                }
                ClearForm();
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (!Decimal.TryParse(str, out value))
                {
                    weightTextBox.Dispatcher.Invoke(() =>
                    {
                        weightTextBox.Text = str;
                    });
                    return;
                }

                var _weight = Convert.ToDouble(str);


                /// Hessian weight 2.2 Kg per bale. STEC set 2.3 Kg per bale. 
                /// Open bale string weight 0.1 Kg per bale.
                /// 
                if (_baleType.BaleTypeName == "Close Bale")
                    _weight = _weight - 2.3;
                else
                    _weight = _weight - 0.1;

                if (weightTextBox.Dispatcher.CheckAccess())
                    weightTextBox.Text = _weight.ToString();
                else
                    weightTextBox.Dispatcher.Invoke(() =>
                    {
                        weightTextBox.Text = _weight.ToString();
                    });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //useDigitalScaleCheckBox.IsChecked = false;
            }
        }

        private void useDigitalScaleCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (useDigitalScaleCheckBox.IsChecked == false)
                {
                    /// close a serial port for communicate with a digital scale.
                    /// 
                    weightTextBox.IsReadOnly = false;
                    weightTextBox.Text = "";
                    weightTextBox.Focus();
                    if (Helper.DigitalScaleComunicationHelper.serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang
                        CloseDown.Start(); //close port in new thread to avoid hang
                    }
                }
                else
                {
                    /// open a serial port for communicate with a digital scale.
                    /// 
                    if (SerialPort.GetPortNames().Count() <= 0)
                    {
                        MessageBox.Show("ไม่พบ Comport สำหรับใช้ในเชื่อมต่อกับเครื่องชั่งดิจิตอล บนเครื่องคอมพิวเตอร์ของคุณ",
                            "warning", MessageBoxButton.OK, MessageBoxImage.Warning);

                        useDigitalScaleCheckBox.IsChecked = false;
                        return;
                    }

                    Helper.DigitalScaleComunicationHelper.Setup(new Helper.ComportParameter
                    {
                        PortName = Properties.Settings.Default.PortName,
                        BaudRate = Properties.Settings.Default.BaudRate,
                        Parity = Properties.Settings.Default.Parity,
                        StopBits = Properties.Settings.Default.StopBits,
                        DataBits = Properties.Settings.Default.Databit,
                        ReplaceChar = Properties.Settings.Default.ReplaceChar,
                        StrLength = Properties.Settings.Default.SubStringLength,
                        ThreadSleep = Properties.Settings.Default.ThreadSleep
                    });

                    if (Helper.DigitalScaleComunicationHelper.serialPort.IsOpen == true)
                    {
                        MessageBox.Show("มีโปรแกรมอื่นกำลังเข้าถึง port การเชื่อมต่อนี้อยู่ ไม่สามารถเชื่อมต่อได้ในขณะนี้", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        useDigitalScaleCheckBox.IsChecked = false;
                        return;
                    }

                    Helper.DigitalScaleComunicationHelper.serialPort.Open();
                    Helper.DigitalScaleComunicationHelper.serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                    accessControlFromCentralThread = displayTextReadIn;
                    weightTextBox.IsReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                useDigitalScaleCheckBox.IsChecked = false;
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (_farmerProfile.FarmerCode.Contains("PR"))
                displayTextReadIn(Helper.DigitalScaleComunicationHelper.GetWeightResultFrom19eA());
            else
                displayTextReadIn(Helper.DigitalScaleComunicationHelper.GetWeightResultFromTigerModel());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Helper.DigitalScaleComunicationHelper.serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                Helper.DigitalScaleComunicationHelper.serialPort.DataReceived -= port_DataReceived;
                Helper.DigitalScaleComunicationHelper.serialPort.Close(); //close the serial port
                this.Dispatcher.Invoke(() =>
                {
                    this.Close();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void CloseSerialNonExit()
        {
            try
            {
                Helper.DigitalScaleComunicationHelper.serialPort.DataReceived -= port_DataReceived;
                Helper.DigitalScaleComunicationHelper.serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                _buying = (Buying)buyingDataGrid.SelectedItem;

                if (_document.RequestFinishStatus == true)
                {
                    MessageBox.Show("Buying document นี้อยู่ในสถานะ รอการยืนยัน (request finish ไม่สามารถลบ/แก้ไขข้อมูลได้",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_document.FinishStatus == true)
                {
                    MessageBox.Show("Buying document นี้อยู่ในสถานะ ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL().DeleteBaleBarcode(_buying.BaleBarcode);
                ClearForm();
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleNumberTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกหมายเลขห่อยา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsNumericCharacter(baleNumberTextBox.Text))
                {
                    MessageBox.Show("ระบบอนุญาตให้หมายเลขห่อยาประกอบด้วยตัวเลขเท่านั้น",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                    return;
                }

                if (baleNumberTextBox.Text.Length > 4)
                {
                    MessageBox.Show("หมายเลขห่อยา ไม่ควรเกิน 4 หลัก",
                       "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Text = "";
                    baleNumberTextBox.Focus();
                }

                //weightTextBox.Text = "";
                weightTextBox.SelectAll();
                weightTextBox.Focus();

                if (useDigitalScaleCheckBox.IsChecked == false)
                    weightTextBox.IsReadOnly = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void weightTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                var model = (Buying)buyingDataGrid.SelectedItem;
                BaleBarcodeBinding(model.BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนหรือกรอกหมายเลขบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (baleBarcodeTextBox.Text.Length != 17)
                {
                    MessageBox.Show("รหัสบาร์โค้ตจะต้องมีจำนวน 17 หลัก", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Clear();
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (baleBarcodeTextBox.Text.Substring(0, 2) != "06")
                {
                    MessageBox.Show("รหัสบาร์โค้ตจะต้องขึ้นต้นด้วยรหัส 06 เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Clear();
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(baleBarcodeTextBox.Text))
                {
                    MessageBox.Show("ระบบอนุญาตให้รหัสบาร์โค้ตประกอบด้วยตัวเลข เครื่องหมาย - และตัวอักษรภาษาอังกฤษเท่านั้น",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                baleNumberTextBox.Text = "";
                baleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void weightTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (baleBarcodeTextBox.Text == "")
            {
                MessageBox.Show("โปรดสแกนรหัสบาร์โค้ตก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                baleBarcodeTextBox.Focus();
                numpadPopup.IsOpen = false;
                return;
            }
            //Key in. (not use digital scale)
            if (isShowNumpadCheckBox.IsChecked == true)
            {
                numpadPopup.IsOpen = true;
                NumpadTypeTextBlock.Text = "Weight";
                NumpadDisplayTextBox.Text = "";
                weightTextBox.SelectAll();
                //weightTextBox.Text = "";
            }
        }

        private void baleNumberTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (baleBarcodeTextBox.Text == "")
            {
                MessageBox.Show("โปรดสแกนรหัสบาร์โค้ตก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                baleBarcodeTextBox.Focus();
                numpadPopup.IsEnabled = false;
                return;
            }

            //Key in. (not use digital scale)
            if (isShowNumpadCheckBox.IsChecked == true)
            {
                numpadPopup.IsOpen = true;
                NumpadTypeTextBlock.Text = "Bale Number";
                NumpadDisplayTextBox.Text = "";
                //baleNumberTextBox.Text = "";
            }
        }

        private void baleBarcodeTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            //Key in. (not use digital scale)
            //if (isShowNumpadCheckBox.IsChecked == true)
            //{
            //    numpadPopup.IsOpen = true;
            //    NumpadTypeTextBlock.Text = "Bale Barcode";
            //    NumpadDisplayTextBox.Text = "";
            //    baleBarcodeTextBox.Text = "";
            //}
        }

        private void isShowNumpadCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (isShowNumpadCheckBox.IsChecked == true)
                weightTextBox.IsReadOnly = false;
        }

        private void baleTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (baleTypeComboBox.SelectedIndex < 0)
                return;

            _baleType = (BaleType)baleTypeComboBox.SelectedItem;
        }

        private void NumpadKeyIn(string number)
        {
            if (NumpadTypeTextBlock.Text == "Weight")
            {
                NumpadDisplayTextBox.Text = NumpadDisplayTextBox.Text + number;
                weightTextBox.Text = NumpadDisplayTextBox.Text;
            }
            else if (NumpadTypeTextBlock.Text == "Bale Number")
            {
                NumpadDisplayTextBox.Text = baleNumberTextBox.Text + number;
                baleNumberTextBox.Text = NumpadDisplayTextBox.Text;
            }
            else if (NumpadTypeTextBlock.Text == "Bale Barcode")
            {
                NumpadDisplayTextBox.Text = baleBarcodeTextBox.Text + number;
                baleBarcodeTextBox.Text = NumpadDisplayTextBox.Text;
            }
        }

        private void OneButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TwoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("2");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ThreeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("3");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void FourButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("4");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FiveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("5");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SixButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("6");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SevenButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("7");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EightButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("8");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NineButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("9");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ZeroButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn("0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DotButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NumpadKeyIn(".");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Key in. (not use digital scale)
                if (useDigitalScaleCheckBox.IsChecked == false)
                {
                    if (NumpadTypeTextBlock.Text == "Bale Number")
                    {
                        NumpadTypeTextBlock.Text = "Weight";
                        NumpadDisplayTextBox.Text = "";
                        weightTextBox.Focus();
                    }
                    else if (NumpadTypeTextBlock.Text == "Bale Barcode")
                    {
                        NumpadTypeTextBlock.Text = "Bale Number";
                        NumpadDisplayTextBox.Text = "";
                        baleNumberTextBox.Focus();
                    }
                    else if (NumpadTypeTextBlock.Text == "Weight")
                    {
                        Save();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClosePopupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                numpadPopup.IsOpen = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearNumPadButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NumpadTypeTextBlock.Text == "Weight")
                {
                    NumpadDisplayTextBox.Text = "";
                    weightTextBox.Text = "";
                }
                else if (NumpadTypeTextBlock.Text == "Bale Number")
                {
                    NumpadDisplayTextBox.Text = "";
                    baleNumberTextBox.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleBarcodeTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ClearForm();
        }
    }
}
