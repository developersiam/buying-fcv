﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Helper;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for TransportationDetails.xaml
    /// </summary>
    public partial class TransportationDetails : Window
    {
        Buying _buying;
        string _transportationCode;
        vm_TransportationDocument _document;

        public TransportationDetails(string transportationCode)
        {
            try
            {
                InitializeComponent();

                _buying = new Buying();
                _document = new vm_TransportationDocument();

                _transportationCode = transportationCode;
                loadingRadioButton.IsChecked = true;
                ReloadDocument();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadDocument()
        {
            try
            {
                _document = TransportationHelper.GetSingle(_transportationCode);
                var transport = _document.TransportationDocument;

                transportationCodeTextBox.Text = transport.TransportationCode;
                truckNumberTextBox.Text = transport.TruckNo;
                lockedStatusButton.Visibility = transport.IsFinish == true ? Visibility.Collapsed : Visibility.Visible;
                unlockedStatusButton.Visibility = transport.IsFinish == true ? Visibility.Visible : Visibility.Collapsed;
                printDetailsButton.IsEnabled = transport.IsFinish;
                baleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadDataGrid()
        {
            try
            {
                var list = BuyingHelper.GetByTransportationCode(_transportationCode);

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = list.OrderByDescending(x => x.LoadBaleToTruckDate);

                totalBaleTextBox.Text = string.Format("{0:N0}", list.Count());
                totalWeightTextBox.Text = string.Format("{0:N1}", list.Sum(b => b.Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save(string baleBarcode)
        {
            try
            {
                var transport = _document.TransportationDocument;
                if (baleBarcode == "")
                {
                    MessageBox.Show("โปรดกรอกรหัสป้ายบาร์โค้ต.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying.Grade == null)
                    throw new Exception("ยาห่อนี้ไม่มีเกรดซื้อ ไม่สามารถบันทึกขึ้นรถบรรทุกได้ โปรดตรวจสอบอีกครั้ง");

                if (loadingRadioButton.IsChecked == true)
                {
                    if (transport.IsFinish == true)
                        throw new Exception("เอกสารหมายเลข " + transport.TransportationCode +
                            " นี้ถูกเปลี่ยนสถานะเป็น Lock เพื่อพิมพ์ให้กับรถบรรทุกแล้ว " +
                            "ไม่สามารถบันทึกห่อยาขึ้นรถคันนี้ได้ ท่านต้องปลดล็อคก่อน จึงจะบันทึกข้อมูลได้อีกครั้ง");

                    if (_buying.TransportationCode != null
                        && _buying.TransportationCode == transport.TransportationCode)
                        if (MessageBoxHelper.Question("ยาห่อนี้ถูกบันทึกข้อมูลการขนขึ้นรถแล้วในเอกสารหมายเลข" + 
                            _buying.TransportationCode + " ท่านต้องการบันทึกซ้ำหรือไม่?")
                            == MessageBoxResult.No)
                            return;

                    if (_buying.TransportationCode != null
                        && _buying.TransportationCode != transport.TransportationCode)
                    {
                        if (MessageBoxHelper.Question("ยาห่อนี้ถูกบันทึกการขนขึ้นรถบรรทุกไปแล้ว ในเอกสารหมายเลข " + _buying.TransportationCode +
                            " หมายเลขทะเบียนรถ " + _buying.TransportationDocument.TruckNo + Environment.NewLine +
                            "ท่านต้องการย้ายข้อมูลห่อยาจากเอกสารหมายเลขดังกล่าว " +
                            "มายังเอกสารหมายเลข " + transport.TransportationCode +
                            " ทะเบียนรถ " + transport.TruckNo + " นี้ใช่หรือไม่?")
                            == MessageBoxResult.No)
                            return;

                        if (_buying.TransportationDocument.IsFinish == true)
                        {
                            if (MessageBoxHelper.Question("ท่านต้องกลับไปพิมพ์เอกสารหมายเลข " + _buying.TransportationCode +
                                " ทะเบียนรถ " + _buying.TransportationDocument.TruckNo +
                                " อีกครั้งเนื่องจากข้อมูลมีการเปลี่ยนแปลง " +
                                "ท่านยังยืนยันที่จะย้ายห่อยามายังเอกสารหมายเลข " + transport.TransportationCode + 
                                " นี้ใช่หรือไม่?") 
                                == MessageBoxResult.No)
                                return;
                        }
                    }

                    BusinessLayerService.TransportationDocumentBL()
                        .BringUp(baleBarcode, user_setting.User.Username, transport.TransportationCode);
                }
                else
                {
                    if (_buying.TransportationCode != null && 
                        _buying.TransportationCode != transport.TransportationCode)
                    {
                        MessageBoxHelper.Warning("ยาห่อนี้ไม่ได้อยู่ในเอกสารหมายเลข " + transport.TransportationCode + 
                            " ทะเบียน " + transport.TruckNo +
                            " โปรดย้อนกลับไปเลือกเอกสารหมายเลข " + _buying.TransportationCode + 
                            " ทะเบียน " + _buying.TransportationDocument.TruckNo +
                            " เพื่อลบห่อยาออกจากรถคันดังกล่าว");
                        return;
                    }

                    BusinessLayerService.TransportationDocumentBL().BringDown(baleBarcode);
                }

                ReloadDocument();
                ReloadDataGrid();
                baleBarcodeTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBoxHelper.Warning("โปรดระบุรหัสป้ายบาร์โค้ต");
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    return;
                }

                _buying = BusinessLayerService.BuyingBL()
                    .GetSingle(baleBarcodeTextBox.Text.Replace(" ", ""));

                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " + 
                        baleBarcodeTextBox.Text + " นี้ในระบบ");
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    return;
                }

                Save(_buying.BaleBarcode);
                baleBarcodeTextBox.Clear();
                baleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TakeOffRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            loadingRadioButton.IsChecked = false;
        }

        private void LockedStatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BusinessLayerService.BuyingBL()
                    .GetByTransportationDocument(_transportationCode)
                    .Count < 1)
                    if (MessageBoxHelper.Question("เอกสารแต่ละรายการ จะต้องมีห่อยาจำนวน 1 ห่อ ขึ้นไป " +
                        "ระบบจึงจะอนุญาตให้ Lock ได้ ท่านยังจะต้องการ Lock เอกสารฉบับนี้อีกหรือไม่?") 
                        == MessageBoxResult.No)
                        return;

                if (MessageBoxHelper.Question("ท่านต้องการล็อคเอกสารนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.TransportationDocumentBL()
                    .Finish(_document.TransportationDocument.TransportationCode);

                ReloadDocument();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UnlockedStatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BusinessLayerService.TransportationDocumentBL()
                    .UnFinish(_document.TransportationDocument.TransportationCode);
                ReloadDocument();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void baleBarcodeTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            baleBarcodeTextBox.Clear();
        }

        private void printSummaryButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                var window = new Report.RPTTR02(_document.TransportationDocument);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void printDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Report.RPTTR01 window = new Report.RPTTR01(_document.TransportationDocument);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
