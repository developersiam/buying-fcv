﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingGrades.xaml
    /// </summary>
    public partial class PopupBuyingGrades : Window
    {
        public static BuyingGrade _returnVal { get; set; }
        public static List<BuyingGrade> _buyingGradeList { get; set; }
        public static string _gradeGroupCode { get; set; }
        public static string _gradePrefix { get; set; }

        public PopupBuyingGrades()
        {
            try
            {
                InitializeComponent();
                _returnVal = null;
                gradeGroupCodeTextBox.Text = _gradeGroupCode;
                buyingGradeBinding(_gradePrefix);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public static BuyingGrade ReturnValue()
        {
            PopupBuyingGrades window = new PopupBuyingGrades();
            window.ShowDialog();
            return _returnVal;
        }

        private void buyingGradeBinding(string gradePrefix)
        {
            try
            {
                if (gradeGroupCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุ group group code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                buyingGradeItemsControl.ItemsSource = null;

                if (gradePrefix == "Other")
                {
                    List<BuyingGrade> fullList = new List<BuyingGrade>();
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "X").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "C").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "B").ToList());
                    fullList.AddRange(_buyingGradeList.Where(x => x.Grade.Substring(0, 1) == "T").ToList());
                    List<BuyingGrade> ortherList = new List<BuyingGrade>();
                    ortherList.AddRange(_buyingGradeList.Except(fullList));
                    buyingGradeItemsControl.ItemsSource = ortherList
                    .OrderBy(x => x.Grade);
                }
                else
                {
                    buyingGradeItemsControl.ItemsSource = _buyingGradeList
                        .Where(x => x.Grade.Substring(0, 1) == gradePrefix)
                        .OrderBy(x => x.Grade);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void gradeGroupXButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("X");
        }

        private void gradeGroupCButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("C");
        }

        private void gradeGroupBButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("B");
        }

        private void gradeGroupTButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("T");
        }

        private void gradeGroupOtherButton_Click(object sender, RoutedEventArgs e)
        {
            buyingGradeBinding("Other");
        }

        private void buyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = new Button();
                btn = (Button)sender;

                if (btn.Content.ToString() == "")
                {
                    MessageBox.Show("ไม่พบเกรดที่ท่านจะบันทึก โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบระบบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                _returnVal = BusinessLayerService.BuyingGradeBL()
                    .GetSingle(gradeGroupCodeTextBox.Text, btn.Content.ToString());

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
