﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for DigitalScaleSetup.xaml
    /// </summary>
    /// 
    public struct Port
    {
        public string PortName { get; set; }
    }
    public struct Parities {
        public Parity Parity { get; set; }
    }
    public struct DataBit {
        public short DataBits { get; set; }
    }
    public struct BuardRate
    {
        public int BuardRates { get; set; }
    }
    public struct StopBit
    {
        public StopBits StopBits { get; set; }
    }
    public struct ThreadSleep
    {
        public int ThreadSleeps { get; set; }
    }

    public partial class PopupDigitalScaleSetup : Window
    {
        SerialPort _serialPort;
        public PopupDigitalScaleSetup()
        {
            InitializeComponent();

            _serialPort = new SerialPort();

            List<Port> ports = new List<Port>();
            foreach (var item in SerialPort.GetPortNames())
                ports.Add(new Port { PortName = item });

            comPortComboBox.ItemsSource = null;
            comPortComboBox.ItemsSource = ports;

            List<Parities> parities = new List<Parities>();
            parities.Add(new Parities { Parity = Parity.Even });
            parities.Add(new Parities { Parity = Parity.Odd });
            parities.Add(new Parities { Parity = Parity.None });
            parities.Add(new Parities { Parity = Parity.Mark });
            parities.Add(new Parities { Parity = Parity.Space });
            parityComboBox.ItemsSource = null;
            parityComboBox.ItemsSource = parities;

            List<DataBit> dataBits = new List<DataBit>();
            dataBits.Add(new DataBit { DataBits = 8 });
            dataBits.Add(new DataBit { DataBits = 7 });
            dataBits.Add(new DataBit { DataBits = 6 });
            dataBits.Add(new DataBit { DataBits = 5 });
            dataBitComboBox.ItemsSource = null;
            dataBitComboBox.ItemsSource = dataBits;

            List<BuardRate> buardRates = new List<BuardRate>();
            buardRates.Add(new BuardRate { BuardRates = 115200 });
            buardRates.Add(new BuardRate { BuardRates = 57600 });
            buardRates.Add(new BuardRate { BuardRates = 38400 });
            buardRates.Add(new BuardRate { BuardRates = 19200 });
            buardRates.Add(new BuardRate { BuardRates = 9600 });
            buardRates.Add(new BuardRate { BuardRates = 7200 });
            buardRates.Add(new BuardRate { BuardRates = 4800 });
            buardRates.Add(new BuardRate { BuardRates = 2400 });
            buardRateComboBox.ItemsSource = null;
            buardRateComboBox.ItemsSource = buardRates;

            List<StopBit> stopBits = new List<StopBit>();
            stopBits.Add(new StopBit { StopBits = StopBits.None });
            stopBits.Add(new StopBit { StopBits = StopBits.One });
            stopBits.Add(new StopBit { StopBits = StopBits.OnePointFive });
            stopBits.Add(new StopBit { StopBits = StopBits.Two });
            stopBitComboBox.ItemsSource = null;
            stopBitComboBox.ItemsSource = stopBits;

            List<ThreadSleep> threadSleeps = new List<ThreadSleep>();
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 100 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 200 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 300 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 400 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 500 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 600 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 700 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 800 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 900 });
            threadSleeps.Add(new ThreadSleep { ThreadSleeps = 1000 });
            ThreadSleepComboBox.ItemsSource = null;
            ThreadSleepComboBox.ItemsSource = threadSleeps;

            /// Binding setting parameter to control.
            comPortComboBox.SelectedValue = Properties.Settings.Default.PortName;
            buardRateComboBox.SelectedValue = Properties.Settings.Default.BaudRate;
            parityComboBox.SelectedValue = Properties.Settings.Default.Parity;
            stopBitComboBox.SelectedValue = Properties.Settings.Default.StopBits;
            dataBitComboBox.SelectedValue = Properties.Settings.Default.Databit;
            ThreadSleepComboBox.SelectedValue = Properties.Settings.Default.ThreadSleep;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(comPortComboBox.SelectedIndex <0)
                {
                    MessageBox.Show("โปรดเลือก COM Port", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    comPortComboBox.Focus();
                    return;
                }
                if (buardRateComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Buard Rate", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    buardRateComboBox.Focus();
                    return;
                }
                if (parityComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Parity", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    parityComboBox.Focus();
                    return;
                }
                if (stopBitComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Stop bit", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stopBitComboBox.Focus();
                    return;
                }
                if (dataBitComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือก Data bit", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    dataBitComboBox.Focus();
                    return;
                }

                Properties.Settings.Default.PortName = comPortComboBox.SelectedValue.ToString();
                Properties.Settings.Default.BaudRate = Convert.ToInt32(buardRateComboBox.SelectedValue.ToString());
                Properties.Settings.Default.StopBits = ((StopBits)stopBitComboBox.SelectedValue);
                Properties.Settings.Default.Databit = Convert.ToInt16(dataBitComboBox.SelectedValue);
                Properties.Settings.Default.Parity = ((Parity)parityComboBox.SelectedValue);
                Properties.Settings.Default.ThreadSleep = Convert.ToInt32(ThreadSleepComboBox.SelectedValue);

                Properties.Settings.Default.Save();

                MessageBox.Show("บันทึกข้อมูลสำเร็จ!", "information", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
