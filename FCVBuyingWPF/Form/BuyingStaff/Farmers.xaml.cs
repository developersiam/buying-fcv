﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for Farmers.xaml
    /// </summary>
    public partial class Farmers : Page
    {
        public Farmers()
        {
            InitializeComponent();
            extensionAgentBinding();
        }

        private void extensionAgentBinding()
        {
            try
            {
                List<ExtensionAgent> list = new List<ExtensionAgent>();

                foreach (var item in BusinessLayerService.UserAreaBL()
                    .GetByUser(user_setting.User.Username))
                {
                    list.AddRange(BusinessLayerService.ExtensionAgentBL()
                        .GetByArea(item.AreaCode));
                }

                extensionAgentComboBox.ItemsSource = null;
                extensionAgentComboBox.ItemsSource = list.OrderBy(x => x.ExtensionAgentCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerDataGridBinding()
        {
            try
            {
                if (extensionAgentComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุตัวแทน");

                farmerDataGrid.ItemsSource = null;
                farmerDataGrid.ItemsSource = BusinessLayerService.FarmerQuotaBL()
                    .GetByExtensionAgent(user_setting.Crop.Crop1,
                    extensionAgentComboBox.SelectedValue.ToString());

                TotalRecordTextBox.Text = farmerDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            farmerDataGridBinding();
        }

        private void cropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            farmerDataGridBinding();
        }

        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            farmerDataGridBinding();
        }

        private void farmerQuotaGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (farmerDataGrid.SelectedIndex < 0)
                    return;

                var model = (FCVBuyingEntities.FarmerQuota)farmerDataGrid.SelectedItem;
                FarmerQuota window = new FarmerQuota(BusinessLayerService.FarmerBL().GetSingle(model.FarmerCode));
                window.ShowDialog();
                farmerDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
