﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NTRMInspection.xaml
    /// </summary>
    public partial class NTRMInspection : Page
    {
        Buying _baleBarcode;
        public NTRMInspection()
        {
            InitializeComponent();

            _baleBarcode = new Buying();

            ntrmTypeItemsControl.ItemsSource = null;
            ntrmTypeItemsControl.ItemsSource = BusinessLayerService.NTRMTypeBL()
                .GetNtrmTypes()
                .OrderBy(x=>x.NTRMTypeName);

            baleBarcodeTextBox.Focus();
        }

        private void ClearForm()
        {
            buyingDocumentCodeTextBox.Text = "";
            farmerNameTextBox.Text = "";
            baleNumberTextBox.Text = "";
            weightDateTextBox.Text = "";
            weightTextBox.Text = "";
            gradeDateTextBox.Text = "";
            gradeTextBox.Text = "";
            rejectTypeTextBox.Text = "";
            baleBarcodeTextBox.Text = "";
            baleBarcodeTextBox.Focus();

            ntrmInspectionDataGrid.ItemsSource = null;
            TotalRecordTextBox.Text = ntrmInspectionDataGrid.Items.Count.ToString();
        }

        private void RefreshForm()
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดแสกนบาร์โค้ตห่อยาที่ต้องการบันทึก NTRM", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                _baleBarcode = BusinessLayerService.BuyingBL().GetSingle(baleBarcodeTextBox.Text);

                if (_baleBarcode == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcodeTextBox.Text + " ในระบบ", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                buyingDocumentCodeTextBox.Text = _baleBarcode.Crop + "-" +
                    _baleBarcode.FarmerCode + "-" +
                    _baleBarcode.StationCode + "-" +
                    _baleBarcode.DocumentNumber;

                var _farmerProfile = BusinessLayerService.FarmerBL()
                    .GetSingle(_baleBarcode.FarmerCode);

                farmerNameTextBox.Text = _farmerProfile.Person.Prefix +
                    _farmerProfile.Person.FirstName + " " +
                    _farmerProfile.Person.LastName;

                baleNumberTextBox.Text = _baleBarcode.BaleNumber.ToString();
                weightTextBox.Text = _baleBarcode.Weight.ToString("N1");
                gradeTextBox.Text = _baleBarcode.Grade;
                rejectTypeTextBox.Text = _baleBarcode.RejectTypeID == null ?
                    "" : _baleBarcode.RejectType.RejectTypeName;
                weightDateTextBox.Text = _baleBarcode.WeightDate.ToShortDateString();
                gradeDateTextBox.Text = _baleBarcode.GradeDate.ToString();

                var list = BusinessLayerService.NTRMInspectionBL()
                    .GetByBaleBarcode(_baleBarcode.BaleBarcode)
                    .ToList();

                ntrmInspectionDataGrid.ItemsSource = null;
                ntrmInspectionDataGrid.ItemsSource = list;

                TotalRecordTextBox.Text = ntrmInspectionDataGrid.Items.Count.ToString();
                TotalQuantityTextBox.Text = list.Sum(x => x.Quantity).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ntrmTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดแสกนบาร์โค้ตห่อยาที่ต้องการบันทึก NTRM", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (_baleBarcode == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcodeTextBox.Text + " ในระบบ", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                var qty = NTRMQuantity.ReturnQty();
                if (qty <= 0)
                    return;

                Button btn = new Button();
                btn = (Button)sender;

                if(BusinessLayerService.BuyingBL().GetSingle(baleBarcodeTextBox.Text) == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ตนี้ในระบบ", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                /// add ntrm inspection.
                BusinessLayerService.NTRMInspectionBL().Add(new FCVBuyingEntities.NTRMInspection
                {
                    BaleBarcode = baleBarcodeTextBox.Text,
                    NTRMTypeCode = btn.ToolTip.ToString(),
                    InspectionUser = user_setting.User.Username,
                    InspectionDate = DateTime.Now,
                    Quantity = Convert.ToByte(qty)
                });

                /// refresh form
                /// 
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void baleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ntrmInspectionDataGrid.SelectedIndex < 0)
                    return;

                var model = (FCVBuyingEntities.NTRMInspection)ntrmInspectionDataGrid.SelectedItem;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning", MessageBoxButton.YesNo, 
                    MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.NTRMInspectionBL().Delete(model);
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }
    }
}
