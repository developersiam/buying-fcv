﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using FCVBuyingWPF.MVVM.View;
using FCVBuyingWPF.MVVM.ViewModel;
using Microsoft.Xaml.Behaviors.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NPIBuyingDocuments.xaml
    /// </summary>
    public partial class BuyingDocuments : Page
    {
        List<vm_BuyingDocument> _documentList;

        public BuyingDocuments()
        {
            try
            {
                InitializeComponent();

                _documentList = new List<vm_BuyingDocument>();

                /// Binding buying station by area of user.
                /// 
                List<BuyingStation> stationList = new List<BuyingStation>();
                foreach (var item in BusinessLayerService.UserAreaBL()
                .GetByUser(user_setting.User.Username))
                {
                    stationList.AddRange(Helper.BuyingStationHelper
                        .GetBuyingStationByArea(item.AreaCode));
                }

                stationComboBox.ItemsSource = null;
                stationComboBox.ItemsSource = stationList.OrderBy(x => x.StationCode);


                /// Binding extension agent by area of user.
                /// 
                List<ExtensionAgent> list = new List<ExtensionAgent>();
                foreach (var item in BusinessLayerService.UserAreaBL().GetByUser(user_setting.User.Username))
                {
                    list.AddRange(BusinessLayerService.ExtensionAgentBuyingAreaBL()
                        .GetByArea(item.AreaCode).Select(x => new ExtensionAgent
                        {
                            AreaCode = x.AreaCode,
                            ExtensionAgentCode = x.ExtensionAgentCode,
                            ExtensionAgentName = x.ExtensionAgent.ExtensionAgentName
                        }));
                }

                extensionAgentComboBox.ItemsSource = null;
                extensionAgentComboBox.ItemsSource = list.OrderBy(x => x.ExtensionAgentCode);
                createDateDatePicker.SelectedDate = DateTime.Now;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                if (createDateDatePicker.SelectedDate == null)
                    return;

                if (extensionAgentComboBox.SelectedIndex < 0)
                    return;

                if (stationComboBox.SelectedIndex < 0)
                    return;

                _documentList = Helper.BuyingDocumentHelper
                    .GetByDate(Convert.ToDateTime(createDateDatePicker.SelectedDate),
                    extensionAgentComboBox.SelectedValue.ToString(),
                    stationComboBox.SelectedValue.ToString());

                buyingDocumentDataGrid.ItemsSource = null;
                buyingDocumentDataGrid.ItemsSource = _documentList.OrderByDescending(x => x.BuyingDocumentCode);
                totalDocumentTextBox.Text = _documentList.Count().ToString("N0");
                closeBaleTextBox.Text = _documentList.Sum(x => x.CloseBales).ToString("N0");
                openBaleTextBox.Text = _documentList.Sum(x => x.OpenBales).ToString("N0");
                totalBaleTextBox.Text = _documentList.Sum(x => x.Bales).ToString("N0");
                totalWeightTextBox.Text = _documentList.Sum(x => x.TotalWeight).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (createDateDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่สร้างเอกสาร");

                if (extensionAgentComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุตัวแทน");

                if (stationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ (Buying station)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationComboBox.Focus();
                    return;
                }

                BuyingDocumentCreate window = new BuyingDocumentCreate(
                    Convert.ToDateTime(createDateDatePicker.SelectedDate),
                    stationComboBox.SelectedValue.ToString(),
                    extensionAgentComboBox.SelectedValue.ToString());

                window.ShowDialog();
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            BuyingDocumentBinding();
        }

        private void buyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BuyingDocumentBinding();
        }

        private void extensionAgentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BuyingDocumentBinding();
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                var _extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(item.FarmerCode).ExtensionAgentCode;

                if (_extensionAgentCode.Contains("NPI"))
                {
                    BuyingDocumentDetailNPI window1 = new BuyingDocumentDetailNPI(
                        new vm_BuyingDocument
                        {
                            Crop = item.Crop,
                            FarmerCode = item.FarmerCode,
                            StationCode = item.StationCode,
                            DocumentNumber = item.DocumentNumber
                        });

                    window1.ShowDialog();
                }
                else
                {
                    BuyingDocumentDetailTW window2 = new BuyingDocumentDetailTW(
                        new vm_BuyingDocument
                        {
                            Crop = item.Crop,
                            FarmerCode = item.FarmerCode,
                            StationCode = item.StationCode,
                            DocumentNumber = item.DocumentNumber
                        });
                    window2.ShowDialog();
                }

                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var model = (BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                if (model.Buyings.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบ buying document นี้ได้ " +
                        "เนื่องจากมีการบันทึกข้อมูลห่อยาเข้าไปแล้ว โปรดตรวจสอบอีกครั้ง");
                if (model.FinishStatus == true)
                    throw new ArgumentException("ไม่สามารถลบ buying document นี้ได้" +
                        " เนื่องจากถูกเปลี่ยนสถานะเป็น finish แล้ว โปรดตรวจสอบอีกครั้ง");

                BusinessLayerService.BuyingDocumentBL().Delete(model);
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                if (MessageBox.Show("ท่านต้องการบันทึกข้อมูล เลขห่อ น้ำหนัก และเกรด พร้อมกันในคราวเดียวเลย ใช่หรือไม่?",
                    "confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    var defaultGradeGroup = BusinessLayerService.BuyingGradeGroupBL()
                     .GetByCrop(user_setting.Crop.Crop1)
                     .Where(x => x.DefaultStatus == true)
                     .ToList();

                    if (defaultGradeGroup.Count() <= 0)
                        throw new Exception("ระบบยังไม่ได้ระบุ grade group default" +
                                " โปรดติดต่อผู้ดูแลระบบเพื่อดำเนินการ");

                    if (defaultGradeGroup.Count() > 1)
                        throw new Exception("มีการตั้งค่า default grade group มากกว่า 1 " +
                               " โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบข้อมูล");

                    var window = new CaptureBuyingInfo();
                    var vm = new vmCaptureBuyingInfo();
                    vm.Document = BusinessLayerService.BuyingDocumentBL()
                        .GetSingle(item.Crop,
                        item.FarmerCode,
                        item.StationCode,
                        item.DocumentNumber);
                    window.DataContext = vm;
                    window.ShowDialog();

                    //CaptureBuyingInfoNPI window = new CaptureBuyingInfoNPI(new vm_BuyingDocument
                    //{
                    //    Crop = item.Crop,
                    //    FarmerCode = item.FarmerCode,
                    //    StationCode = item.StationCode,
                    //    DocumentNumber = item.DocumentNumber
                    //});
                    //window.ShowDialog();
                }
                else
                {
                    CaptureBuyingWeight window = new CaptureBuyingWeight(new vm_BuyingDocument
                    {
                        Crop = item.Crop,
                        FarmerCode = item.FarmerCode,
                        StationCode = item.StationCode,
                        DocumentNumber = item.DocumentNumber
                    });
                    window.ShowDialog();
                }
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
