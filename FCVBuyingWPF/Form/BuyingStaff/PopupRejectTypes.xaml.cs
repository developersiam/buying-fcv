﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for RejectTypes.xaml
    /// </summary>
    public partial class PopupRejectTypes : Window
    {
        static RejectType _rejectType;
        public PopupRejectTypes()
        {
            InitializeComponent();

            _rejectType = new RejectType();

            rejectTypeItemsControl.ItemsSource = null;
            rejectTypeItemsControl.ItemsSource = BusinessLayerService.RejectTypeBL()
                .GetRejectTypes()
                .OrderBy(x => x.RejectTypeName);
        }

        public static RejectType ReturnRejectType()
        {
            PopupRejectTypes window = new PopupRejectTypes();
            window.ShowDialog();

            return _rejectType;
        }

        private void rejectTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                _rejectType = BusinessLayerService.RejectTypeBL()
                    .GetRejectType(Guid.Parse(btn.ToolTip.ToString()));

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
