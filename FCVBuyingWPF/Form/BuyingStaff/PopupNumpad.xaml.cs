﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for Numpad.xaml
    /// </summary>
    public partial class PopupNumpad : Window
    {
        public static double _double { get; set; }
        public static string _mode { get; set; }

        public PopupNumpad()
        {
            InitializeComponent();
            modeTextBlock.Text = _mode;
            displayTextBox.Text = _double.ToString();
            displayTextBox.SelectAll();
            displayTextBox.Focus();
        }

        public static double ReturnValue()
        {
            PopupNumpad window = new PopupNumpad();
            window.ShowDialog();
            return _double;
        }

        private void NumpadKeyIn(string number)
        {
            displayTextBox.Text = displayTextBox.Text + number;
        }

        private void oneButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("1");
        }

        private void twoButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("2");
        }

        private void threeButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("3");
        }

        private void fourButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("4");
        }

        private void fiveButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("5");
        }

        private void sixButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("6");
        }

        private void sevenButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("7");
        }

        private void eightButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("8");
        }

        private void nineButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("9");
        }

        private void zeroButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn("0");
        }

        private void dotButton_Click(object sender, RoutedEventArgs e)
        {
            NumpadKeyIn(".");
        }

        private void oKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Helper.RegularExpressionHelper.IsDecimalCharacter(displayTextBox.Text) == false)
                {
                    MessageBox.Show("รูปแบบตัวเลขไม่ตรงตามรูปแบบที่กำหนด โปรดตรวจสอบอีกครั้ง", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                _double = Convert.ToDouble(displayTextBox.Text);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            displayTextBox.Clear();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            _double = 0;
            this.Close();
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var test = e.Key;

                if (e.Key != Key.NumPad0 &&
                    e.Key != Key.NumPad0 &&
                    e.Key != Key.NumPad1 &&
                    e.Key != Key.NumPad2 &&
                    e.Key != Key.NumPad3 &&
                    e.Key != Key.NumPad4 &&
                    e.Key != Key.NumPad5 &&
                    e.Key != Key.NumPad6 &&
                    e.Key != Key.NumPad7 &&
                    e.Key != Key.NumPad8 &&
                    e.Key != Key.NumPad9 &&
                    e.Key != Key.Decimal &&
                    e.Key != Key.Enter &&
                    e.Key != Key.Escape)
                    return;

                if (e.Key == Key.Escape)
                    displayTextBox.Clear();

                if (e.Key == Key.NumPad0)
                    NumpadKeyIn("0");
                if (e.Key == Key.NumPad1)
                    NumpadKeyIn("1");
                if (e.Key == Key.NumPad2)
                    NumpadKeyIn("2");
                if (e.Key == Key.NumPad3)
                    NumpadKeyIn("3");
                if (e.Key == Key.NumPad4)
                    NumpadKeyIn("4");
                if (e.Key == Key.NumPad5)
                    NumpadKeyIn("5");
                if (e.Key == Key.NumPad6)
                    NumpadKeyIn("6");
                if (e.Key == Key.NumPad7)
                    NumpadKeyIn("7");
                if (e.Key == Key.NumPad8)
                    NumpadKeyIn("8");
                if (e.Key == Key.NumPad9)
                    NumpadKeyIn("9");
                if (e.Key == Key.Decimal)
                    NumpadKeyIn(".");

                if (e.Key == Key.Enter)
                {
                    if (Helper.RegularExpressionHelper.IsDecimalCharacter(displayTextBox.Text) == false)
                    {
                        MessageBox.Show("รูปแบบตัวเลขไม่ตรงตามรูปแบบที่กำหนด โปรดตรวจสอบอีกครั้ง", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    _double = Convert.ToDouble(displayTextBox.Text);

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
