﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Report;
using FCVBuyingWPF.Helper;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NPICaptureBuyingInfo.xaml
    /// </summary>
    /// 
    class BuyingInfo
    {
        public ObservableCollection<string> GradeList { get; set; }
        public string Grade { get; set; }
    }

    public partial class CaptureBuyingInfoNPI : Window
    {
        vm_BuyingDocument _document;
        Buying _buying;
        BuyingGrade _buyingGrade;
        List<Buying> _buyingList;
        List<BuyingGrade> _buyingGradeList;
        string _gradePrefix;
        string _gradeGroupCode;
        BuyingInfo _buyingInfo = new BuyingInfo();

        public CaptureBuyingInfoNPI(vm_BuyingDocument document)
        {
            try
            {
                InitializeComponent();

                _document = new vm_BuyingDocument();
                _document = document;
                _buying = new Buying();
                _buying = null;
                _buyingGrade = new BuyingGrade();
                _buyingGrade = null;
                _buyingList = new List<Buying>();
                _buyingGradeList = new List<BuyingGrade>();

                var defaultGradeGroup = BusinessLayerService.BuyingGradeGroupBL()
                         .GetByCrop(user_setting.Crop.Crop1)
                         .Where(x => x.DefaultStatus == true)
                         .ToList();

                if (defaultGradeGroup.Count() <= 0)
                    throw new Exception("ระบบยังไม่ได้ระบุ grade group default" +
                            " โปรดติดต่อผู้ดูแลระบบเพื่อดำเนินการ");

                if (defaultGradeGroup.Count() > 1)
                    throw new Exception("มีการตั้งค่า default grade group มากกว่า 1 " +
                           " โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบข้อมูล");

                _gradePrefix = "X";
                _gradeGroupCode = defaultGradeGroup.FirstOrDefault().GradeGroupCode;
                _buyingGradeList = BusinessLayerService.BuyingGradeBL()
                        .GetByGradeGroup(_gradeGroupCode);

                FarmerProfileBinding();
                BuyingDocumentBinding();
                baleBarcodeTextBox.Focus();

                var strList = new ObservableCollection<string>();
                var gradeList = BusinessLayerService.BuyingGradeBL()
                    .GetByGradeGroup(_gradeGroupCode);
                foreach (var item in gradeList)
                    strList.Add(item.Grade);
                _buyingInfo.GradeList = strList;
                DataContext = _buyingInfo;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            baleBarcodeTextBox.Clear();
            baleBarcodeTextBox.Focus();
            baleNumberTextBox.Clear();
            gradeComboBox.SelectedIndex = -1;
            buyingWeightTextBox.Clear();
            _buying = null;
        }

        private void FarmerProfileBinding()
        {
            try
            {
                var _farmerProfile = Helper.FarmerProfileHelper
                    .GetSingleByCropLiteVersion(user_setting.Crop.Crop1, _document.FarmerCode);

                if (_farmerProfile == null)
                    throw new Exception("ไม่พบข้อมูลชาวไร่รหัส " + farmerCodeTextBox.Text + " ในระบบ");

                farmerCodeTextBox.Text = _farmerProfile.CitizenID;
                generalCodeTextBox.Text = _farmerProfile.GeneralCode;
                firstNameTextBox.Text = _farmerProfile.FirstName;
                lastNameTextBox.Text = _farmerProfile.LastName;

                extensionAgentCodeTextBox.Text = _farmerProfile.ExtensionAgentCode;
                //totalQuotaTextBox.Text = _farmerProfile.Quota.ToString("N1");
                //totalSoldTextBox.Text = _farmerProfile.Sold.ToString("N1");
                //balanceTextBox.Text = (_farmerProfile.Quota - _farmerProfile.Sold).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                _document = Helper.BuyingDocumentHelper
                    .GetSingle(_document.Crop,
                    _document.FarmerCode,
                    _document.StationCode,
                    _document.DocumentNumber);

                documentCodeTextBox.Text = _document.BuyingDocumentCode;
                totalBaleTextBox.Text = _document.Bales.ToString("N0");
                totalRejectBaleTextBox.Text = _document.RejectedBales.ToString("N0");
                totalBuyingBaleTextBox.Text = _document.BuyingBales.ToString("N0");
                totalWeightTextBox.Text = _document.BuyingWeight.ToString("N1");
                finishDateTextBox.Text = _document.FinishTime.ToString();
                finishStatusCheckBox.IsChecked = _document.FinishStatus;

                sendRequestButton.IsEnabled = !_document.FinishStatus;
                cancelRequestButton.IsEnabled = _document.FinishStatus;
                printButton.IsEnabled = _document.FinishStatus;

                var list = BusinessLayerService.BuyingBL()
                    .GetByDocument(_document.Crop,
                    _document.StationCode,
                    _document.FarmerCode,
                    _document.DocumentNumber)
                    .ToList()
                    .OrderByDescending(x => x.WeightDate)
                    .ThenByDescending(x => x.WeightModifiedDate);

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = list;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                var grade = _buyingInfo.Grade;
                decimal _weight = Convert.ToDecimal(buyingWeightTextBox.Text);
                if (_weight > 70)
                {
                    if (MessageBoxHelper.Question("น้ำหนักห่อยานี้มากกว่า 70 กก." +
                        " ท่านต้องการบันทึกข้อมูลนี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_buying == null) /// Adding function.
                {
                    BusinessLayerService.BuyingBL().CaptureBuyingWeight(
                        baleBarcodeTextBox.Text,
                        Convert.ToInt32(baleNumberTextBox.Text),
                        _document.Crop,
                        _document.FarmerCode,
                        _document.StationCode,
                        _document.DocumentNumber,
                        2,
                        _weight,
                        user_setting.User.Username);

                    BusinessLayerService.BuyingBL()
                        .CaptureBuyingGrade(baleBarcodeTextBox.Text,
                        _buyingGrade.GradeGroupCode,
                        gradeComboBox.SelectedValue.ToString(),
                        user_setting.User.Username);
                }
                else /// editing function.
                {
                    if (_buying.Crop != _document.Crop ||
                            _buying.FarmerCode != _document.FarmerCode ||
                            _buying.StationCode != _document.StationCode ||
                            _buying.DocumentNumber != _document.DocumentNumber)
                        throw new Exception("ห่อยานี้ไม่ใช่ห่อยาของชาวไร่รายนี้ " + Environment.NewLine +
                            "bale barcode: " + _buying.BaleBarcode + Environment.NewLine +
                            "bale number: " + _buying.BaleNumber + Environment.NewLine +
                            "farmer code: " + _buying.FarmerCode + Environment.NewLine +
                            "farmer name: " + _buying.BuyingDocument.FarmerQuota.Farmer.Person.Prefix +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.FirstName + " " +
                           _buying.BuyingDocument.FarmerQuota.Farmer.Person.LastName + Environment.NewLine +
                            "document code: " + _buying.DocumentNumber + Environment.NewLine +
                            "create date: " + _buying.BuyingDocument.CreateDate + Environment.NewLine +
                            "weight date: " + _buying.WeightDate);
                }

                if (MessageBoxHelper.Question("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?")
                == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .ChangeBuyingWeightInfo(
                    _buying.BaleBarcode,
                    Convert.ToInt32(baleNumberTextBox.Text),
                    _buying.BaleTypeID,
                    _weight,
                    user_setting.User.Username);

                BusinessLayerService.BuyingBL()
                    .CaptureBuyingGrade(baleBarcodeTextBox.Text,
                    _buyingGrade == null ? _buying.GradeGroupCode : _buyingGrade.GradeGroupCode,
                    gradeComboBox.SelectedValue.ToString(),
                 user_setting.User.Username);

                FarmerProfileBinding();
                BuyingDocumentBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingBinding()
        {
            try
            {
                if (_buying == null)
                    return;

                if (_buying.Crop != _document.Crop ||
                    _buying.FarmerCode != _document.FarmerCode ||
                    _buying.StationCode != _document.StationCode ||
                    _buying.DocumentNumber != _document.DocumentNumber)
                {
                    MessageBoxHelper.Warning("ห่อยานี้มาจาก Buying Document หมายเลข " +
                        _buying.Crop + "-" +
                        _buying.StationCode + "-" +
                        _buying.FarmerCode + "-" +
                        _buying.DocumentNumber +
                         " ซึ่งไม่ได้อยู่ใน Buying Document เดียวกัน ไม่สามารถดำเนินการใดๆ ได้");
                    ClearForm();
                    return;
                }

                baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                gradeComboBox.SelectedValue = _buying.Grade;
                buyingWeightTextBox.Text = _buying.Weight.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void baleNumberTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                    throw new Exception("โปรดสแกนบาร์โค้ตที่ต้องการบันทึกข้อมูลก่อน");

                PopupNumpad._mode = "หมายเลขห่อ";
                var returnVal = PopupNumpad.ReturnValue();

                baleNumberTextBox.Text = returnVal.ToString();

                if (returnVal <= 0)
                    throw new Exception("Bale number ต้องมากกว่า 0");

                if (RegularExpressionHelper.IsNumericCharacter(baleNumberTextBox.Text) == false)
                    throw new Exception("Bale number จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                ///ถ้าเป็นการแก้ไขข้อมูล ไม่ต้อง Set focus ไปที่ control ตัวอื่น แต่ให้ user รอกด Save ข้อมูลเพื่อแก้ไขต่อไป
                if (_buying == null)
                {
                    gradeComboBox.SelectedIndex = -1;
                    gradeComboBox.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void baleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                    throw new Exception("โปรดระบุหมายเลขบาร์โค้ต");

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(baleBarcodeTextBox.Text))
                    throw new Exception("รหัสบาร์โค้ตจะต้องประกอบด้วยตัวเลข เครื่องหมาย - และตัวอักษรภาษาอังกฤษเท่านั้น");

                _buying = BusinessLayerService.BuyingBL().GetSingle(baleBarcodeTextBox.Text);

                if (_buying != null)
                {
                    BuyingBinding();
                    FarmerProfileBinding();
                    BuyingDocumentBinding();
                    return;
                }

                baleNumberTextBox.Text = "";
                baleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void buyingGradeTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                    throw new Exception("โปรดระบุบาร์โค้ต");

                PopupBuyingGrades._buyingGradeList = _buyingGradeList;
                PopupBuyingGrades._gradeGroupCode = _gradeGroupCode;
                PopupBuyingGrades._gradePrefix = _gradePrefix;

                var retutnVal = PopupBuyingGrades.ReturnValue();
                _buyingGrade = retutnVal;

                if (_buyingGrade == null)
                    throw new Exception("ไม่พบเกรดที่เลือก โปรดตรวจสอบหรือลองเลือกใหม่อีกครั้ง");

                _gradePrefix = _buyingGrade.Grade.Substring(0, 1);
                gradeComboBox.SelectedValue = _buyingGrade.Grade;

                ///ถ้าเป็นการแก้ไขข้อมูล ไม่ต้อง Set focus ไปที่ control ตัวอื่น แต่ให้ user รอกด Save ข้อมูลเพื่อแก้ไขต่อไป
                if (_buying == null)
                    buyingWeightTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void buyingWeightTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                    throw new Exception("โปรดสแกนบาร์โค้ตที่ต้องการบันทึกข้อมูลก่อน");

                /// buyingWeightTextBox.Clear();
                PopupNumpad._mode = "น้ำหนัก";
                var returnVal = PopupNumpad.ReturnValue();

                buyingWeightTextBox.Text = returnVal.ToString();

                if (returnVal <= 0)
                    throw new Exception("Buying Weight ต้องมากกว่า 0");

                if (Helper.RegularExpressionHelper.IsDecimalCharacter(buyingWeightTextBox.Text) == false)
                    throw new Exception("Buying Weight จะต้องเป็นตัวเลขทศนิยมเท่านั้น");

                if (_buying == null)
                    Save();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                _buying = (Buying)buyingDataGrid.SelectedItem;

                baleBarcodeTextBox.Text = _buying.BaleBarcode;
                baleNumberTextBox.Text = _buying.BaleNumber.ToString();
                gradeComboBox.SelectedValue = _buying.Grade;
                buyingWeightTextBox.Text = _buying.Weight.ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                _buying = (Buying)buyingDataGrid.SelectedItem;

                //if (_document.RequestFinishStatus == true)
                //{
                //    MessageBox.Show("Buying document นี้อยู่ในสถานะ รอการยืนยัน (request finish ไม่สามารถลบ/แก้ไขข้อมูลได้",
                //        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                if (_document.FinishStatus == true)
                {
                    MessageBoxHelper.Warning("Buying document นี้อยู่ในสถานะ " +
                        "ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");
                    return;
                }

                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL().DeleteBaleBarcode(_buying.BaleBarcode);

                FarmerProfileBinding();
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void baleBarcodeTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void sendRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_document.FinishStatus == true)
                    throw new Exception("Buying document นี้ถูกเปลี่ยนสถานะเป็น finish แล้ว");

                /// check list of the all bale barcode must be recode a buying grade every bales.
                ///

                var notRecordingGrade = _document.Buyings
                    .Where(x => x.RejectTypeID == null &&
                    x.Grade == null).Count();

                if (notRecordingGrade > 0)
                    throw new Exception("ยังมีห่อยาใน buying document นี้อีกจำนวน " + notRecordingGrade +
                        " ห่อ ที่ยังไม่ได้บันทึกเกรด โปรดตรวจสอบอีกครั้งก่อนการส่ง request");

                if (MessageBoxHelper.Question("ท่านต้องการเปลี่ยนสถานะ finish " +
                    "เพื่อเสร็จสิ้นขั้นตอนการซื้อขายใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .Finish(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                FarmerProfileBinding();
                BuyingDocumentBinding();
                ClearForm();

                MessageBoxHelper.Info("Finished successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void cancelRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_document.FinishStatus == false)
                    throw new Exception("Buying document นี้ถูกเปลี่ยนสถานะเป็น Unfinish อยู่ก่อนหน้านี้แล้ว");

                if (MessageBoxHelper.Question("ท่านต้องการยกเลิกสถานะ finish " +
                    "เพื่อกลับไปแก้ไขข้อมูลใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .UnFinish(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                BusinessLayerService.BuyingDocumentBL()
                    .CancelRequest(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                FarmerProfileBinding();
                BuyingDocumentBinding();
                ClearForm();

                MessageBoxHelper.Info("Unfinish successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_document.FinishStatus == false)
                    throw new Exception("คุณต้องทำการ finish document ก่อนจึงจะสามารถพิมพ์เวาเชอร์ได้");

                var extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(_document.FarmerCode).ExtensionAgentCode;

                if (extensionAgentCode.Contains("NPI"))
                {
                    RPTBUY001 window = new RPTBUY001(_document);
                    window.ShowDialog();
                }
                else
                {
                    if (MessageBoxHelper.Question("ท่านต้องการให้แสดงราคาซื้อในเอกสารหรือไม่? " +
                        "(Yes = แสดง / No = ไม่แสดง)") == MessageBoxResult.Yes)
                    {
                        var window = new RPTBUY002(_document, extensionAgentCode);
                        window.ShowDialog();
                    }
                    else
                    {
                        var window = new RPTBUY004(_document, extensionAgentCode);
                        window.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            FarmerProfileBinding();
            BuyingDocumentBinding();
        }

        private void rejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                    throw new Exception("โปรดสแกนหรือกรอกหมายเลขบาร์โค้ต");

                _buying = BusinessLayerService.BuyingBL().GetSingle(baleBarcodeTextBox.Text);
                if (_buying == null)
                    throw new Exception("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcodeTextBox.Text + " นี้ในระบบ");

                if (_document.RequestFinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ รอการยืนยัน (request finish ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (_document.FinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (_buying.Grade != null)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้มีเกรดซื้อแล้ว ท่านต้องการ Reject ห่อยานี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                var rejectResult = PopupRejectTypes.ReturnRejectType();
                if (rejectResult == null)
                    throw new Exception("ไม่พบข้อมูลที่เลือก โปรดลองใหม่อีกครั้ง หรือติดต่อแผนกไอที");

                BusinessLayerService.BuyingBL().RejectBale(_buying.BaleBarcode,
                    rejectResult.RejectTypeID, user_setting.User.Username);

                FarmerProfileBinding();
                BuyingDocumentBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
