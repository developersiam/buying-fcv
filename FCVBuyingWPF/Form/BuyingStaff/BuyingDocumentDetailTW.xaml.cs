﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Report;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingDocumentDetailTW.xaml
    /// </summary>
    public partial class BuyingDocumentDetailTW : Window
    {
        vm_BuyingDocument _buyingDocument;
        List<Buying> _buyingList;

        public BuyingDocumentDetailTW(vm_BuyingDocument buyingDocument)
        {
            InitializeComponent();

            _buyingList = new List<Buying>();
            _buyingDocument = new vm_BuyingDocument();
            _buyingDocument = buyingDocument;

            BuyingDocumentDataBinding();
        }

        private void BuyingDocumentDataBinding()
        {
            try
            {
                _buyingDocument = Helper.BuyingDocumentHelper
                    .GetSingle(_buyingDocument.Crop,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.StationCode,
                    _buyingDocument.DocumentNumber);

                buyingDocumentCodeTextBox.Text = _buyingDocument.BuyingDocumentCode;
                RequstStatusCheckBox.IsChecked = _buyingDocument.RequestFinishStatus;
                FinishStatusCheckBox.IsChecked = _buyingDocument.FinishStatus;

                printButton.IsEnabled = _buyingDocument.FinishStatus;
                sendRequestButton.IsEnabled = !_buyingDocument.RequestFinishStatus;
                cancelRequestButton.IsEnabled = _buyingDocument.FinishStatus == false
                    && _buyingDocument.RequestFinishStatus == true ? true : false;

                _buyingList = BusinessLayerService.BuyingBL()
                    .GetByDocument(_buyingDocument.Crop,
                    _buyingDocument.StationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.DocumentNumber)
                    .ToList();

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = _buyingList;

                totalBaleTextBox.Text = _buyingList
                    .Where(x => x.Grade != null)
                    .Count()
                    .ToString("N0");

                totalWeightTextBox.Text = _buyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight).ToString("N1");

                if (_buyingList.Where(x => x.Grade != null).Count() > 0)
                    weightPerBaleTextBox.Text = (_buyingList
                        .Where(x => x.Grade != null)
                        .Sum(x => x.Weight) /
                        _buyingList
                        .Where(x => x.Grade != null).Count())
                        .ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void sendRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buyingDocument.FinishStatus == true)
                    throw new ArgumentException("Buying document นี้ถูกเปลี่ยนสถานะเป็น finish แล้ว");

                var notRecordingGrade = _buyingList
                    .Where(x => x.RejectTypeID == null &&
                    x.Grade == null).Count();
                if (notRecordingGrade > 0)
                    throw new ArgumentException("ยังมีห่อยาใน buying document นี้อีกจำนวน " + notRecordingGrade +
                        " ห่อ ที่ยังไม่ได้บันทึกเกรด โปรดตรวจสอบอีกครั้งก่อนการส่ง request");

                if (MessageBox.Show("ท่านต้องการส่ง request ไปยัง manager เพื่อ approve การซื้อขายนี้ใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .SendRequest(new BuyingDocument
                    {
                        Crop = _buyingDocument.Crop,
                        FarmerCode = _buyingDocument.FarmerCode,
                        StationCode = _buyingDocument.StationCode,
                        DocumentNumber = _buyingDocument.DocumentNumber
                    });

                BuyingDocumentDataBinding();
                MessageBox.Show("Finished successfully.", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void cancelRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingDocumentDataBinding();
                if (_buyingDocument.RequestFinishStatus == false)
                    throw new ArgumentException("Buying document นี้ถูกเปลี่ยนสถานะเป็น cancel request อยู่ก่อนหน้านี้แล้ว");

                if (_buyingDocument.FinishStatus == true)
                    throw new ArgumentException("Buying document นี้ถูก approve โดยผู้ซื้อแล้ว ไม่สามารถยกเลิกการร้องขอได้");

                if (MessageBox.Show("ท่านต้องการยกเลิกสถานะ request เพื่อกลับไปแก้ไขข้อมูลใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .CancelRequest(_buyingDocument);

                BuyingDocumentDataBinding();
                MessageBox.Show("Unfinish successfully.", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingDocumentDataBinding();
                if (_buyingDocument.FinishStatus == false)
                    throw new ArgumentException("คุณต้องให้ manager เข้ามากด finish document ก่อนจึงจะสามารถพิมพ์เวาเชอร์ได้");

                var extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(_buyingDocument.FarmerCode).ExtensionAgentCode;

                if (extensionAgentCode.Contains("NPI"))
                {
                    var window = new RPTBUY001(_buyingDocument);
                    window.ShowDialog();
                }
                else
                {
                    if (MessageBox.Show("ท่านต้องการให้แสดงราคาซื้อในเอกสารหรือไม่? (Yes = แสดง / No = ไม่แสดง)",
                           "โปรดยืนยัน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var window = new RPTBUY002(_buyingDocument, extensionAgentCode);
                        window.ShowDialog();
                    }
                    else
                    {
                        var window = new RPTBUY004(_buyingDocument, extensionAgentCode);
                        window.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            BuyingDocumentDataBinding();
        }
    }
}
