﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingVoucherList.xaml
    /// </summary>
    public partial class BuyingVouchers : Page
    {
        public BuyingVouchers()
        {
            InitializeComponent();
            createDocumentDatePicker.SelectedDate = DateTime.Now;

            List<BuyingStation> stationList = new List<BuyingStation>();

            foreach (var item in BusinessLayerService.UserAreaBL()
                .GetByUser(user_setting.User.Username))
            {
                stationList.AddRange(Helper.BuyingStationHelper
                    .GetBuyingStationByArea(item.AreaCode));
            }

            buyingStationComboBox.ItemsSource = null;
            buyingStationComboBox.ItemsSource = stationList.OrderBy(x => x.StationCode);
        }

        private void VoucherBinding()
        {
            try
            {
                if (createDocumentDatePicker.SelectedDate == null)
                    return;

                if (buyingStationComboBox.SelectedIndex < 0)
                    return;

                buyingDocumentDataGrid.ItemsSource = null;
                buyingDocumentDataGrid.ItemsSource = Helper.BuyingDocumentHelper
                    .GetBuyingVoucherByStation(Convert.ToDateTime(createDocumentDatePicker.SelectedDate),
                    buyingStationComboBox.SelectedValue.ToString());

                totalItemsLable.Content = buyingDocumentDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            VoucherBinding();
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(item.Crop,
                    item.FarmerCode,
                    item.StationCode,
                    item.DocumentNumber);

                if (document.Buyings.Count() <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาในใบเวาเชอร์นี้ โปรดตรวจสอบอีกครั้ง");
                        
                var _extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(item.FarmerCode).ExtensionAgentCode;

                if (_extensionAgentCode.Contains("NPI") || _extensionAgentCode.Contains("TT"))
                {
                    BuyingDocumentDetailNPI window1 = new BuyingDocumentDetailNPI(
                        new vm_BuyingDocument
                        {
                            Crop = item.Crop,
                            FarmerCode = item.FarmerCode,
                            StationCode = item.StationCode,
                            DocumentNumber = item.DocumentNumber
                        });
                    window1.ShowDialog();
                }
                else
                {
                    BuyingDocumentDetailTW window2 = new BuyingDocumentDetailTW(
                        new vm_BuyingDocument
                        {
                            Crop = item.Crop,
                            FarmerCode = item.FarmerCode,
                            StationCode = item.StationCode,
                            DocumentNumber = item.DocumentNumber
                        });
                    window2.ShowDialog();
                }

                VoucherBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            VoucherBinding();
        }

        private void buyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            VoucherBinding();
        }
    }
}
