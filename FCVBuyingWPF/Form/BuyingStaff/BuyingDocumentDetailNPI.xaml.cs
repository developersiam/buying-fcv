﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Report;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingDocumentDetails.xaml
    /// </summary>
    public partial class BuyingDocumentDetailNPI : Window
    {
        vm_BuyingDocument _buyingDocument;
        List<Buying> _buyingList;

        public BuyingDocumentDetailNPI(vm_BuyingDocument buyingDocument)
        {
            try
            {
                InitializeComponent();

                _buyingList = new List<Buying>();
                _buyingDocument = new vm_BuyingDocument();
                _buyingDocument = buyingDocument;

                BuyingDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentDataBinding()
        {
            try
            {
                _buyingDocument = Helper.BuyingDocumentHelper
                     .GetSingle(_buyingDocument.Crop,
                     _buyingDocument.FarmerCode,
                     _buyingDocument.StationCode,
                     _buyingDocument.DocumentNumber);

                buyingDocumentCodeTextBox.Text = _buyingDocument.BuyingDocumentCode;
                FinishStatusCheckBox.IsChecked = _buyingDocument.FinishStatus;
                printButton.IsEnabled = _buyingDocument.FinishStatus;
                sendRequestButton.IsEnabled = !_buyingDocument.FinishStatus;
                cancelRequestButton.IsEnabled = _buyingDocument.FinishStatus;

                _buyingList = BusinessLayerService.BuyingBL()
                    .GetByDocument(_buyingDocument.Crop,
                    _buyingDocument.StationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.DocumentNumber)
                    .ToList();

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = _buyingList;

                totalBaleTextBox.Text = _buyingList.Where(x => x.Grade != null)
                    .Count().ToString("N0");

                totalWeightTextBox.Text = _buyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight).ToString("N1");

                if (_buyingList.Where(x => x.Grade != null).Count() > 0)
                    weightPerBaleTextBox.Text = (_buyingList
                        .Where(x => x.Grade != null)
                        .Sum(x => x.Weight) /
                        _buyingList
                        .Where(x => x.Grade != null).Count())
                        .ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            BuyingDocumentDataBinding();
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buyingDocument.FinishStatus == false)
                    throw new ArgumentException("คุณต้องทำการ finish document ก่อนจึงจะสามารถพิมพ์เวาเชอร์ได้");

                var extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(_buyingDocument.FarmerCode).ExtensionAgentCode;

                if (extensionAgentCode.Contains("NPI"))
                {
                    var window = new RPTBUY001(_buyingDocument);
                    window.ShowDialog();
                }
                else
                {
                    if(MessageBox.Show("ท่านต้องการให้แสดงราคาซื้อในเอกสารหรือไม่? (Yes = แสดง / No = ไม่แสดง)",
                        "โปรดยืนยัน",MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var window = new RPTBUY002(_buyingDocument, extensionAgentCode);
                        window.ShowDialog();
                    }
                    else
                    {
                        var window = new RPTBUY004(_buyingDocument, extensionAgentCode);
                        window.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void cancelRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buyingDocument.FinishStatus == false)
                    throw new ArgumentException("Buying document นี้ถูกเปลี่ยนสถานะเป็น Unfinish อยู่ก่อนหน้านี้แล้ว");

                if (MessageBox.Show("ท่านต้องการยกเลิกสถานะ finish เพื่อกลับไปแก้ไขข้อมูลใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .UnFinish(_buyingDocument);

                BuyingDocumentDataBinding();
                MessageBox.Show("Unfinish successfully.", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void sendRequestButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buyingDocument.FinishStatus == true)
                    throw new ArgumentException("Buying document นี้ถูกเปลี่ยนสถานะเป็น finish แล้ว");

                var notRecordingGrade = _buyingList
                    .Where(x => x.RejectTypeID == null &&
                    x.Grade == null).Count();

                if (notRecordingGrade > 0)
                    throw new ArgumentException("ยังมีห่อยาใน buying document นี้อีกจำนวน " + notRecordingGrade +
                        " ห่อ ที่ยังไม่ได้บันทึกเกรด โปรดตรวจสอบอีกครั้งก่อนการส่ง request");

                if (MessageBox.Show("ท่านต้องการเปลี่ยนสถานะ finish เพื่อเสร็จสิ้นขั้นตอนการซื้อขายใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .Finish(new BuyingDocument
                    {
                        Crop = _buyingDocument.Crop,
                        FarmerCode = _buyingDocument.FarmerCode,
                        StationCode = _buyingDocument.StationCode,
                        DocumentNumber = _buyingDocument.DocumentNumber
                    });

                BuyingDocumentDataBinding();

                MessageBox.Show("Finished successfully.", "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
