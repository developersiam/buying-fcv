﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NTRMQuantity.xaml
    /// </summary>
    public partial class NTRMQuantity : Window
    {
        public static int _qty { get; set; }

        public NTRMQuantity()
        {
            InitializeComponent();
            _qty = 1;
        }

        public static int ReturnQty()
        {
            NTRMQuantity window = new NTRMQuantity();
            window.ShowDialog();
            return _qty;
        }

        private void increaseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _qty = Convert.ToInt16(ntrmQuantityTextBox.Text) + 1;
                ntrmQuantityTextBox.Text = _qty.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void decreaseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _qty = Convert.ToInt16(ntrmQuantityTextBox.Text)-1;
                if(_qty < 1)
                {
                    MessageBox.Show("จำนวน NTRM ที่จะบันทึกไม่ควรน้อยกว่า 1", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ntrmQuantityTextBox.Text = _qty.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
