﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using FCVBuyingWPF.MVVM.View;
using FCVBuyingWPF.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NPIBuyingDocumentCreate.xaml
    /// </summary>
    public partial class BuyingDocumentCreate : Window
    {
        string _extensionAgentCode;
        string _buyingStation;
        DateTime _createDocumentDate;
        List<vm_BuyingDocument> _documentList;

        public BuyingDocumentCreate(DateTime createDocumentDate, string buyingStation, string extentionAgentCode)
        {
            try
            {
                InitializeComponent();

                _documentList = new List<vm_BuyingDocument>();
                _buyingStation = buyingStation;
                _extensionAgentCode = extentionAgentCode;
                _createDocumentDate = createDocumentDate;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerProfileBinding(string farmerCode)
        {
            try
            {
                if (farmerCode == "")
                    throw new ArgumentException("โปรดระบุ farmer code");

                var _farmerProfile = Helper.FarmerProfileHelper
                    .GetSingleByCrop(user_setting.Crop.Crop1, farmerCode);
                if (_farmerProfile == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + farmerCodeTextBox.Text + " ในระบบ");

                if (_farmerProfile.ExtensionAgentCode != _extensionAgentCode)
                    throw new ArgumentException("ชาวไร่รหัส " + farmerCodeTextBox.Text + "ลงทะเบียนไว้แล้วกับ " + _farmerProfile.ExtensionAgentCode +
                        " แต่ไม่ได้ลงทะเบียนไว้กับ " + _extensionAgentCode);

                citizenIDTextBox.Text = _farmerProfile.CitizenID;
                prefixTextBox.Text = _farmerProfile.Prefix;
                firstNameTextBox.Text = _farmerProfile.FirstName;
                lastNameTextBox.Text = _farmerProfile.LastName;

                extensionAgentCodeTextBox.Text = _farmerProfile.ExtensionAgentCode;
                contractCodeTextBox.Text = _farmerProfile.ContractCode;
                generalCodeTextBox.Text = _farmerProfile.GeneralCode;
                registerDateDatePicker.SelectedDate = _farmerProfile.RegisterDate;
                quotaTextBox.Text = _farmerProfile.Quota.ToString("N1");
                soldTextBox.Text = _farmerProfile.Sold.ToString("N1");
                balanceTextBox.Text = (_farmerProfile.Quota - _farmerProfile.Sold).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                //_documentList = Helper.BuyingDocumentHelper
                //    .GetByFarmerAndStation(user_setting.Crop.Crop1,
                //    farmerCodeTextBox.Text,
                //    _buyingStation);

                //buyingDocumentDataGrid.ItemsSource = null;
                //buyingDocumentDataGrid.ItemsSource = _documentList.OrderByDescending(x => x.BuyingDocumentCode);
                //totalItemLabel.Content = _documentList.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            farmerCodeTextBox.Clear();
            farmerCodeTextBox.Focus();
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var searchResult = PopupSearchFarmers.ReturnFarmerCode(_extensionAgentCode);
                if (searchResult == null)
                    throw new ArgumentException("ไม่พบข้อมูล farmer code ดังกล่าวจากการ search");

                farmerCodeTextBox.Text = searchResult;
                FarmerProfileBinding(searchResult);
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (farmerCodeTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุรหัสชาวไร่ (Farmer code)");

                if (_documentList.Where(x => x.Bales <= 0).Count() >= 1)
                    throw new ArgumentException("ยังมี Buying document บางรายการที่ยังไม่ได้บันทึกข้อมูลห่อยา ระบบยังไม่อนุญาตให้สร้างใหม่ โปรดตรวจสอบอีกครั้ง");

                if (_documentList.Where(x => x.FinishStatus == false).Count() >= 1)
                    throw new ArgumentException("ยังมี Buying document บางรายการที่ยังไม่ได้เปลี่ยนสถานะเป็น Finish โปรดตรวจสอบอีกครั้ง");

                var newDocument = BusinessLayerService.BuyingDocumentBL()
                    .Add(user_setting.Crop.Crop1,
                    farmerCodeTextBox.Text,
                    _buyingStation,
                    _createDocumentDate,
                    user_setting.User.Username);

                if (MessageBox.Show("ท่านต้องการบันทึกข้อมูล เลขห่อ น้ำหนัก และเกรด พร้อมกันในคราวเดียวเลย ใช่หรือไม่?",
                    "confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    var defaultGradeGroup = BusinessLayerService.BuyingGradeGroupBL()
                     .GetByCrop(user_setting.Crop.Crop1)
                     .Where(x => x.DefaultStatus == true)
                     .ToList();

                    if (defaultGradeGroup.Count() <= 0)
                        throw new Exception("ระบบยังไม่ได้ระบุ grade group default" +
                                " โปรดติดต่อผู้ดูแลระบบเพื่อดำเนินการ");

                    if (defaultGradeGroup.Count() > 1)
                        throw new Exception("มีการตั้งค่า default grade group มากกว่า 1 " +
                               " โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบข้อมูล");

                    var window = new CaptureBuyingInfo();
                    var vm = new vmCaptureBuyingInfo();
                    vm.Document = BusinessLayerService.BuyingDocumentBL()
                        .GetSingle(newDocument.Crop,
                        newDocument.FarmerCode,
                        newDocument.StationCode,
                        newDocument.DocumentNumber);
                    window.DataContext = vm;
                    window.ShowDialog();
                }
                else
                {
                    CaptureBuyingWeight window = new CaptureBuyingWeight(new vm_BuyingDocument
                    {
                        Crop = newDocument.Crop,
                        FarmerCode = newDocument.FarmerCode,
                        StationCode = newDocument.StationCode,
                        DocumentNumber = newDocument.DocumentNumber
                    });
                    window.ShowDialog();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerIDTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                FarmerProfileBinding(farmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (buyingDocumentDataGrid.SelectedIndex < 0)
                //    return;

                //if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                //    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                //    return;

                //var model = (BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                //if (model.Buyings.Count() > 0)
                //{
                //    MessageBox.Show("ไม่สามารถลบ buying document นี้ได้ เนื่องจากมีการบันทึกข้อมูลห่อยาเข้าไปแล้ว โปรดตรวจสอบอีกครั้ง",
                //        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}
                //if (model.FinishStatus == true)
                //{
                //    MessageBox.Show("ไม่สามารถลบ buying document นี้ได้ เนื่องจากถูกเปลี่ยนสถานะเป็น finish แล้ว โปรดตรวจสอบอีกครั้ง",
                //        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                //BusinessLayerService.BuyingDocumentBL().Delete(model);
                //FarmerProfileBinding(farmerCodeTextBox.Text);
                //BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (buyingDocumentDataGrid.SelectedIndex < 0)
                //    return;

                //var item = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;

                //NPICaptureBuyingInfo window = new NPICaptureBuyingInfo(item);
                //window.ShowDialog();

                //BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            FarmerProfileBinding(farmerCodeTextBox.Text);
        }
    }
}
