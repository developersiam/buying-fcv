﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for TransportationDocuments.xaml
    /// </summary>
    public partial class TransportationDocuments : Page
    {
        List<vm_TransportationDocument> _transportationDocumentList;

        public TransportationDocuments()
        {
            try
            {
                InitializeComponent();

                _transportationDocumentList = new List<vm_TransportationDocument>();
                createDocumentDatePicker.SelectedDate = DateTime.Now;
                var userAreaList = BusinessLayerService.UserAreaBL()
                    .GetByUser(user_setting.User.Username);
                var areaList = new List<Area>();
                foreach (var item in userAreaList)
                    areaList.Add(item.Area);
                areaComboBox.ItemsSource = areaList
                    .OrderBy(x => x.AreaName)
                    .ToList();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDataGrid()
        {
            try
            {
                if (createDocumentDatePicker.SelectedDate == null)
                    return;

                _transportationDocumentList = Helper.TransportationHelper.
                    GetByCreateDate(Convert.ToDateTime(createDocumentDatePicker.SelectedDate))
                    .ToList();

                var transportList = new List<vm_TransportationDocument>();
                foreach (var item in user_setting.User.UserAreas)
                    foreach (var transport in _transportationDocumentList
                        .Where(x => x.TransportationDocument.AreaCode == item.AreaCode))
                        transportList.Add(transport);

                transportationDataGrid.ItemsSource = null;
                transportationDataGrid.ItemsSource = transportList
                    .OrderByDescending(t => t.TransportationCode)
                    .ToList();

                totalRecordTextBox.Text = string.Format("{0:N0}", _transportationDocumentList.Count());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocument()
        {
            try
            {
                if (createDocumentDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("Please select a create document date.",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    createDocumentDatePicker.Focus();
                    return;
                }

                if (truckNumberTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุทะเบียนรถ", "warning",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    truckNumberTextBox.Focus();
                    return;
                }

                if (areaComboBox.SelectedIndex == -1)
                {
                    MessageBox.Show("โปรดระบุพื้นที่ (Area Code)", "warning",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    areaComboBox.Focus();
                    return;
                }

                BusinessLayerService.TransportationDocumentBL()
                    .Add(user_setting.Crop.Crop1,
                    truckNumberTextBox.Text.Replace(" ", string.Empty),
                    Convert.ToDateTime(createDocumentDatePicker.SelectedDate),
                    areaComboBox.SelectedValue.ToString(),
                    user_setting.User.Username);

                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void truckNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    CreateDocument();
                    truckNumberTextBox.Text = "";
                    truckNumberTextBox.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void createDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            CreateDocument();
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
            truckNumberTextBox.Text = "";
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDataGrid.SelectedIndex < 0)
                    return;

                var item = (vm_TransportationDocument)transportationDataGrid.SelectedItem;

                TransportationDetails window = new TransportationDetails(item.TransportationCode);
                window.ShowDialog();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "Warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var item = (vm_TransportationDocument)transportationDataGrid.SelectedItem;

                // Delete item.
                BusinessLayerService.TransportationDocumentBL().Delete(item.TransportationCode);

                ReloadDataGrid();
                truckNumberTextBox.Text = "";
                truckNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
