﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingDocuments.xaml
    /// </summary>
    public partial class _BuyingDocuments : Page
    {
        List<vm_BuyingDocument> _documentList;
        public _BuyingDocuments()
        {
            InitializeComponent();

            _documentList = new List<vm_BuyingDocument>();
            List<BuyingStation> stationList = new List<BuyingStation>();

            foreach (var item in BusinessLayerService.UserAreaBL()
                .GetByUser(user_setting.User.Username))
            {
                stationList.AddRange(Helper.BuyingStationHelper
                    .GetBuyingStationByArea(item.AreaCode));
            }

            stationComboBox.ItemsSource = null;
            stationComboBox.ItemsSource = stationList.OrderBy(x => x.StationCode);

            List<ExtensionAgent> list = new List<ExtensionAgent>();

            foreach (var item in BusinessLayerService.UserAreaBL()
                .GetByUser(user_setting.User.Username))
            {
                list.AddRange(BusinessLayerService.ExtensionAgentBL()
                    .GetByArea(item.AreaCode));
            }

            extensionAgentComboBox.ItemsSource = null;
            extensionAgentComboBox.ItemsSource = list.OrderBy(x => x.ExtensionAgentCode);
        }

        private void ClearForm()
        {
            try
            {
                farmerCodeSearchTextBox.Text = "";
                farmerCodeSearchTextBox.Focus();

                citizenIDTextBox.Text = "";
                prefixTextBox.Text = "";
                firstNameTextBox.Text = "";
                lastNameTextBox.Text = "";

                extensionAgentCodeTextBox.Text = "";
                contractCodeTextBox.Text = "";
                registerDateDatePicker.SelectedDate = DateTime.Now;
                quotaTextBox.Text = "";
                soldTextBox.Text = "";
                balanceTextBox.Text = "";

                buyingDocumentDataGrid.ItemsSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshForm()
        {
            try
            {
                FarmerProfileBinding(farmerCodeSearchTextBox.Text);
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerProfileBinding(string farmerCode)
        {
            try
            {
                if (farmerCode == "")
                {
                    MessageBox.Show("โปรดระบุ farmer code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerCodeSearchTextBox.Focus();
                    return;
                }

                var _farmerProfile = Helper.FarmerProfileHelper
                    .GetSingleByCrop(user_setting.Crop.Crop1, farmerCode);

                if (_farmerProfile == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลชาวไร่รหัส " + farmerCodeSearchTextBox.Text + " ในระบบ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerCodeSearchTextBox.Text = "";
                    farmerCodeSearchTextBox.Focus();
                    return;
                }

                citizenIDTextBox.Text = _farmerProfile.CitizenID;
                prefixTextBox.Text = _farmerProfile.Prefix;
                firstNameTextBox.Text = _farmerProfile.FirstName;
                lastNameTextBox.Text = _farmerProfile.LastName;

                extensionAgentCodeTextBox.Text = _farmerProfile.ExtensionAgentCode;
                contractCodeTextBox.Text = _farmerProfile.ContractCode;
                registerDateDatePicker.SelectedDate = _farmerProfile.RegisterDate;
                quotaTextBox.Text = _farmerProfile.Quota.ToString("N1");
                soldTextBox.Text = _farmerProfile.Sold.ToString("N1");
                balanceTextBox.Text = (_farmerProfile.Quota - _farmerProfile.Sold).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                if (stationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ (Buying station)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationComboBox.Focus();
                    return;
                }

                _documentList = Helper.BuyingDocumentHelper
                    .GetByFarmerAndStation(user_setting.Crop.Crop1,
                    farmerCodeSearchTextBox.Text,
                    stationComboBox.SelectedValue.ToString());

                buyingDocumentDataGrid.ItemsSource = null;
                buyingDocumentDataGrid.ItemsSource = _documentList.OrderByDescending(x => x.BuyingDocumentCode);
                totalItemsLable.Content = _documentList.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var searchResult = PopupSearchFarmers.ReturnFarmerCode(extensionAgentComboBox.SelectedValue.ToString());
                if (searchResult == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล farmer code ดังกล่าวจากการ search",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                farmerCodeSearchTextBox.Text = searchResult;
                FarmerProfileBinding(searchResult);
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void farmerCodeSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (farmerCodeSearchTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกข้อมูล farmer code",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerCodeSearchTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(farmerCodeSearchTextBox.Text))
                {
                    MessageBox.Show("รูปแบบรหัสชาวไร่ไม่ถูกต้อง โปรดตรวจสอบใหม่อีกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerCodeSearchTextBox.Text = "";
                    farmerCodeSearchTextBox.Focus();
                    return;
                }

                FarmerProfileBinding(farmerCodeSearchTextBox.Text);
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        //private void refreshButton_Click(object sender, RoutedEventArgs e)
        //{
        //    RefreshForm();
        //}

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (stationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ (Buying station)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    stationComboBox.Focus();
                    return;
                }

                if (farmerCodeSearchTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสชาวไร่ (Farmer code)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    farmerCodeSearchTextBox.Focus();
                    return;
                }

                if (_documentList.Where(x => x.Bales <= 0).Count() >= 1)
                {
                    MessageBox.Show("ยังมี Buying document บางรายการที่ยังไม่ได้บันทึกข้อมูลห่อยา ระบบยังไม่อนุญาตให้สร้างใหม่ โปรดตรวจสอบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_documentList.Where(x => x.FinishStatus == false).Count() >= 1)
                {
                    MessageBox.Show("ยังมี Buying document บางรายการที่ยังไม่ได้เปลี่ยนสถานะเป็น Finish โปรดตรวจสอบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BusinessLayerService.BuyingDocumentBL().Add(user_setting.Crop.Crop1,
                    farmerCodeSearchTextBox.Text,
                    stationComboBox.SelectedValue.ToString(),
                    user_setting.User.Username);

                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var model = (BuyingDocument)buyingDocumentDataGrid.SelectedItem;
                if (model.Buyings.Count() > 0)
                {
                    MessageBox.Show("ไม่สามารถลบ buying document นี้ได้ เนื่องจากมีการบันทึกข้อมูลห่อยาเข้าไปแล้ว โปรดตรวจสอบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (model.FinishStatus == true)
                {
                    MessageBox.Show("ไม่สามารถลบ buying document นี้ได้ เนื่องจากถูกเปลี่ยนสถานะเป็น finish แล้ว โปรดตรวจสอบอีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BusinessLayerService.BuyingDocumentBL().Delete(model);
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingDocumentDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (buyingDocumentDataGrid.SelectedIndex < 0)
                    return;

                var model = (vm_BuyingDocument)buyingDocumentDataGrid.SelectedItem;

                CaptureBuyingWeight window = new CaptureBuyingWeight(model);
                window.ShowDialog();
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void stationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (stationComboBox.SelectedIndex < 0)
                    return;

                if (farmerCodeSearchTextBox.Text != "")
                    RefreshForm();
                else
                     extensionAgentComboBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
