﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.BuyingStaff
{
    /// <summary>
    /// Interaction logic for FarmerQuota.xaml
    /// </summary>
    public partial class FarmerQuota : Window
    {
        Farmer _farmer;
        public FarmerQuota(Farmer farmer)
        {
            InitializeComponent();
            _farmer = new Farmer();
            _farmer = farmer;
            FarmerProfileDataBinding();
        }

        private void FarmerProfileDataBinding()
        {
            try
            {
                citizenIDTextBox.Text = _farmer.Person.CitizenID;
                prefixNameTextBox.Text = _farmer.Person.Prefix;
                firstNameTextBox.Text = _farmer.Person.FirstName;
                lastNameTextBox.Text = _farmer.Person.LastName;
                birthDateDatePicker.SelectedDate = _farmer.Person.BirthDate;
                mooTextBox.Text = _farmer.Person.Village;
                homeNumberTextBox.Text = _farmer.Person.HouseNumber;
                subDistrictTextBox.Text = _farmer.Person.Tumbon;
                districtTextBox.Text = _farmer.Person.Amphur;
                provinceTextBox.Text = _farmer.Person.Province;
                phoneNumberTextBox.Text = _farmer.Person.TelephoneNumber;
                farmerCodeTextBox.Text = _farmer.FarmerCode;
                extensionAgentCodeTextBox.Text = _farmer.ExtensionAgentCode;

                var farmerQuota = BusinessLayerService.FarmerQuotaBL()
                    .GetSingle(user_setting.Crop.Crop1, _farmer.FarmerCode);

                cropTextBox.Text = farmerQuota.Crop.ToString();
                generalCodeTextBox.Text = farmerQuota.GeneralCode;
                contractCodeTextBox.Text = farmerQuota.ContractCode;
                quotaTextBox.Text = farmerQuota.Quota.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            FarmerProfileDataBinding();
        }
    }
}
