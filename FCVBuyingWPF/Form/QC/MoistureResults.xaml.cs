﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.Form.QC
{
    /// <summary>
    /// Interaction logic for MoistureResults.xaml
    /// </summary>
    public partial class MoistureResults : Page
    {
        Buying _baleBarcode;
        public MoistureResults()
        {
            InitializeComponent();

            _baleBarcode = new Buying();
            baleBarcodeTextBox.Focus();
        }

        private void ClearFrom()
        {
            _baleBarcode = null;
            baleBarcodeTextBox.Text = "";
            farmerCodeTextBox.Text = "";
            farmerNameTextBox.Text = "";
            baleNumberTextBox.Text = "";
            weightDateTextBox.Text = "";
            gradeDateTextBox.Text = "";
            rejectTypeTextBox.Text = "";
            resultDateTextBox.Text = "";
            resultTextBox.Text = "";
            weightTextBox.Text = "";
            gradeTextBox.Text = "";
            baleBarcodeTextBox.Focus();
        }

        private void BaleBarcodeDataBinding(string baleBarcode)
        {
            try
            {
                _baleBarcode = BusinessLayerService.BuyingBL().GetSingle(baleBarcode);
                if (_baleBarcode == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยานี้ในระบบ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Text = "";
                    baleBarcodeTextBox.Focus();
                    return;
                }

                var _farmerProfile = BusinessLayerService.FarmerBL()
                    .GetSingle(_baleBarcode.FarmerCode);
                farmerNameTextBox.Text = _farmerProfile.Person.Prefix +
                    _farmerProfile.Person.FirstName + " " +
                    _farmerProfile.Person.LastName;
                farmerCodeTextBox.Text = _baleBarcode.FarmerCode;
                baleNumberTextBox.Text = _baleBarcode.BaleNumber.ToString();
                weightTextBox.Text = _baleBarcode.Weight.ToString("N1");
                weightDateTextBox.Text = _baleBarcode.WeightDate.ToShortDateString();
                gradeTextBox.Text = _baleBarcode.Grade;
                gradeDateTextBox.Text = _baleBarcode.GradeDate.ToString();
                rejectTypeTextBox.Text = _baleBarcode.RejectTypeID == null ? null : _baleBarcode.RejectType.RejectTypeName;
                resultTextBox.Text = _baleBarcode.MoistureResult.ToString();
                resultDateTextBox.Text = _baleBarcode.MoistureResultDate.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DataGrideBinding()
        {
            try
            {
                if (resultDateDatePicker.SelectedDate == null)
                    return;

                var list = BusinessLayerService.BuyingBL()
                    .GetMoistureResultByResultDate((DateTime)resultDateDatePicker.SelectedDate);

                buyingDataGrid.ItemsSource = null;
                buyingDataGrid.ItemsSource = list.OrderByDescending(x => x.MoistureResultDate);
                totalRecordTextBox.Text = list.Count().ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save()
        {
            try
            {
                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสบาร์โค้ต (bale barcode)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (resultTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุค่าความชื้นที่ได้ (mosture result)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    resultTextBox.Focus();
                    return;
                }

                if (!Helper.RegularExpressionHelper.IsDecimalCharacter(resultTextBox.Text))
                {
                    MessageBox.Show("ค่าความชื้นจะต้องเป็นค่าตัวเลขเท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    resultTextBox.Focus();
                    return;
                }

                DateTime inputDate = Convert.ToDateTime(resultDateDatePicker.SelectedDate);
                inputDate = inputDate.AddHours(DateTime.Now.Hour);
                inputDate = inputDate.AddMinutes(DateTime.Now.Minute);
                inputDate = inputDate.AddSeconds(DateTime.Now.Second);

                BusinessLayerService.BuyingBL()
                    .AddOrEditMoistureResult(baleBarcodeTextBox.Text,
                    Convert.ToDecimal(resultTextBox.Text),
                    inputDate,
                    user_setting.User.Username);

                ClearFrom();
                DataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                var model = (Buying)buyingDataGrid.SelectedItem;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .RemoveMoistureResult(model.BaleBarcode,
                    user_setting.User.Username);

                ClearFrom();
                DataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void resultDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrideBinding();
        }

        private void baleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสบาร์โค้ต (bale barcode)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                BaleBarcodeDataBinding(baleBarcodeTextBox.Text);
                resultTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void resultTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void buyingDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (buyingDataGrid.SelectedIndex < 0)
                    return;

                _baleBarcode = (Buying)buyingDataGrid.SelectedItem;
                BaleBarcodeDataBinding(_baleBarcode.BaleBarcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show("warning: " + ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
