﻿using FCVBuyingWPF.Helper;
using FCVBuyingWPF.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF.MVVM.View
{
    /// <summary>
    /// Interaction logic for CaptureGrade.xaml
    /// </summary>
    public partial class CaptureGrade : Page
    {
        public CaptureGrade()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            BaleBarcodeTextBox.Focus();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            try
            {
                var vm = (vmCaptureGrade)DataContext;
                switch (e.PropertyName)
                {
                    case nameof(vm.BaleBarcode):
                        BaleBarcodeTextBox.SelectAll();
                        BaleBarcodeTextBox.Focus();
                        break;
                    case nameof(vm.TransportationCode):
                        TransportationComboBox.Focus();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
