﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Admin;
using FCVBuyingWPF.Form.BuyingStaff;
using FCVBuyingWPF.Helper;
using FCVBuyingWPF.Model;
using FCVBuyingWPF.MVVM.View;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Input.StylusPlugIns;

namespace FCVBuyingWPF.MVVM.ViewModel
{
    public class vmCaptureGrade : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmCaptureGrade(short crop)
        {
            try
            {
                if (user_setting.User == null)
                    throw new Exception("โปรดทำการ login เข้าสู่ระบบก่อน");

                _transportationCreateDate = DateTime.Now;
                _isUseTransportOption = false;
                RaisePropertyChangedEvent(nameof(TransportationCreateDate));
                RaisePropertyChangedEvent(nameof(IsUseTransportOption));
                TransportationListBinding();
                GradeListBinding(crop);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region Properties
        private string _gradeGroupCode;

        public string GradeGroupCode
        {
            get { return _gradeGroupCode; }
            set { _gradeGroupCode = value; }
        }

        private Farmer _farmer;

        public Farmer Farmer
        {
            get { return _farmer; }
            set { _farmer = value; }
        }

        private FCVBuyingEntities.FarmerQuota _quota;

        public FCVBuyingEntities.FarmerQuota Quota
        {
            get { return _quota; }
            set { _quota = value; }
        }

        private BuyingDocument _document;

        public BuyingDocument Document
        {
            get { return _document; }
            set { _document = value; }
        }

        private Buying _buying;

        public Buying Buying
        {
            get { return _buying; }
            set { _buying = value; }
        }

        private int _totalBale;

        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private int _rejectedBale;

        public int RejectedBale
        {
            get { return _rejectedBale; }
            set { _rejectedBale = value; }
        }

        private int _buyingBale;

        public int BuyingBale
        {
            get { return _buyingBale; }
            set { _buyingBale = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private string _baleBarcode;

        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private string _grade;

        public string Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }

        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private bool _finistStatus;

        public bool FinishStatus
        {
            get { return _finistStatus; }
            set { _finistStatus = value; }
        }

        private TransportationDocument _transportationSelectedItem;

        public TransportationDocument TransportationSelectedItem
        {
            get { return _transportationSelectedItem; }
            set { _transportationSelectedItem = value; }
        }


        private string _transportationCode;

        public string TransportationCode
        {
            get { return _transportationCode; }
            set
            {
                _transportationCode = value;
                if (string.IsNullOrEmpty(_transportationCode))
                    return;
                _transportationSelectedItem = _transportationList
                    .SingleOrDefault(x => x.TransportationCode == _transportationCode);
                RaisePropertyChangedEvent(nameof(TransportationSelectedItem));
            }
        }

        private DateTime? _transportationCreateDate;

        public DateTime? TransportationCreateDate
        {
            get { return _transportationCreateDate; }
            set
            {
                _transportationCreateDate = value;
                _transportationCode = null;
                _transportationSelectedItem = null;
                RaisePropertyChangedEvent(nameof(TransportationCode));
                RaisePropertyChangedEvent(nameof(TransportationSelectedItem));
                TransportationListBinding();
            }
        }

        private bool _isUseTransportOption;

        public bool IsUseTransportOption
        {
            get { return _isUseTransportOption; }
            set
            {
                _isUseTransportOption = value;
                RaisePropertyChangedEvent(nameof(IsUseTransportOption));
            }
        }
        #endregion


        #region List
        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }

        private List<BuyingGrade> _gradeList;

        public List<BuyingGrade> GradeList
        {
            get { return _gradeList; }
            set { _gradeList = value; }
        }

        private List<BuyingGrade> _selectGradeList;

        public List<BuyingGrade> SelectGradeList
        {
            get { return _selectGradeList; }
            set { _selectGradeList = value; }
        }

        private List<TransportationDocument> _transportationList;

        public List<TransportationDocument> TransportationList
        {
            get { return _transportationList; }
            set { _transportationList = value; }
        }
        #endregion


        #region Command
        private ICommand _onBaleBarcodeEnterCommand;

        public ICommand OnBaleBarcodeEnterCommand
        {
            get { return _onBaleBarcodeEnterCommand ?? (_onBaleBarcodeEnterCommand = new RelayCommand(OnBaleBarcodeEnter)); }
            set { _onBaleBarcodeEnterCommand = value; }
        }

        private void OnBaleBarcodeEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(BaleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนหรือป้อนบาร์โค้ตในช่อง barcode");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                _buying = BusinessLayerService.BuyingBL()
                    .GetSingle(_baleBarcode);
                if (_buying == null)
                    throw new Exception("ไม่พบข้อมูลยาห่อนี้ในระบบ " +
                        "ห่อยาจะต้องผ่านการบันทึกน้ำหนักมาแล้วก่อนหน้านี้");

                RaisePropertyChangedEvent(nameof(Buying));
                BuyingBinding();
                DocumentBinding();
                FarmerInfoBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBaleBarcodeClickCommand;

        public ICommand OnBaleBarcodeClickCommand
        {
            get { return _onBaleBarcodeClickCommand ?? (_onBaleBarcodeClickCommand = new RelayCommand(OnBaleBarcodeClick)); }
            set { _onBaleBarcodeClickCommand = value; }
        }

        private void OnBaleBarcodeClick(object obj)
        {
            ClearForm();
        }

        private ICommand _onClearButtonClickCommand;

        public ICommand OnClearButtonClickCommand
        {
            get { return _onClearButtonClickCommand ?? (_onClearButtonClickCommand = new RelayCommand(OnClearButtonClick)); }
            set { _onClearButtonClickCommand = value; }
        }

        private void OnClearButtonClick(object obj)
        {
            ClearForm();
        }

        private ICommand _onRemoveGradeButtonClickCommand;

        public ICommand OnRemoveGradeButtonClickCommand
        {
            get { return _onRemoveGradeButtonClickCommand ?? (_onRemoveGradeButtonClickCommand = new RelayCommand(OnRemoveGradeButtonClick)); }
            set { _onRemoveGradeButtonClickCommand = value; }
        }

        private void OnRemoveGradeButtonClick(object obj)
        {
            try
            {
                if (_buying == null)
                    throw new Exception("โปรดสแกนบาร์โค้ตเพื่อตรวจสอบข้อมูลห่อยาที่ต้องการยกเลิกเกรดก่อน");

                if (_document.FinishStatus)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขาย ไม่สามารถยกเลิกเกรดซื้อได้");

                if (string.IsNullOrEmpty(_buying.Grade))
                    throw new Exception("ยาห่อนี้ยังไม่มีการบันทึกเกรดซื้อ ท่านสามารถเลือกเกรดที่ต้องการได้เลย");

                if (MessageBoxHelper.Question("ท่านต้องการยกเลิกเกรดซื้อยาห่อนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .RemoveBuyingGrade(_buying.BaleBarcode, user_setting.User.Username);

                BuyingBinding();
                DocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRejectionButtonClickCommand;

        public ICommand OnRejectionButtonClickCommand
        {
            get { return _onRejectionButtonClickCommand ?? (_onRejectionButtonClickCommand = new RelayCommand(OnRejectionButtonClick)); }
            set { _onRejectionButtonClickCommand = value; }
        }

        private void OnRejectionButtonClick(object obj)
        {
            try
            {
                if (_buying == null)
                    throw new Exception("โปรดสแกนบาร์โค้ตก่อนทุกครั้งเมื่อต้องการ reject" +
                        " เพื่อตรวจสอบข้อมูลห่อยาดังกล่าวก่อนบันทึกข้อมูล" +
                        " (Buying model cannot be null.)");

                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะถูกยืนยันและปิดการซื้อขายแล้ว");

                if (_buying.Grade != null)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้มีเกรดซื้อแล้ว ท่านต้องการ Reject ห่อยานี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                var window = new RejectPopup();
                var vm = new vmRejectPopup();
                vm.Window = window;
                window.DataContext = vm;
                window.ShowDialog();
                if (vm.RejectType == null)
                    throw new Exception("โปรดระบุประเภทของการ Reject");

                if (MessageBoxHelper.Question("ท่านต้องการ reject ห่อยานี้โดยระบุให้เป็น " +
                    vm.RejectType.RejectTypeName +
                    " ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .RejectBale(_buying.BaleBarcode,
                    vm.RejectType.RejectTypeID,
                    user_setting.User.Username);

                DocumentBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDocumentDetailsButtonClickCommand;

        public ICommand OnDocumentDetailsButtonClickCommand
        {
            get { return _onDocumentDetailsButtonClickCommand ?? (_onDocumentDetailsButtonClickCommand = new RelayCommand(OnDocumentDetailsButtonClick)); }
            set { _onDocumentDetailsButtonClickCommand = value; }
        }

        private void OnDocumentDetailsButtonClick(object obj)
        {
            try
            {
                if (_buying == null)
                    throw new Exception("โปรดสแกนบาร์โค้ตเพื่อดูข้อมูลใบซื้อของชาวไร่ที่ต้องการก่อน");

                var window = new BuyingDocumentDetailNPI(
                        new vm_BuyingDocument
                        {
                            Crop = _buying.Crop,
                            FarmerCode = _buying.FarmerCode,
                            StationCode = _buying.StationCode,
                            DocumentNumber = _buying.DocumentNumber
                        });
                window.ShowDialog();

                //if (_farmer.ExtensionAgentCode == "NPIPR")
                //{
                //    var window = new BuyingDocumentDetailNPI(
                //        new vm_BuyingDocument
                //        {
                //            Crop = _buying.Crop,
                //            FarmerCode = _buying.FarmerCode,
                //            StationCode = _buying.StationCode,
                //            DocumentNumber = _buying.DocumentNumber
                //        });
                //    window.ShowDialog();
                //}
                //else
                //{
                //    var window = new BuyingDocumentDetailTW(
                //        new vm_BuyingDocument
                //        {
                //            Crop = _buying.Crop,
                //            FarmerCode = _buying.FarmerCode,
                //            StationCode = _buying.StationCode,
                //            DocumentNumber = _buying.DocumentNumber
                //        });
                //    window.ShowDialog();
                //}
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onXGradeButtonClickCommand;

        public ICommand OnXGradeButtonClickCommand
        {
            get { return _onXGradeButtonClickCommand ?? (_onXGradeButtonClickCommand = new RelayCommand(OnXGradeButtonClick)); }
            set { _onXGradeButtonClickCommand = value; }
        }

        private void OnXGradeButtonClick(object obj)
        {
            SelectedGradeBinding("X");
        }

        private ICommand _onCGradeButtonClickCommand;

        public ICommand OnCGradeButtonClickCommand
        {
            get { return _onCGradeButtonClickCommand ?? (_onCGradeButtonClickCommand = new RelayCommand(OnCGradeButtonClick)); }
            set { _onCGradeButtonClickCommand = value; }
        }

        private void OnCGradeButtonClick(object obj)
        {
            SelectedGradeBinding("C");
        }

        private ICommand _onBGradeButtonClickCommand;

        public ICommand OnBGradeButtonClickCommand
        {
            get { return _onBGradeButtonClickCommand ?? (_onBGradeButtonClickCommand = new RelayCommand(OnBGradeButtonClick)); }
            set { _onBGradeButtonClickCommand = value; }
        }

        private void OnBGradeButtonClick(object obj)
        {
            SelectedGradeBinding("B");
        }

        private ICommand _onTGradeButtonClickCommand;

        public ICommand OnTGradeButtonClickCommand
        {
            get { return _onTGradeButtonClickCommand ?? (_onTGradeButtonClickCommand = new RelayCommand(OnTGradeButtonClick)); }
            set { _onTGradeButtonClickCommand = value; }
        }

        private void OnTGradeButtonClick(object obj)
        {
            SelectedGradeBinding("T");
        }

        private ICommand _onOtherGradeButtonClickCommand;

        public ICommand OnOtherGradeButtonClickCommand
        {
            get { return _onOtherGradeButtonClickCommand ?? (_onOtherGradeButtonClickCommand = new RelayCommand(OnOtherGradeButtonClick)); }
            set { _onOtherGradeButtonClickCommand = value; }
        }

        private void OnOtherGradeButtonClick(object obj)
        {
            SelectedGradeBinding("Other");
        }

        private ICommand _onGradeButtonClickCommand;

        public ICommand OnGradeButtonClickCommand
        {
            get { return _onGradeButtonClickCommand ?? (_onGradeButtonClickCommand = new RelayCommand(OnGradeButtonClick)); }
            set { _onGradeButtonClickCommand = value; }
        }

        private void OnGradeButtonClick(object obj)
        {
            var grade = (BuyingGrade)obj;
            if (grade == null)
                return;
            _grade = grade.Grade;
            _gradeGroupCode = grade.GradeGroupCode;
            Save();
        }

        private ICommand _onTransportClearButtonClickCommand;

        public ICommand OnTransportClearButtonClickCommand
        {
            get { return _onTransportClearButtonClickCommand ?? (_onTransportClearButtonClickCommand = new RelayCommand(OnTransportClearButtonClick)); }
            set { _onTransportClearButtonClickCommand = value; }
        }

        private void OnTransportClearButtonClick(object obj)
        {
            TransportationListBinding();
            _transportationCode = null;
            _transportationSelectedItem = null;
            RaisePropertyChangedEvent(nameof(TransportationCode));
            RaisePropertyChangedEvent(nameof(TransportationSelectedItem));
        }

        #endregion


        #region Function
        private void DocumentBinding()
        {
            try
            {
                _document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_buying.Crop,
                    _buying.FarmerCode,
                    _buying.StationCode,
                    _buying.DocumentNumber);

                _documentCode = _document.Crop + "-" +
                    _document.StationCode + "-" +
                    _document.FarmerCode + "-" +
                    _document.DocumentNumber;
                _finistStatus = _document.FinishStatus;
                _totalBale = _document.Buyings.Count;
                _totalWeight = _document.Buyings.Sum(x=>x.Weight);
                _rejectedBale = _document.Buyings.Where(x => x.RejectTypeID != null).Count();
                _buyingBale = _totalBale - _rejectedBale;

                RaisePropertyChangedEvent(nameof(Document));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(FinishStatus));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(TotalWeight));
                RaisePropertyChangedEvent(nameof(RejectedBale));
                RaisePropertyChangedEvent(nameof(BuyingBale));
                FarmerInfoBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FarmerInfoBinding()
        {
            try
            {
                if (_document == null)
                    return;

                _farmer = BusinessLayerService.FarmerBL()
                    .GetSingle(_document.FarmerCode);
                _quota = BusinessLayerService.FarmerQuotaBL()
                    .GetSingle(_document.Crop, _document.FarmerCode);
                RaisePropertyChangedEvent(nameof(Farmer));
                RaisePropertyChangedEvent(nameof(Quota));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GradeListBinding(short crop)
        {
            try
            {
                var gradeGroups = BusinessLayerService.BuyingGradeGroupBL()
                    .GetByCrop(crop);
                var defaultGradeGroup = gradeGroups
                    .SingleOrDefault(x => x.DefaultStatus == true);
                if (defaultGradeGroup == null)
                    throw new Exception("This crop don't have to setup the default grade group." +
                        " Please contact admin for setup.");

                _gradeGroupCode = defaultGradeGroup.GradeGroupCode;
                _gradeList = BusinessLayerService.BuyingGradeBL()
                    .GetByGradeGroup(defaultGradeGroup.GradeGroupCode);

                RaisePropertyChangedEvent(nameof(GradeGroupCode));
                RaisePropertyChangedEvent(nameof(GradeList));
                SelectedGradeBinding("X");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingBinding()
        {
            try
            {
                if (_buying == null)
                    return;

                _grade = _buying.Grade;
                _weight = _buying.Weight;
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));
                DocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                _baleBarcode = string.Empty;
                _grade = string.Empty;
                _weight = (decimal)0.0;
                _buying = null;
                _document = null;
                _farmer = null;
                _quota = null;
                _documentCode = null;

                RaisePropertyChangedEvent(nameof(BaleBarcode));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));
                RaisePropertyChangedEvent(nameof(Buying));
                RaisePropertyChangedEvent(nameof(Document));
                RaisePropertyChangedEvent(nameof(Farmer));
                RaisePropertyChangedEvent(nameof(Quota));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                OnFocusRequested(nameof(BaleBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SelectedGradeBinding(string gradePrefix)
        {
            try
            {
                if (gradePrefix == "Other")
                {
                    List<BuyingGrade> fullList = new List<BuyingGrade>();
                    fullList.AddRange(_gradeList.Where(x => x.Grade.Substring(0, 1) == "X").ToList());
                    fullList.AddRange(_gradeList.Where(x => x.Grade.Substring(0, 1) == "C").ToList());
                    fullList.AddRange(_gradeList.Where(x => x.Grade.Substring(0, 1) == "B").ToList());
                    fullList.AddRange(_gradeList.Where(x => x.Grade.Substring(0, 1) == "T").ToList());
                    List<BuyingGrade> ortherList = new List<BuyingGrade>();
                    ortherList.AddRange(_gradeList.Except(fullList));
                    _selectGradeList = ortherList
                        .OrderBy(x => x.Grade)
                        .ToList();
                }
                else
                {
                    _selectGradeList = _gradeList
                        .Where(x => x.Grade.Substring(0, 1) == gradePrefix)
                        .OrderBy(x => x.Grade)
                        .ToList();
                }
                RaisePropertyChangedEvent(nameof(SelectGradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TransportationListBinding()
        {
            try
            {
                if (_transportationCreateDate == null)
                {
                    _transportationList = null;
                }
                else
                {
                    var areaList = user_setting.User.UserAreas;
                    var transportList = new List<TransportationDocument>();
                    foreach (var area in areaList)
                        transportList.AddRange(BusinessLayerService.TransportationDocumentBL()
                            .GetByCreateDateAndArea((DateTime)_transportationCreateDate, area.AreaCode));
                    _transportationList = null;
                    _transportationList = transportList;

                    if (_transportationList.Count > 0)
                        _transportationList
                            //.Where(x => x.IsFinish == false)
                            .OrderByDescending(x => x.TransportationCode)
                            .ToList();
                }
                RaisePropertyChangedEvent(nameof(TransportationList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                if (_buying == null)
                    throw new Exception("ไม่พบข้อมูล โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง ถ้ายังไม่ได้ ให้ติดต่อผู้ดแลระบบ");

                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้ถูกปิดการซื้อขายแล้ว");

                if (_buying.Weight.Equals(null))
                    throw new Exception("ยาห่อนี้ยังไม่มีข้อมูลน้ำหนักซื้อ");

                if (_buying.RejectTypeID != null)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้มีข้อมูลการทำ reject ท่านต้องการที่จะบันทึกเกรดซื้อใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_buying.Grade != null && _buying.Grade != _grade)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้ผ่านการบันทึกเกรดมาแล้ว ท่านต้องการที่จะแก้ไขเกรดซื้อใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_isUseTransportOption == true)
                {
                    if (string.IsNullOrEmpty(_transportationCode))
                        throw new Exception("หากท่านเลือกบันทึกข้อมูลใบนำส่งอัตโนมัติ ท่านจะต้องระบุใบนำส่งด้วย");

                    if (_transportationSelectedItem.IsFinish == true)
                        throw new Exception("ใบนำส่งนี้อยู่ในสถานะ Lock แล้ว โปรดเลือกใบนำส่งอื่น");
                }

                BusinessLayerService.BuyingBL()
                    .CaptureBuyingGrade(_buying.BaleBarcode,
                    _gradeGroupCode,
                    _grade,
                    user_setting.User.Username);

                if (_isUseTransportOption == true)
                    BusinessLayerService.TransportationDocumentBL()
                                .BringUp(_buying.BaleBarcode,
                                user_setting.User.Username,
                                _transportationCode);

                MessageBoxHelper.Info("บันทึกสำเร็จ" + Environment.NewLine +
                    "Bale Barcode:" + _buying.BaleBarcode + Environment.NewLine +
                    "Bale Number:" + _buying.BaleNumber + Environment.NewLine +
                    "Grade:" + _grade + Environment.NewLine +
                    "Weight:" + _buying.Weight.ToString("N1") + Environment.NewLine +
                    "Transportation Code:" + _transportationCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
