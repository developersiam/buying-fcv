﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Admin;
using FCVBuyingWPF.Form.BuyingStaff;
using FCVBuyingWPF.Form.Report;
using FCVBuyingWPF.Helper;
using FCVBuyingWPF.Model;
using FCVBuyingWPF.MVVM;
using FCVBuyingWPF.MVVM.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Input.StylusPlugIns;

namespace FCVBuyingWPF.MVVM.ViewModel
{
    public class vmCaptureBuyingInfo : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmCaptureBuyingInfo()
        {
            _transportationCreateDate = DateTime.Now;
            _isTabletMode = true;
            _isUseTransportOption = false;
            RaisePropertyChangedEvent(nameof(TransportationCreateDate));
            RaisePropertyChangedEvent(nameof(IsTabletMode));
            RaisePropertyChangedEvent(nameof(IsUseTransportOption));
            BaleTypeListBinding();
            TransportationListBinding();
            GradeListBinding();
        }

        #region Properties
        private Farmer _farmer;

        public Farmer Farmer
        {
            get { return _farmer; }
            set { _farmer = value; }
        }

        private FCVBuyingEntities.FarmerQuota _quota;

        public FCVBuyingEntities.FarmerQuota Quota
        {
            get { return _quota; }
            set { _quota = value; }
        }

        private BuyingDocument _document;

        public BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;
                DocumentBinding();
                BuyingListBinding();
                GradeListBinding();
                BaleTypeListBinding();
            }
        }

        private Buying _buying;

        public Buying Buying
        {
            get { return _buying; }
            set { _buying = value; }
        }

        private int _totalBale;

        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private int _rejectedBale;

        public int RejectedBale
        {
            get { return _rejectedBale; }
            set { _rejectedBale = value; }
        }

        private int _buyingBale;

        public int BuyingBale
        {
            get { return _buyingBale; }
            set { _buyingBale = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private string _baleBarcode;

        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private int? _baleNumber;

        public int? BaleNumber
        {
            get { return _baleNumber; }
            set { _baleNumber = value; }
        }

        private string _grade;

        public string Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }

        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private bool _finistStatus;

        public bool FinishStatus
        {
            get { return _finistStatus; }
            set { _finistStatus = value; }
        }

        private int _baleTypeID;

        public int BaleTypeID
        {
            get { return _baleTypeID; }
            set { _baleTypeID = value; }
        }

        private string _transportationCode;

        public string TransportationCode
        {
            get { return _transportationCode; }
            set
            {
                _transportationCode = value;
                if (string.IsNullOrEmpty(value))
                {
                    _truckNo = null;
                    _transportationStatus = false;
                }
                else
                {
                    var transport = _transportationList
                        .SingleOrDefault(x => x.TransportationCode == value);
                    _truckNo = transport.TruckNo;
                    _transportationStatus = transport.IsFinish;
                }
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(TransportationStatus));
            }
        }

        private string _truckNo;

        public string TruckNo
        {
            get { return _truckNo; }
            set { _truckNo = value; }
        }

        private bool _transportationStatus;

        public bool TransportationStatus
        {
            get { return _transportationStatus; }
            set { _transportationStatus = value; }
        }

        private DateTime _transportationCreateDate;

        public DateTime TransportationCreateDate
        {
            get { return _transportationCreateDate; }
            set
            {
                _transportationCreateDate = value;
                _transportationCode = null;
                RaisePropertyChangedEvent(nameof(TransportationCode));
                TransportationListBinding();
            }
        }

        private bool _isTabletMode;

        public bool IsTabletMode
        {
            get { return _isTabletMode; }
            set
            {
                _isTabletMode = value;
                _isLabtopMode = !value;
                RaisePropertyChangedEvent(nameof(IsTabletMode));
                RaisePropertyChangedEvent(nameof(IsLabtopMode));
            }
        }

        private bool _isLabtopMode;

        public bool IsLabtopMode
        {
            get { return _isLabtopMode; }
            set { _isLabtopMode = value; }
        }

        private bool _isUseTransportOption;

        public bool IsUseTransportOption
        {
            get { return _isUseTransportOption; }
            set
            {
                _isUseTransportOption = value;
                RaisePropertyChangedEvent(nameof(IsUseTransportOption));
            }
        }
        #endregion


        #region List
        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }

        private List<BuyingGrade> _gradeList;

        public List<BuyingGrade> GradeList
        {
            get { return _gradeList; }
            set { _gradeList = value; }
        }

        private List<BaleType> _baleTypeList;

        public List<BaleType> BaleTypeList
        {
            get { return _baleTypeList; }
            set { _baleTypeList = value; }
        }

        private List<TransportationDocument> _transportationList;

        public List<TransportationDocument> TransportationList
        {
            get { return _transportationList; }
            set { _transportationList = value; }
        }
        #endregion


        #region Command
        private ICommand _onBaleBarcodeClickCommand;

        public ICommand OnBaleBarcodeClickCommand
        {
            get { return _onBaleBarcodeClickCommand ?? (_onBaleBarcodeClickCommand = new RelayCommand(OnBaleBarcodeClick)); }
            set { _onBaleBarcodeClickCommand = value; }
        }

        private void OnBaleBarcodeClick(object obj)
        {
            ClearForm();
        }

        private ICommand _onBaleBarcodeEnterCommand;

        public ICommand OnBaleBarcodeEnterCommand
        {
            get { return _onBaleBarcodeEnterCommand ?? (_onBaleBarcodeEnterCommand = new RelayCommand(OnBaleBarcodeEnter)); }
            set { _onBaleBarcodeEnterCommand = value; }
        }

        private void OnBaleBarcodeEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดป้อน Barcode");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (!RegularExpressionHelper.IsBaleBarcodeCorrectFormat(_baleBarcode))
                    throw new Exception("รหัสบาร์โค้ตจะต้องประกอบด้วยตัวเลข เครื่องหมาย - และตัวอักษรภาษาอังกฤษเท่านั้น");

                if (_baleBarcode.Length != 17)
                    throw new Exception("รหัสบาร์โค้ตจะต้องมี 17 หลัก");

                _buying = BusinessLayerService.BuyingBL()
                    .GetSingle(_baleBarcode);
                if (_buying != null)
                {
                    if (_buying.Crop != _document.Crop ||
                        _buying.FarmerCode != _document.FarmerCode ||
                        _buying.StationCode != _document.StationCode ||
                        _buying.DocumentNumber != _document.DocumentNumber)
                        throw new Exception("ห่อยารหัสบาร์โค้ต " +
                            _buying.BaleBarcode +
                            " ซ้ำอยู่ในใบซื้ออื่น (" +
                            _buying.Crop + "-" +
                            _buying.StationCode + "-" +
                            _buying.FarmerCode + "-" +
                            _buying.DocumentNumber +
                             ") โปรดเลือกใบซื้อให้ถูกต้อง");

                    BuyingBinding();
                    FarmerInfoBinding();
                    DocumentBinding();
                }

                OnFocusRequested(nameof(BaleNumber));
                RaisePropertyChangedEvent(nameof(Buying));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBaleNumberEnterCommand;

        public ICommand OnBaleNumberEnterCommand
        {
            get { return _onBaleNumberEnterCommand ?? (_onBaleBarcodeEnterCommand = new RelayCommand(OnBaleNumberEnter)); }
            set { _onBaleNumberEnterCommand = value; }
        }

        private void OnBaleNumberEnter(object obj)
        {
            try
            {
                if (_baleNumber == null)
                {
                    MessageBoxHelper.Warning("โปรดระบุหมายเลขห่อ");
                    OnFocusRequested(nameof(BaleNumber));
                    return;
                }

                if (_baleNumber <= 0)
                {
                    MessageBoxHelper.Warning("หมายเลขห่อ (Bale number) ควรเริ่มจาก 1 เป็นต้นไป");
                    OnFocusRequested(nameof(BaleNumber));
                    return;
                }

                var duplicateBaleNumber = _buyingList
                    .SingleOrDefault(x => x.BaleNumber == _baleNumber);
                if (duplicateBaleNumber != null)
                {
                    if (duplicateBaleNumber.BaleBarcode != _baleBarcode)
                    {
                        MessageBoxHelper.Warning("หมายเลขห่อซ้ำ (ในใบซื้อจะต้องไม่มีเลขห่อซ้ำกัน)");
                        OnFocusRequested(nameof(BaleNumber));
                        return;
                    }
                }

                OnFocusRequested(nameof(Grade));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBaleNumberGotFocusCommand;

        public ICommand OnBaleNumberGotFocusCommand
        {
            get { return _onBaleNumberGotFocusCommand ?? (_onBaleNumberGotFocusCommand = new RelayCommand(OnBaleNumberGotFocus)); }
            set { _onBaleNumberGotFocusCommand = value; }
        }

        private void OnBaleNumberGotFocus(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนรหัสบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (_isTabletMode == false)
                    return;

                PopupNumpad._mode = "หมายเลขห่อ";
                if (_buying != null)
                {
                    PopupNumpad._double = _buying.BaleNumber;
                }
                else
                {
                    if (_buyingList.Count() > 0)
                    {
                        var max = _buyingList.Max(x => x.BaleNumber);
                        PopupNumpad._double = (double)max + 1;
                    }
                    else
                    {
                        PopupNumpad._double = 1;
                    }
                }


                var returnValue = PopupNumpad.ReturnValue();
                if (returnValue <= 0)
                    throw new Exception("Bale number ต้องมากกว่า 0");

                if (RegularExpressionHelper.IsNumericCharacter(returnValue.ToString()) == false)
                    throw new Exception("Bale number จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                _baleNumber = (int)returnValue;
                RaisePropertyChangedEvent(nameof(BaleNumber));
                OnFocusRequested(nameof(Grade));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onGradeGotFocusCommand;

        public ICommand OnGradeGotFocusCommand
        {
            get { return _onGradeGotFocusCommand ?? (_onGradeEnterCommand = new RelayCommand(OnGradeGotFocus)); }
            set { _onGradeGotFocusCommand = value; }
        }

        private void OnGradeGotFocus(object obj)
        {
            try
            {
                PopupBuyingGrades._gradeGroupCode = _gradeList.FirstOrDefault().GradeGroupCode;
                PopupBuyingGrades._gradePrefix = "X";
                PopupBuyingGrades._buyingGradeList = _gradeList;
                var returnVal = PopupBuyingGrades.ReturnValue();
                _grade = returnVal == null ? null : returnVal.Grade;
                RaisePropertyChangedEvent(nameof(Grade));
                if (_grade != null)
                    OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onGradeEnterCommand;

        public ICommand OnGradeEnterCommand
        {
            get { return _onGradeEnterCommand ?? (_onGradeEnterCommand = new RelayCommand(OnGradeEnter)); }
            set { _onGradeEnterCommand = value; }
        }

        private void OnGradeEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_grade))
                {
                    MessageBoxHelper.Warning("โปรดระบุเกรดซื้อ");
                    OnFocusRequested(nameof(Grade));
                    return;
                }

                if (_gradeList
                    .Where(x => x.Grade == _grade)
                    .Count() <= 0)
                {
                    MessageBoxHelper.Warning("ไม่พบเกรดนี้ในรายการตัวเลือก โปรดลองใหม่อีกครั้ง");
                    OnFocusRequested(nameof(Grade));
                    return;
                }
                OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onWeightEnterCommand;

        public ICommand OnWeightEnterCommand
        {
            get { return _onWeightEnterCommand ?? (_onWeightEnterCommand = new RelayCommand(OnWeightEnter)); }
            set { _onWeightEnterCommand = value; }
        }

        private void OnWeightEnter(object obj)
        {
            Save();
        }

        private ICommand _onWeightGotFocusCommand;

        public ICommand OnWeightGotFocusCommand
        {
            get { return _onWeightGotFocusCommand ?? (_onWeightGotFocusCommand = new RelayCommand(OnWeightGotFocus)); }
            set { _onWeightGotFocusCommand = value; }
        }

        private void OnWeightGotFocus(object obj)
        {
            try
            {
                //if (string.IsNullOrEmpty(_baleBarcode))
                //{
                //    MessageBoxHelper.Warning("โปรดสแกนรหัสบาร์โค้ต");
                //    OnFocusRequested(nameof(BaleBarcode));
                //    return;
                //}

                if (_isTabletMode == false)
                    return;

                PopupNumpad._mode = "น้ำหนัก";
                if (_buying != null)
                    PopupNumpad._double = (double)_buying.Weight;
                else
                    PopupNumpad._double = (double)_weight;

                var returnValue = PopupNumpad.ReturnValue();
                _weight = (decimal)returnValue;
                RaisePropertyChangedEvent(nameof(Weight));
                Save();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveButtonClickCommand;

        public ICommand OnSaveButtonClickCommand
        {
            get { return _onSaveButtonClickCommand ?? (_onSaveButtonClickCommand = new RelayCommand(OnSaveButtonClick)); }
            set { _onSaveButtonClickCommand = value; }
        }

        private void OnSaveButtonClick(object obj)
        {
            Save();
        }

        private ICommand _onClearButtonClickCommand;

        public ICommand OnClearButtonClickCommand
        {
            get { return _onClearButtonClickCommand ?? (_onClearButtonClickCommand = new RelayCommand(OnClearButtonClick)); }
            set { _onClearButtonClickCommand = value; }
        }

        private void OnClearButtonClick(object obj)
        {
            ClearForm();
            DocumentBinding();
            BuyingListBinding();
        }

        private ICommand _onRejectButtonClickCommand;

        public ICommand OnRejectButtonClickCommand
        {
            get { return _onRejectButtonClickCommand ?? (_onRejectButtonClickCommand = new RelayCommand(OnRejectButtonClick)); }
            set { _onRejectButtonClickCommand = value; }
        }

        private void OnRejectButtonClick(object obj)
        {
            try
            {
                if (_buying == null)
                    throw new Exception("โปรดสแกนบาร์โค้ต (หรือคลิกที่ปุ่ม Edit ในตาราง)" +
                        " ก่อนทุกครั้งเมื่อต้องการ reject" +
                        " เพื่อตรวจสอบข้อมูลห่อยาดังกล่าวก่อนบันทึกข้อมูล" +
                        " (Buying model cannot be null.)");

                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะถูกยืนยันและปิดการซื้อขายแล้ว");

                if (_buying.Grade != null)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้มีเกรดซื้อแล้ว ท่านต้องการ Reject ห่อยานี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                var window = new RejectPopup();
                var vm = new vmRejectPopup();
                vm.Window = window;
                window.DataContext = vm;
                window.ShowDialog();
                if (vm.RejectType == null)
                    throw new Exception("โปรดระบุประเภทของการ Reject");

                if (MessageBoxHelper.Question("ท่านต้องการ reject ห่อยานี้โดยระบุให้เป็น " +
                    vm.RejectType.RejectTypeName +
                    " ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .RejectBale(_buying.BaleBarcode,
                    vm.RejectType.RejectTypeID,
                    user_setting.User.Username);

                DocumentBinding();
                BuyingListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishButtonClickCommand;

        public ICommand OnFinishButtonClickCommand
        {
            get { return _onFinishButtonClickCommand ?? (_onFinishButtonClickCommand = new RelayCommand(OnFinishButtonClick)); }
            set { _onFinishButtonClickCommand = value; }
        }

        private void OnFinishButtonClick(object obj)
        {
            try
            {
                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้ถูกเปลี่ยนสถานะเป็น finish แล้ว");

                /// check list of the all bale barcode must be recode a buying grade every bales.
                ///
                var waitForBuyingBale = _document.Buyings
                    .Where(x => x.RejectTypeID == null &&
                    x.Grade == null)
                    .Count();
                if (waitForBuyingBale > 0)
                    throw new Exception("ยังมีห่อยาในใบซื้อนี้อีกจำนวน " + waitForBuyingBale +
                        " ห่อ ที่ยังไม่ได้บันทึกเกรด โปรดตรวจสอบอีกครั้งก่อนการปิดใบซื้อนี้");

                if (MessageBoxHelper.Question("ท่านต้องการเปลี่ยนสถานะ finish " +
                    "เพื่อเสร็จสิ้นขั้นตอนการซื้อขายใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .Finish(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                DocumentBinding();
                ClearForm();
                MessageBoxHelper.Info("Finished successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnfinishButtonClickCommand;

        public ICommand OnUnfinishButtonClickCommand
        {
            get { return _onUnfinishButtonClickCommand ?? (_onUnfinishButtonClickCommand = new RelayCommand(OnUnfinishButtonClick)); }
            set { _onUnfinishButtonClickCommand = value; }
        }

        private void OnUnfinishButtonClick(object obj)
        {
            try
            {
                if (_document.FinishStatus == false)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะเป็น Unfinish แล้ว");

                if (MessageBoxHelper.Question("ท่านต้องการ Unfinish ใบซื้อนี้" +
                    "เพื่อกลับไปแก้ไขข้อมูลใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingDocumentBL()
                    .UnFinish(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                BusinessLayerService.BuyingDocumentBL()
                    .CancelRequest(new BuyingDocument
                    {
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        StationCode = _document.StationCode,
                        DocumentNumber = _document.DocumentNumber
                    });

                DocumentBinding();
                ClearForm();

                MessageBoxHelper.Info("Unfinish successfully.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPrintButtonClickCommand;

        public ICommand OnPrintButtonClickCommand
        {
            get { return _onPrintButtonClickCommand ?? (_onPrintButtonClickCommand = new RelayCommand(OnPrintButtonClick)); }
            set { _onPrintButtonClickCommand = value; }
        }

        private void OnPrintButtonClick(object obj)
        {
            try
            {
                if (_document.FinishStatus == false)
                    throw new Exception("คุณต้องทำการปิดการขายใบซื้อนี้ก่อนจึงจะสามารถพิมพ์เวาเชอร์ได้");

                var extensionAgentCode = BusinessLayerService.FarmerBL()
                    .GetSingle(_document.FarmerCode).ExtensionAgentCode;

                if (extensionAgentCode.Contains("NPI"))
                {
                    RPTBUY001 window = new RPTBUY001(_document);
                    window.ShowDialog();
                }
                else
                {
                    if (MessageBoxHelper.Question("ท่านต้องการให้แสดงราคาซื้อในเอกสารหรือไม่? " +
                        "(Yes = แสดง / No = ไม่แสดง)") == MessageBoxResult.Yes)
                    {
                        var window = new RPTBUY002(_document, extensionAgentCode);
                        window.ShowDialog();
                    }
                    else
                    {
                        var window = new RPTBUY004(_document, extensionAgentCode);
                        window.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditBaleInfoButtonClickCommand;

        public ICommand OnEditBaleInfoButtonClickCommand
        {
            get { return _onEditBaleInfoButtonClickCommand ?? (_onEditBaleInfoButtonClickCommand = new RelayCommand(OnEditBaleInfoButtonClick)); }
            set { _onEditBaleInfoButtonClickCommand = value; }
        }

        private void OnEditBaleInfoButtonClick(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _buying = (Buying)obj;
                _baleBarcode = _buying.BaleBarcode;
                _baleNumber = _buying.BaleNumber;
                _baleTypeID = _buying.BaleTypeID;
                _grade = _buying.Grade;
                _weight = _buying.Weight;

                RaisePropertyChangedEvent(nameof(Buying));
                RaisePropertyChangedEvent(nameof(BaleBarcode));
                RaisePropertyChangedEvent(nameof(BaleNumber));
                RaisePropertyChangedEvent(nameof(BaleTypeID));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));

                if (!string.IsNullOrEmpty(_buying.TransportationCode))
                {
                    if (_buying.TransportationCode == _transportationCode)
                        return;

                    var transport = _buying.TransportationDocument;
                    _transportationCreateDate = transport.CreateDate;
                    TransportationListBinding();
                    _transportationCode = _buying.TransportationCode;
                    _truckNo = transport.TruckNo;
                    _transportationStatus = transport.IsFinish;
                    RaisePropertyChangedEvent(nameof(TransportationCode));
                    RaisePropertyChangedEvent(nameof(TransportationCreateDate));
                    RaisePropertyChangedEvent(nameof(TruckNo));
                    RaisePropertyChangedEvent(nameof(TransportationStatus));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteButtonClickCommand;

        public ICommand OnDeleteButtonClickCommand
        {
            get { return _onDeleteButtonClickCommand ?? (_onDeleteButtonClickCommand = new RelayCommand(OnDeleteButtonClick)); }
            set { _onDeleteButtonClickCommand = value; }
        }

        private void OnDeleteButtonClick(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _buying = (Buying)obj;
                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขายแล้ว ไม่สามารถลบข้อมูลได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BusinessLayerService.BuyingBL()
                    .DeleteBaleBarcode(_buying.BaleBarcode);
                DocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void DocumentBinding()
        {
            try
            {
                _document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_document.Crop,
                    _document.FarmerCode,
                    _document.StationCode,
                    _document.DocumentNumber);

                _documentCode = _document.Crop + "-" +
                    _document.StationCode + "-" +
                    _document.FarmerCode + "-" +
                    _document.DocumentNumber;
                _finistStatus = _document.FinishStatus;

                RaisePropertyChangedEvent(nameof(Document));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(FinishStatus));
                FarmerInfoBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingListBinding()
        {
            try
            {
                _buyingList = BusinessLayerService.BuyingBL()
                    .GetByDocument(_document.Crop,
                    _document.StationCode,
                    _document.FarmerCode,
                    _document.DocumentNumber);
                _totalBale = _buyingList.Count();
                _rejectedBale = _buyingList.Where(x => x.ReplaceTypeID != null).Count();
                _buyingBale = _buyingList.Where(x => x.Grade != null).Count();
                _totalWeight = _buyingList.Sum(x => x.Weight);
                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(RejectedBale));
                RaisePropertyChangedEvent(nameof(BuyingBale));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FarmerInfoBinding()
        {
            try
            {
                if (_document == null)
                    return;

                _farmer = BusinessLayerService.FarmerBL()
                    .GetSingle(_document.FarmerCode);
                _quota = BusinessLayerService.FarmerQuotaBL()
                    .GetSingle(_document.Crop, _document.FarmerCode);
                RaisePropertyChangedEvent(nameof(Farmer));
                RaisePropertyChangedEvent(nameof(Quota));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GradeListBinding()
        {
            try
            {
                var defaultCrop = BusinessLayerService.CropBL().GetDefault();
                if (defaultCrop == null)
                    throw new Exception("The default crop not to setup. Please contact the IT department.");

                var gradeGroups = BusinessLayerService.BuyingGradeGroupBL()
                    .GetByCrop(defaultCrop.Crop1);
                var defaultGradeGroup = gradeGroups.SingleOrDefault(x => x.DefaultStatus == true);
                if (defaultGradeGroup == null)
                    throw new Exception("This crop don't have to setup the default grade group." +
                        " Please contact admin for setup.");

                _gradeList = BusinessLayerService.BuyingGradeBL()
                    .GetByGradeGroup(defaultGradeGroup.GradeGroupCode);
                RaisePropertyChangedEvent(nameof(GradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BaleTypeListBinding()
        {
            _baleTypeList = BusinessLayerService.BaleTypeBL()
                .GetBaleTypes()
                .OrderBy(x => x.BaleTypeName)
                .ToList();
            RaisePropertyChangedEvent(nameof(BaleTypeList));
        }

        private void TransportationListBinding()
        {
            try
            {
                var userAreaList = user_setting.User.UserAreas;
                var transportList = new List<TransportationDocument>();
                foreach (var item in userAreaList)
                    transportList.AddRange(BusinessLayerService.TransportationDocumentBL()
                        .GetByCreateDateAndArea(_transportationCreateDate, item.AreaCode));

                _transportationList = transportList
                    .Where(x => x.IsFinish == false)
                    .OrderByDescending(x => x.TransportationCode)
                    .ToList();
                RaisePropertyChangedEvent(nameof(TransportationList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BuyingBinding()
        {
            try
            {
                if (_buying == null)
                    return;

                _baleNumber = _buying.BaleNumber;
                _baleTypeID = _buying.BaleTypeID;
                _grade = _buying.Grade;
                _weight = _buying.Weight;
                RaisePropertyChangedEvent(nameof(BaleNumber));
                RaisePropertyChangedEvent(nameof(BaleTypeID));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));

                if (!string.IsNullOrEmpty(_buying.TransportationCode))
                {
                    if (_buying.TransportationCode == _transportationCode)
                        return;

                    var transport = _buying.TransportationDocument;
                    _transportationCreateDate = transport.CreateDate;
                    TransportationListBinding();
                    _transportationCode = _buying.TransportationCode;
                    _truckNo = transport.TruckNo;
                    _transportationStatus = transport.IsFinish;
                    RaisePropertyChangedEvent(nameof(TransportationCode));
                    RaisePropertyChangedEvent(nameof(TransportationCreateDate));
                    RaisePropertyChangedEvent(nameof(TruckNo));
                    RaisePropertyChangedEvent(nameof(TransportationStatus));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                _baleBarcode = string.Empty;
                _baleNumber = null;
                _grade = string.Empty;
                _weight = (decimal)0.0;
                _buying = null;
                RaisePropertyChangedEvent(nameof(BaleBarcode));
                RaisePropertyChangedEvent(nameof(BaleNumber));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));
                RaisePropertyChangedEvent(nameof(Buying));
                OnFocusRequested(nameof(BaleBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Save()
        {
            try
            {
                if (_document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขายแล้ว ไม่สามารถแก้ข้อมูลได้");

                if (string.IsNullOrEmpty(_baleBarcode))
                    throw new Exception("โปรดสแกนบาร์โค้ต");

                if (_baleNumber == null)
                    throw new Exception("โปรดระบุ Bale Number");

                if (_baleTypeID == 0)
                    throw new Exception("โปรดระบุประเภทห่อยา Bale Type");

                if (string.IsNullOrEmpty(_grade))
                    throw new Exception("โปรดระบุเกรดซื้อ");

                var user = user_setting.User.Username;
                var selectedGrade = _gradeList
                    .SingleOrDefault(x => x.Grade == _grade);

                if (selectedGrade == null)
                    throw new Exception("เกรดที่ระบุไม่ตรงกับที่มีในรายการตัวเลือก โปรดลองใหม่อีกครั้ง");

                if (_weight > 70)
                {
                    if (MessageBoxHelper.Question("น้ำหนักห่อยานี้มากกว่า 70 กก." +
                        " ท่านต้องการบันทึกข้อมูลนี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_buyingList
                    .Where(x => x.BaleNumber == _baleNumber)
                    .Count() > 0 &&
                    _buying.BaleNumber != _baleNumber)
                {
                    MessageBoxHelper.Warning("หมายเลขห่อซ้ำ (ในใบซื้อจะต้องไม่มีเลขห่อซ้ำกัน)");
                    OnFocusRequested(nameof(BaleNumber));
                    return;
                }

                if (_isUseTransportOption == true)
                {
                    if (string.IsNullOrEmpty(_transportationCode))
                        throw new Exception("เมื่อมีการเปิดใช้งานฟังก์ชันบันทึกข้อมูลใบนำส่ง ท่านจะต้องระบุรหัสใบนำส่งด้วย");
                }
                else
                {
                    _transportationCode = null;
                }

                if (_buying == null)
                {
                    BusinessLayerService.BuyingBL()
                        .Add(
                        _baleBarcode,
                        (int)_baleNumber,
                        _document,
                        _baleTypeID,
                        _weight,
                        selectedGrade.GradeGroupCode,
                        selectedGrade.Grade,
                        _transportationCode,
                        user
                        );
                }
                else
                {
                    /// Editing the buying info.
                    if (_buying.Crop != _document.Crop ||
                            _buying.FarmerCode != _document.FarmerCode ||
                            _buying.StationCode != _document.StationCode ||
                            _buying.DocumentNumber != _document.DocumentNumber)
                        throw new Exception("ห่อยารหัสบาร์โค้ต " +
                            _buying.BaleBarcode +
                            " ซ้ำอยู่ในใบซื้ออื่น (" +
                            _buying.Crop + "-" +
                            _buying.StationCode + "-" +
                            _buying.FarmerCode + "-" +
                            _buying.DocumentNumber +
                             ") โปรดเลือกใบซื้อให้ถูกต้อง");

                    if (MessageBoxHelper.Question("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                        return;

                    BusinessLayerService.BuyingBL()
                        .Edit(_buying.BaleBarcode,
                        (int)_baleNumber,
                        _document,
                        _baleTypeID,
                        _weight,
                        selectedGrade.GradeGroupCode,
                        selectedGrade.Grade,
                        _transportationCode,
                        user);
                }

                DocumentBinding();
                BuyingListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
