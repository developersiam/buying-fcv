﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Helper;
using FCVBuyingWPF.MVVM.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FCVBuyingWPF.MVVM.ViewModel
{
    public class vmRejectPopup : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmRejectPopup()
        {
            _rejectType = null;
            _rejectTypeList = BusinessLayerService.RejectTypeBL()
                .GetRejectTypes()
                .OrderBy(x => x.RejectTypeName)
                .ToList();
            RaisePropertyChangedEvent(nameof(RejectType));
            RaisePropertyChangedEvent(nameof(RejectTypeList));
        }

        #region Properties
        private RejectPopup _window;

        public RejectPopup Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private RejectType _rejectType;

        public RejectType RejectType
        {
            get { return _rejectType; }
            set { _rejectType = value; }
        }

        private List<RejectType> _rejectTypeList;

        public List<RejectType> RejectTypeList
        {
            get { return _rejectTypeList; }
            set { _rejectTypeList = value; }
        }
        #endregion


        #region Command
        private ICommand _onRejectTypeButtonClickCommand;

        public ICommand OnRejectTypeButtonClickCommand
        {
            get { return _onRejectTypeButtonClickCommand ?? (_onRejectTypeButtonClickCommand = new RelayCommand(OnRejectTypeButtonClick)); }
            set { _onRejectTypeButtonClickCommand = value; }
        }

        private void OnRejectTypeButtonClick(object obj)
        {
            try
            {
                _rejectType = (RejectType)obj;
                RaisePropertyChangedEvent(nameof(RejectType));
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
