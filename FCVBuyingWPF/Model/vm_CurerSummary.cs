﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public class vm_CurerSummary
    {
        public decimal AveragePrice { get; set; }
        public int Quota { get; set; }
        public decimal Sold { get; set; }
        public decimal Remaining { get; set; }
    }
}
