﻿using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public class vm_TransportationDocument : TransportationDocument
    {
        public TransportationDocument TransportationDocument { get; set; }
        public Double TotalWeight { get; set; }
        public Double TotalBales { get; set; }
    }
}
