﻿using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public class vm_BuyingDocument : BuyingDocument
    {
        public int Bales { get; set; }
        public int CloseBales { get; set; }
        public int OpenBales { get; set; }
        public int BuyingBales { get; set; }
        public int RejectedBales { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal BuyingWeight { get; set; }
        public decimal RejectWeight { get; set; }
        public string BuyingDocumentCode { get; set; }
        public string CitizenID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ExtensionAgentCode { get; set; }
        public string StationName { get; set; }
    }
}
