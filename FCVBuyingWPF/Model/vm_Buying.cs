﻿using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public class vm_Buying : Buying
    {
        public string Quality { get; set; }
        public decimal UnitPrice { get; set; }
        public string RejectTypeName { get; set; }
        public decimal Price { get; set; }
        public string BaleTypeName { get; set; }
        public string Position { get; set; }
        public string ExtensionAgentCode { get; set; }
        public string FarmerName { get; set; }
        public string GeneralCode { get; set; }
    }
}
