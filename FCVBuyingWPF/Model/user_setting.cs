﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public static class user_setting
    {
        public static User User { get; set; }
        public static List<Role> UserRoles { get; set; }
        public static Crop Crop { get { return BusinessLayerService.CropBL().GetDefault(); } }
    }
}
