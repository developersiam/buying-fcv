﻿using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Model
{
    public class vm_FarmerProfile : Person
    {
        public short Crop { get; set; }
        public string ExtensionAgentCode { get; set; }
        public string FarmerCode { get; set; }
        public string GeneralCode { get; set; }
        public string ContractCode { get; set; }
        public DateTime RegisterDate { get; set; }
        public decimal Quota { get; set; }
        public decimal Sold { get; set; }
        public int Bales { get; set; }
        public decimal AveragePrice { get; set; }
        public string FarmerAddress { get; set; }
    }
}
