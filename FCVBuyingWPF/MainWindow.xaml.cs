﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Form.Admin;
using FCVBuyingWPF.Form.BuyingStaff;
using FCVBuyingWPF.Model;
using FCVBuyingWPF.MVVM.View;
using FCVBuyingWPF.MVVM.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCVBuyingWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ////This code below use for test by skip the login step.
            //user_setting.User = BusinessLayerService.UserBL().GetById("eakkaluck");
            //var document = BusinessLayerService.BuyingDocumentBL()
            //    .GetSingle(2023, "TW00001", "CR01", 10);
            //var window = new CaptureBuyingInfo();
            //var vm = new vmCaptureBuyingInfo();
            //vm.Document = document;
            //window.DataContext = vm;
            //window.ShowDialog();

            //var user = BusinessLayerService.UserBL().GetById("eakkaluck");
            //user_setting.User = user;
            //var page = new CaptureBuyingInfo();
            //var vm = new vmCaptureBuyingInfo();
            //vm.Document = new BuyingDocument
            //{
            //    Crop = 2023,
            //    FarmerCode = "TW00001",
            //    StationCode = "CR01",
            //    DocumentNumber = 10
            //};
            //page.DataContext = vm;
            //page.ShowDialog();
            //Application.Current.Shutdown();


            //var user = BusinessLayerService.UserBL().GetById("eakkaluck");
            //user_setting.User = user;
            //var page = new CaptureGrade();
            //var vm = new vmCaptureGrade(2023);
            //page.DataContext = vm;
            //MainFrame.NavigationService.Navigate(page);
        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void MenuPopupButton_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void OnCopy(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void ReceivingMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Home());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LogoffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                MainFrame.NavigationService.Navigate(new Form.Login());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageUserAccountMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Users());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageUserRoleMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Roles());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageBuyingGradeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.BuyingGradeGroups());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageBuyingStationMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.BuyingStations());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageNTRMTypeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.NTRMTypes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageRejectTypeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.RejectTypes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageFarmerMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Farmers());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageFarmerQuotaMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Farmers());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageExtensionAgentMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.ExtensionAgents());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageCropMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Crops());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageBaleTypeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.BaleTypes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CaptureGradeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var page = new CaptureGrade();
                var crop = BusinessLayerService.CropBL().GetDefault().Crop1;
                var vm = new vmCaptureGrade(crop);
                page.DataContext = vm;
                MainFrame.NavigationService.Navigate(page);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerQuotaMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.BuyingStaff.Farmers());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NTRMInspectionMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.BuyingStaff.NTRMInspection());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintBuyingVoucherMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.BuyingStaff.BuyingVouchers());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageAreaMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Admin.Areas());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MoistureResultMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "QC").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ QC เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.QC.MoistureResults());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SetupDigitalScalParameterMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                Form.BuyingStaff.PopupDigitalScaleSetup window = new Form.BuyingStaff.PopupDigitalScaleSetup();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.BuyingStaff.BuyingDocuments());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NPIBuyingPerDayReportMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                Form.Report.RPTBUY003 window = new Form.Report.RPTBUY003();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RPTQCFCV01Menu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "QC").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ QC เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Report.RPTQCFCV01());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RPTQCFCV02Menu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "QC").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ QC เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.Report.RPTQCFCV02());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TransportationMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin" || x.RoleName == "Staff").Count() < 1)
                {
                    MessageBox.Show("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MainFrame.NavigationService.Navigate(new Form.BuyingStaff.TransportationDocuments());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
