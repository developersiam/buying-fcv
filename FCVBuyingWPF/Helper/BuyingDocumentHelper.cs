﻿using FCVBuyingBL;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class BuyingDocumentHelper
    {
        public static List<vm_BuyingDocument> GetByDate(DateTime date,
            string extentionAgentCode, string stationCode)
        {
            return BusinessLayerService.BuyingDocumentBL()
                      .GetByStaion(date, stationCode)
                      .Where(x => x.FarmerQuota.Farmer.ExtensionAgentCode == extentionAgentCode)
                      .Select(x => new vm_BuyingDocument
                      {
                          BuyingDocumentCode = x.Crop + "-" + x.StationCode + "-" + x.FarmerCode + "-" + x.DocumentNumber,
                          Bales = x.Buyings.Count(),
                          CloseBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.BaleTypeID == 3).Count(),
                          OpenBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.BaleTypeID == 2).Count(),
                          TotalWeight = x.Buyings.Sum(y => y.Weight),
                          BuyingBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Count(),
                          BuyingWeight = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Sum(d => d.Weight),
                          RejectedBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.RejectTypeID != null).Count(),
                          RejectWeight = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.RejectTypeID != null).Sum(y => y.Weight),
                          Crop = x.Crop,
                          FarmerCode = x.FarmerCode,
                          StationCode = x.StationCode,
                          DocumentNumber = x.DocumentNumber,
                          CreateDate = x.CreateDate,
                          CreateUser = x.CreateUser,
                          ModifiedDate = x.ModifiedDate,
                          ModifiedUser = x.ModifiedUser,
                          FinishStatus = x.FinishStatus,
                          FinishTime = x.FinishTime,
                          RequestFinishStatus = x.RequestFinishStatus,
                          UnfinishTime = x.UnfinishTime,
                          CitizenID = x.FarmerQuota.Farmer.Person.CitizenID,
                          FirstName = x.FarmerQuota.Farmer.Person.FirstName,
                          LastName = x.FarmerQuota.Farmer.Person.LastName,
                          ExtensionAgentCode = x.FarmerQuota.Farmer.ExtensionAgentCode,
                          FarmerQuota = x.FarmerQuota
                      }).ToList();
        }

        public static List<vm_BuyingDocument> GetByFarmerAndStation(short crop,
            string farmerCode, string stationCode)
        {
            return BusinessLayerService.BuyingDocumentBL()
                      .GetByFarmerAndStaion(crop,
                      stationCode,
                      farmerCode)
                      .Select(x => new vm_BuyingDocument
                      {
                          BuyingDocumentCode = x.Crop + "-" + x.StationCode + "-" + x.FarmerCode + "-" + x.DocumentNumber,
                          Bales = x.Buyings.Count(),
                          BuyingBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Count(),
                          RejectedBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.RejectTypeID != null).Count(),
                          BuyingWeight = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Sum(d => d.Weight),
                          Crop = x.Crop,
                          FarmerCode = x.FarmerCode,
                          StationCode = x.StationCode,
                          DocumentNumber = x.DocumentNumber,
                          CreateDate = x.CreateDate,
                          CreateUser = x.CreateUser,
                          ModifiedDate = x.ModifiedDate,
                          ModifiedUser = x.ModifiedUser,
                          FinishStatus = x.FinishStatus,
                          FinishTime = x.FinishTime,
                          RequestFinishStatus = x.RequestFinishStatus,
                          UnfinishTime = x.UnfinishTime
                      }).ToList();
        }

        public static vm_BuyingDocument GetSingle(short crop,
            string farmerCode, string stationCode, short documentNumber)
        {
            var model = BusinessLayerService.BuyingDocumentBL()
                      .GetSingle(crop,
                      farmerCode,
                      stationCode,
                      documentNumber);

            return new vm_BuyingDocument
            {
                BuyingDocumentCode = model.Crop + "-" +
                model.StationCode + "-" +
                model.FarmerCode + "-" +
                model.DocumentNumber,

                Bales = model.Buyings.Count(),
                BuyingBales = model.Buyings.Count <= 0 ? 0 : model.Buyings.Where(c => c.Grade != null).Count(),
                BuyingWeight = model.Buyings.Count <= 0 ? 0 : model.Buyings.Where(c => c.Grade != null).Sum(d => d.Weight),
                RejectedBales = model.Buyings.Count <= 0 ? 0 : model.Buyings.Where(c => c.RejectTypeID != null).Count(),
                RejectWeight = model.Buyings.Count <= 0 ? 0 : model.Buyings.Where(x => x.RejectTypeID != null).Sum(x => x.Weight),
                Crop = model.Crop,
                FarmerCode = model.FarmerCode,
                StationCode = model.StationCode,
                DocumentNumber = model.DocumentNumber,
                CreateDate = model.CreateDate,
                CreateUser = model.CreateUser,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                FinishStatus = model.FinishStatus,
                FinishTime = model.FinishTime,
                RequestFinishStatus = model.RequestFinishStatus,
                UnfinishTime = model.UnfinishTime,
                ExtensionAgentCode = model.FarmerQuota.Farmer.ExtensionAgentCode,
                StationName = model.BuyingStation.StationName
            };
        }

        public static List<vm_BuyingDocument> GetBuyingVoucherByStation(DateTime createDate, string stationCode)
        {
            return BusinessLayerService.BuyingDocumentBL()
                      .GetByStaion(createDate, stationCode)
                      .Select(x => new vm_BuyingDocument
                      {
                          BuyingDocumentCode = x.Crop + "-" + x.StationCode + "-" + x.FarmerCode + "-" + x.DocumentNumber,
                          Bales = x.Buyings.Count(),
                          BuyingBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Count(),
                          RejectedBales = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.RejectTypeID != null).Count(),
                          BuyingWeight = x.Buyings.Count <= 0 ? 0 : x.Buyings.Where(c => c.Grade != null).Sum(d => d.Weight),
                          Crop = x.Crop,
                          FarmerCode = x.FarmerCode,
                          StationCode = x.StationCode,
                          DocumentNumber = x.DocumentNumber,
                          CreateDate = x.CreateDate,
                          CreateUser = x.CreateUser,
                          ModifiedDate = x.ModifiedDate,
                          ModifiedUser = x.ModifiedUser,
                          FinishStatus = x.FinishStatus,
                          FinishTime = x.FinishTime,
                          RequestFinishStatus = x.RequestFinishStatus,
                          UnfinishTime = x.UnfinishTime,
                          CitizenID = x.FarmerQuota.Farmer.Person.CitizenID,
                          FirstName = x.FarmerQuota.Farmer.Person.FirstName,
                          LastName = x.FarmerQuota.Farmer.Person.LastName,
                          ExtensionAgentCode = x.FarmerQuota.Farmer.ExtensionAgentCode,
                          FarmerQuota = x.FarmerQuota
                      }).ToList();
        }
    }
}