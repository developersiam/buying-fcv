﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace FCVBuyingWPF.Helper
{
    public struct ComportParameter
    {
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public System.IO.Ports.Parity Parity { get; set; }
        public short DataBits { get; set; }
        public System.IO.Ports.StopBits StopBits { get; set; }
        public short StrLength { get; set; }
        public string ReplaceChar { get; set; }
        public int ThreadSleep { get; set; }
    }

    public static class DigitalScaleComunicationHelper
    {
        public static SerialPort serialPort = new SerialPort();
        static string _replaceChar;
        static string _lineReadIn;
        static short _strLength;
        static int _threadSleep;

        //private delegate void preventCrossThreading(string x);
        //private static preventCrossThreading accessControlFromCentralThread;

        public static void Setup(ComportParameter model)
        {
            try
            {
                serialPort.PortName = model.PortName;
                serialPort.BaudRate = model.BaudRate;
                serialPort.Parity = model.Parity;
                serialPort.DataBits = model.DataBits;
                serialPort.StopBits = model.StopBits;

                _strLength = model.StrLength;
                _replaceChar = model.ReplaceChar;
                _threadSleep = model.ThreadSleep;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightResultFrom19eA()
        {
            try
            {
                do
                {
                    _lineReadIn += serialPort.ReadExisting();
                    Thread.Sleep(_threadSleep);
                } while (_lineReadIn.Length < _strLength);

                /// display what we've acquired.
                string result = _lineReadIn;

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = result.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 2)
                    return "999.9";

                result = splitString[1];

                result = result.Replace(" ", string.Empty);
                result = result.Replace("", string.Empty);


                //foreach (char item in _replaceChar)
                //    result = result.Replace(item.ToString(), "");

                /// replace charctor ทีละตัว ตัวไหนที่ไม่ใช่ตัวเลข จุด และเครื่องหมายลบ ให้ทำการ replace ด้วย ""
                /// 
                char[] replaceOperator = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '-' };
                foreach (var str in result)
                {
                    bool flag = true;
                    foreach (var opr in replaceOperator)
                    {
                        if (str == opr)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag == true)
                        result = result.Replace(str.ToString(), string.Empty);
                }

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                        //MessageBox.Show(result);                     
                        return "Scale Error";
                }

                /// Clear string from digital scale sended.
                _lineReadIn = "";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightResultFromTigerModel()
        {
            try
            {
                _lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(_threadSleep);

                /// display what we've acquired.
                string result = _lineReadIn;

                if (result.Length < 20)
                    return "999.9";

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = result.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 2)
                    return "888.8";

                result = splitString[1];

                result = result.Replace(" ", string.Empty);
                result = result.Replace("", string.Empty);


                //foreach (char item in _replaceChar)
                //    result = result.Replace(item.ToString(), "");

                /// replace charctor ทีละตัว ตัวไหนที่ไม่ใช่ตัวเลข จุด และเครื่องหมายลบ ให้ทำการ replace ด้วย ""
                /// 
                char[] replaceOperator = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '-' };
                foreach (var str in result)
                {
                    bool flag = true;
                    foreach (var opr in replaceOperator)
                    {
                        if (str == opr)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag == true)
                        result = result.Replace(str.ToString(), string.Empty);
                }

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                        //MessageBox.Show(result);                     
                        return "Scale Error";
                }

                /// Clear string from digital scale sended.
                _lineReadIn = "";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
