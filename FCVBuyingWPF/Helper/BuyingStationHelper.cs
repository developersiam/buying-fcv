﻿using FCVBuyingBL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class BuyingStationHelper
    {
        public static List<BuyingStation> GetBuyingStationByArea(string areaCode)
        {
            return BusinessLayerService.BuyingStationBL()
                .GetByArea(areaCode)
                .Select(x => new BuyingStation
                {
                    StationCode = x.StationCode,
                    StationName = "(" + x.StationCode + ") " + x.StationName
                }).ToList();
        }
    }
}
