﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class TransportationHelper
    {
        public static vm_TransportationDocument GetSingle(string transportationCode)
        {
            var model = BusinessLayerService.TransportationDocumentBL().GetSingle(transportationCode);

            return new vm_TransportationDocument
            {
                CreateDate = model.CreateDate,
                TransportationCode = model.TransportationCode,
                TransportationDocument = model,
                TruckNo = model.TruckNo,///ใช้กับ report เท่านั้นเนื่องจากไม่สามารถอ้างอิงแบบเชื่อมโยงใน rdlc ได้
                TotalBales = model.Buyings.Count,
                TotalWeight = Convert.ToDouble(model.Buyings.Sum(b => b.Weight))
            };
        }

        public static List<vm_TransportationDocument> GetByCreateDate(DateTime createDate)
        {
            var model = BusinessLayerService.TransportationDocumentBL().GetByCreateDate(createDate).ToList();

            return (from t in model
                    select new vm_TransportationDocument
                    {
                        TransportationCode = t.TransportationCode,
                        TransportationDocument = t,
                        TotalBales = t.Buyings.Count,
                        TotalWeight = Convert.ToDouble(t.Buyings.Sum(b => b.Weight))
                    }).ToList();
        }

        public static List<vm_TransportationDocument> GetByCrop(short crop)
        {
            List<TransportationDocument> transportationDocumentList = new List<TransportationDocument>();
            transportationDocumentList = BusinessLayerService.TransportationDocumentBL().GetByCrop(crop).ToList();

            return (from t in transportationDocumentList
                    select new vm_TransportationDocument
                    {
                        TransportationCode = t.TransportationCode,
                        TransportationDocument = t,
                        TotalBales = t.Buyings.Count,
                        TotalWeight = Convert.ToDouble(t.Buyings.Sum(b => b.Weight))
                    }).ToList();
        }
    }
}
