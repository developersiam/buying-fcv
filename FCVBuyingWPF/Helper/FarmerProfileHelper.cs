﻿using FCVBuyingBL;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class FarmerProfileHelper
    {
        public static vm_FarmerProfile GetSingleByCrop(short crop, string farmerCode)
        {
            var _farmerProfile = BusinessLayerService.FarmerBL().GetSingle(farmerCode);
            var _farmerQuota = BusinessLayerService.FarmerQuotaBL()
                .GetSingle(user_setting.Crop.Crop1, farmerCode);
            var _buyingList = BusinessLayerService.BuyingBL().GetByFarmer(crop, farmerCode);

            if (_farmerProfile == null)
                return null;
            //throw new ArgumentException("ไม่พบข้อมูลประวัติชาวไร่ในระบบ");
            if (_farmerQuota == null)
                throw new Exception("ไม่พบโควต้าในปีปัจจุบบันของชาวไร่รายนี้ในระบบ");

            return new vm_FarmerProfile
            {
                Crop = _farmerQuota.Crop,
                FarmerCode = _farmerProfile.FarmerCode,
                GeneralCode = _farmerQuota.GeneralCode,
                ExtensionAgentCode = _farmerProfile.ExtensionAgentCode,
                ContractCode = _farmerQuota.ContractCode,
                Quota = _farmerQuota.Quota,
                Sold = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                Bales = _buyingList.Where(x => x.Grade != null).Count(),
                AveragePrice = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight) <= 0 ? 0 :
                _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight * x.BuyingGrade.UnitPrice) /
                _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                FarmerAddress = _farmerProfile.Person.HouseNumber + " " +
                _farmerProfile.Person.Village + " " +
                _farmerProfile.Person.Tumbon + " " +
                _farmerProfile.Person.Amphur + " " +
                _farmerProfile.Person.Province,
                Prefix = _farmerProfile.Person.Prefix,
                FirstName = _farmerProfile.Person.FirstName,
                LastName = _farmerProfile.Person.LastName,
                CitizenID = _farmerProfile.Person.CitizenID
            };
        }

        public static vm_FarmerProfile GetSingleByCropLiteVersion(short crop, string farmerCode)
        {
            var _farmerProfile = BusinessLayerService.FarmerBL().GetSingle(farmerCode);
            var _farmerQuota = BusinessLayerService.FarmerQuotaBL()
                .GetSingle(user_setting.Crop.Crop1, farmerCode);

            //var _buyingList = BusinessLayerService.BuyingBL().GetByFarmer(crop, farmerCode);

            if (_farmerProfile == null)
                return null;
            //throw new ArgumentException("ไม่พบข้อมูลประวัติชาวไร่ในระบบ");

            return new vm_FarmerProfile
            {
                Crop = _farmerQuota.Crop,
                FarmerCode = _farmerProfile.FarmerCode,
                GeneralCode = _farmerQuota.GeneralCode,
                ExtensionAgentCode = _farmerProfile.ExtensionAgentCode,
                ContractCode = _farmerQuota.ContractCode,
                Quota = _farmerQuota.Quota,
                //Sold = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                //Bales = _buyingList.Where(x => x.Grade != null).Count(),
                //AveragePrice = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight) <= 0 ? 0 :
                //_buyingList.Where(x => x.Grade != null).Sum(x => x.Weight * x.BuyingGrade.UnitPrice) /
                //_buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                FarmerAddress = _farmerProfile.Person.HouseNumber + " " +
                _farmerProfile.Person.Village + " " +
                _farmerProfile.Person.Tumbon + " " +
                _farmerProfile.Person.Amphur + " " +
                _farmerProfile.Person.Province,
                Prefix = _farmerProfile.Person.Prefix,
                FirstName = _farmerProfile.Person.FirstName,
                LastName = _farmerProfile.Person.LastName,
                CitizenID = _farmerProfile.Person.CitizenID

                //Crop = _farmerQuota.Crop,
                //FarmerCode = _farmerProfile.FarmerCode,
                //GeneralCode = _farmerQuota.GeneralCode,
                //ExtensionAgentCode = _farmerProfile.ExtensionAgentCode,
                //ContractCode = _farmerQuota.ContractCode,
                //Quota = _farmerQuota.Quota,
                //Sold = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                //Bales = _buyingList.Where(x => x.Grade != null).Count(),
                //AveragePrice = _buyingList.Where(x => x.Grade != null).Sum(x => x.Weight) <= 0 ? 0 :
                //_buyingList.Where(x => x.Grade != null).Sum(x => x.Weight * x.BuyingGrade.UnitPrice) /
                //_buyingList.Where(x => x.Grade != null).Sum(x => x.Weight),
                //FarmerAddress = _farmerProfile.Person.HouseNumber + " " +
                //_farmerProfile.Person.Village + " " +
                //_farmerProfile.Person.Tumbon + " " +
                //_farmerProfile.Person.Amphur + " " +
                //_farmerProfile.Person.Province,
                //Prefix = _farmerProfile.Person.Prefix,
                //FirstName = _farmerProfile.Person.FirstName,
                //LastName = _farmerProfile.Person.LastName,
                //CitizenID = _farmerProfile.Person.CitizenID
            };
        }

        public static List<vm_FarmerProfile> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            var _farmerList = BusinessLayerService.FarmerBL().GetByExtensionAgent(extensionAgentCode);
            var _farmerQuotaList = BusinessLayerService.FarmerQuotaBL().GetByExtensionAgent(crop, extensionAgentCode);

            var _list = from a in _farmerList
                        join b in _farmerQuotaList on a.FarmerCode equals b.FarmerCode
                        into x
                        from b in x.DefaultIfEmpty()
                        select new vm_FarmerProfile
                        {
                            Crop = b == null ? user_setting.Crop.Crop1 : b.Crop,
                            FarmerCode = a.FarmerCode,
                            GeneralCode = b == null ? null : b.GeneralCode,
                            ExtensionAgentCode = a.ExtensionAgentCode,
                            ContractCode = b == null ? null : b.ContractCode,
                            Quota = b == null ? 0 : b.Quota,
                            Sold = 0,
                            Bales = 0,
                            AveragePrice = 0,
                            FarmerAddress = a.Person.HouseNumber + " " +
                            a.Person.Village + " " +
                            a.Person.Tumbon + " " +
                            a.Person.Amphur + " " +
                            a.Person.Province,
                            Prefix = a.Person.Prefix,
                            FirstName = a.Person.FirstName,
                            LastName = a.Person.LastName,
                            CitizenID = a.Person.CitizenID,
                            Amphur = a.Person.Amphur,
                            Tumbon = a.Person.Tumbon,
                            Province = a.Person.Province,
                            ModifiedDate = a.Person.ModifiedDate
                        };

            return _list.ToList();
        }

        public static List<vm_FarmerProfile> GetByExtensionAgentLiteVersion(short crop, string extensionAgentCode)
        {
            var _farmers = BusinessLayerService.FarmerQuotaBL()
                .GetByExtensionAgent(crop, extensionAgentCode)
                .Select(x => new vm_FarmerProfile
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    GeneralCode = x.GeneralCode,
                    ExtensionAgentCode = x.Farmer.ExtensionAgentCode,
                    ContractCode = x.ContractCode,
                    Quota = x.Quota,
                    Sold = 0,
                    Bales = 0,
                    AveragePrice = 0,
                    FarmerAddress = x.Farmer.Person.HouseNumber + " " +
                            x.Farmer.Person.Village + " " +
                            x.Farmer.Person.Tumbon + " " +
                            x.Farmer.Person.Amphur + " " +
                            x.Farmer.Person.Province,
                    Prefix = x.Farmer.Person.Prefix,
                    FirstName = x.Farmer.Person.FirstName,
                    LastName = x.Farmer.Person.LastName,
                    CitizenID = x.Farmer.Person.CitizenID,
                    Amphur = x.Farmer.Person.Amphur,
                    Tumbon = x.Farmer.Person.Tumbon,
                    Province = x.Farmer.Person.Province,
                    ModifiedDate = x.Farmer.Person.ModifiedDate
                });

            return _farmers.ToList();
        }
    }
}