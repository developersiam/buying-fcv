﻿using FCVBuyingBL;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class CurerSummaryHelper
    {
        public static List<vm_CurerSummary> GetByCurer(short crop, string extensionAgentCode)
        {
            try
            {
                var quota = BusinessLayerService.FarmerQuotaBL()
                    .GetByExtensionAgent(crop, extensionAgentCode)
                    .Sum(x => x.Quota);
                var buyingList = BusinessLayerService.BuyingBL()
                    .GetByExtensionAgent(crop, extensionAgentCode);

                decimal price = 0;
                if (crop == 2024 || extensionAgentCode.Contains("RX"))
                {
                    //Add the Selling Bonus to each bales to RX curers.
                    decimal rxSellingPrice = 0;
                    foreach (var item in buyingList)
                    {
                        if (string.IsNullOrEmpty(item.Grade))
                            continue;

                        decimal balePrice = 0;
                        decimal finalUnitPrice = 0;

                        if (item.Grade == "CO1" || item.Grade == "CF1")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 15;
                        else if (item.Grade == "CO2" || item.Grade == "CF2")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 13;
                        else if (item.Grade == "CO3" || item.Grade == "CF3")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 10;
                        else if (item.Grade == "BO1" || item.Grade == "BF1")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 18;
                        else if (item.Grade == "BO2" || item.Grade == "BF2")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 15;
                        else if (item.Grade == "BO3" || item.Grade == "BF3")
                            finalUnitPrice = item.BuyingGrade.UnitPrice + 14;
                        else
                            finalUnitPrice = item.BuyingGrade.UnitPrice + (decimal)6.5;

                        balePrice = item.Weight * finalUnitPrice;
                        rxSellingPrice = rxSellingPrice + balePrice;
                    }

                    price = rxSellingPrice;
                }
                else
                {
                    price = buyingList
                        .Where(x => x.Grade != null)
                        .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                }

                decimal weight = buyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight);

                List<vm_CurerSummary> list = new List<vm_CurerSummary>();
                list.Add(new vm_CurerSummary
                {
                    AveragePrice = price / weight,
                    Quota = quota,
                    Sold = weight,
                    Remaining = quota - weight
                });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get accumulative information by specify document number.
        public static List<vm_CurerSummary> GetByCurerV2(short crop, string extensionAgentCode, short documentNumber)
        {
            try
            {
                var quota = BusinessLayerService.FarmerQuotaBL()
                    .GetByExtensionAgent(crop, extensionAgentCode)
                    .Sum(x => x.Quota);
                var buyingList = BusinessLayerService.BuyingBL()
                    .GetByExtensionAgent(crop, extensionAgentCode);

                var list = buyingList
                    .Where(x => x.DocumentNumber <= documentNumber)
                    .ToList();

                decimal price = 0;
                if (crop == 2024)
                {
                    if (extensionAgentCode.Contains("RX"))
                    {
                        //Add the Selling Bonus to each bales to RX curers.
                        decimal rxSellingPrice = 0;
                        foreach (var item in list)
                        {
                            if (string.IsNullOrEmpty(item.Grade))
                                continue;

                            decimal balePrice = 0;
                            decimal finalUnitPrice = 0;

                            if (item.Grade == "CO1" || item.Grade == "CF1")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 15;
                            else if (item.Grade == "CO2" || item.Grade == "CF2")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 13;
                            else if (item.Grade == "CO3" || item.Grade == "CF3")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 10;
                            else if (item.Grade == "BO1" || item.Grade == "BF1")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 18;
                            else if (item.Grade == "BO2" || item.Grade == "BF2")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 15;
                            else if (item.Grade == "BO3" || item.Grade == "BF3")
                                finalUnitPrice = item.BuyingGrade.UnitPrice + 14;
                            else
                                finalUnitPrice = item.BuyingGrade.UnitPrice + (decimal)6.5;

                            balePrice = item.Weight * finalUnitPrice;
                            rxSellingPrice = rxSellingPrice + balePrice;
                        }

                        price = rxSellingPrice;
                    }
                    else
                    {
                        price = buyingList
                            .Where(x => x.Grade != null)
                            .Sum(x => x.Weight * (x.BuyingGrade.UnitPrice + (decimal)6.5));
                    }
                }

                decimal weight = list
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight);

                List<vm_CurerSummary> resultList = new List<vm_CurerSummary>();
                resultList.Add(new vm_CurerSummary
                {
                    AveragePrice = price / weight,
                    Quota = quota,
                    Sold = weight,
                    Remaining = quota - weight
                });

                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
