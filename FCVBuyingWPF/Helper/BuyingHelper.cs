﻿using FCVBuyingBL;
using FCVBuyingWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWPF.Helper
{
    public static class BuyingHelper
    {
        public static List<vm_Buying> GetByDocument(short crop,
            string farmerCode, string stationCode, short documentNumber)
        {
            return BusinessLayerService.BuyingBL()
                .GetByDocument(crop, stationCode, farmerCode, documentNumber)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    LoadBaleToTruckDate = x.LoadBaleToTruckDate,
                    LoadBaleToTruckUser = x.LoadBaleToTruckUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality
                }).ToList();
        }

        public static List<vm_Buying> GetByDate(DateTime documentDate, string stationCode, string extensionAgentCode)
        {
            return BusinessLayerService.BuyingBL()
                .GetByDocumentDate(documentDate)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    LoadBaleToTruckDate = x.LoadBaleToTruckDate,
                    LoadBaleToTruckUser = x.LoadBaleToTruckUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality
                }).ToList();
        }

        public static List<vm_Buying> GetByTransportationCode(string transportationCode)
        {
            var list = BusinessLayerService.BuyingBL()
                .GetByTransportationDocument(transportationCode)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    LoadBaleToTruckDate = x.LoadBaleToTruckDate,
                    LoadBaleToTruckUser = x.LoadBaleToTruckUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality,
                }).ToList();

            return list;
        }

        public static List<vm_Buying> GetByMoistureResultDate(DateTime resultDate)
        {
            return BusinessLayerService.BuyingBL()
                .GetMoistureResultByResultDate(resultDate)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    LoadBaleToTruckDate = x.LoadBaleToTruckDate,
                    LoadBaleToTruckUser = x.LoadBaleToTruckUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality,
                    Position = x.Grade.Substring(0, 1),
                    ExtensionAgentCode = x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode,
                    MoistureResult = x.MoistureResult,
                    MoistureResultDate = x.MoistureResultDate,
                    MoistureResultUser = x.MoistureResultUser
                }).ToList();
        }

        public static List<vm_Buying> GetMoistureResultByDateRange(DateTime from, DateTime to)
        {
            try
            {
                return BusinessLayerService.BuyingBL()
                .GetMoistureResultByDateRange(from, to)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    LoadBaleToTruckDate = x.LoadBaleToTruckDate,
                    LoadBaleToTruckUser = x.LoadBaleToTruckUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode.Contains("NPI") ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality,
                    Position = x.Grade == null ? "" : x.Grade.Substring(0, 1),
                    ExtensionAgentCode = x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode,
                    MoistureResult = x.MoistureResult,
                    MoistureResultDate = x.MoistureResultDate,
                    MoistureResultUser = x.MoistureResultUser
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
