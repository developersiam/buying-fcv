﻿using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingDAL
{
    public interface IUnitOfWork
    {
        IGenericDataRepository<Area> AreaRepository { get; }
        IGenericDataRepository<BaleType> BaleTypeRepository { get; }
        IGenericDataRepository<Buying> BuyingRepository { get; }
        IGenericDataRepository<BuyingDocument> BuyingDocumentRepository { get; }
        IGenericDataRepository<BuyingGrade> BuyingGradeRepository { get; }
        IGenericDataRepository<BuyingGradeGroup> BuyingGradeGroupRepository { get; }
        IGenericDataRepository<BuyingStation> BuyingStationRepository { get; }
        IGenericDataRepository<Crop> CropRepository { get; }
        IGenericDataRepository<ExtensionAgent> ExtensionAgentRepository { get; }
        IGenericDataRepository<ExtensionAgentBuyingArea> ExtensionAgentBuyingAreaRepository { get; }
        IGenericDataRepository<Farmer> FarmerRepository { get; }
        IGenericDataRepository<FarmerQuota> FarmerQuotaRepository { get; }
        IGenericDataRepository<MasterGrade> MasterGradeRepository { get; }
        IGenericDataRepository<NTRMInspection> NTRMInspectionRepository { get; }
        IGenericDataRepository<NTRMType> NTRMTypeRepository { get; }
        IGenericDataRepository<Person> PersonRepository { get; }
        IGenericDataRepository<RejectType> RejectTypeRepository { get; }
        IGenericDataRepository<ReplaceType> ReplaceTypeRepository { get; }
        IGenericDataRepository<Role> RoleRepository { get; }
        IGenericDataRepository<TransportationDocument> TransportationDocumentRepository { get; }
        IGenericDataRepository<User> UserRepository { get; }
        IGenericDataRepository<UserRole> UserRoleRepository { get; }
        IGenericDataRepository<UserUnderExtensionAgent> UserUnderExtensionAgentRepository { get; }
        IGenericDataRepository<UserArea> UserAreaRepository { get; }
        void Save();
    }
}
