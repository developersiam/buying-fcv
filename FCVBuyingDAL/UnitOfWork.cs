﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCVBuyingEntities;
using System.Data.Entity;

namespace FCVBuyingDAL
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly FCVBuyingSystemEntities _context;
        private IGenericDataRepository<Area> _areaRepository;
        private IGenericDataRepository<BaleType> _baleTypeRepository;
        private IGenericDataRepository<Buying> _buyingRepository;
        private IGenericDataRepository<BuyingDocument> _buyingDocumentRepository;
        private IGenericDataRepository<BuyingGrade> _buyingGradeRepository;
        private IGenericDataRepository<BuyingGradeGroup> _buyingGradeGroupRepository;
        private IGenericDataRepository<BuyingStation> _buyingStationRepository;
        private IGenericDataRepository<Crop> _cropRepository;
        private IGenericDataRepository<ExtensionAgent> _extensionAgentRepository;
        private IGenericDataRepository<ExtensionAgentBuyingArea> _extensionAgentBuyingAreaRepository;
        private IGenericDataRepository<Farmer> _farmerRepository;
        private IGenericDataRepository<FarmerQuota> _farmerQuotaRepository;
        private IGenericDataRepository<MasterGrade> _masterGradeRepository;
        private IGenericDataRepository<Person> _personRepository;
        private IGenericDataRepository<NTRMInspection> _ntrmInspectionRepository;
        private IGenericDataRepository<NTRMType> _ntrmTypeRepository;
        private IGenericDataRepository<RejectType> _rejectTypeRepository;
        private IGenericDataRepository<ReplaceType> _replaceTypeRepository;
        private IGenericDataRepository<Role> _roleRepository;
        private IGenericDataRepository<TransportationDocument> _transportationDocumentRepository;
        private IGenericDataRepository<User> _userRepository;
        private IGenericDataRepository<UserRole> _userRoleRepository;
        private IGenericDataRepository<UserUnderExtensionAgent> _userUnderExtensionAgentRepository;
        private IGenericDataRepository<UserArea> _userAreaRepository;

        public UnitOfWork()
        {
            _context = new FCVBuyingSystemEntities();
        }

        public IGenericDataRepository<BaleType> BaleTypeRepository
        {
            get
            {
                return _baleTypeRepository ?? (_baleTypeRepository = new GenericDataRepository<BaleType>(_context));
            }
        }
        public IGenericDataRepository<Buying> BuyingRepository
        {
            get
            {
                return _buyingRepository ?? (_buyingRepository = new GenericDataRepository<Buying>(_context));
            }
        }
        public IGenericDataRepository<BuyingDocument> BuyingDocumentRepository
        {
            get
            {
                return _buyingDocumentRepository ?? (_buyingDocumentRepository = new GenericDataRepository<BuyingDocument>(_context));
            }
        }
        public IGenericDataRepository<BuyingGrade> BuyingGradeRepository
        {
            get
            {
                return _buyingGradeRepository ?? (_buyingGradeRepository = new GenericDataRepository<BuyingGrade>(_context));
            }
        }
        public IGenericDataRepository<BuyingGradeGroup> BuyingGradeGroupRepository
        {
            get
            {
                return _buyingGradeGroupRepository ?? (_buyingGradeGroupRepository = new GenericDataRepository<BuyingGradeGroup>(_context));
            }
        }
        public IGenericDataRepository<BuyingStation> BuyingStationRepository
        {
            get
            {
                return _buyingStationRepository ?? (_buyingStationRepository = new GenericDataRepository<BuyingStation>(_context));
            }
        }
        public IGenericDataRepository<Crop> CropRepository
        {
            get
            {
                return _cropRepository ?? (_cropRepository = new GenericDataRepository<Crop>(_context));
            }
        }
        public IGenericDataRepository<ExtensionAgent> ExtensionAgentRepository
        {
            get
            {
                return _extensionAgentRepository ?? (_extensionAgentRepository = new GenericDataRepository<ExtensionAgent>(_context));
            }
        }
        public IGenericDataRepository<Farmer> FarmerRepository
        {
            get
            {
                return _farmerRepository ?? (_farmerRepository = new GenericDataRepository<Farmer>(_context));
            }
        }
        public IGenericDataRepository<FarmerQuota> FarmerQuotaRepository
        {
            get
            {
                return _farmerQuotaRepository ?? (_farmerQuotaRepository = new GenericDataRepository<FarmerQuota>(_context));
            }
        }
        public IGenericDataRepository<MasterGrade> MasterGradeRepository
        {
            get
            {
                return _masterGradeRepository ?? (_masterGradeRepository = new GenericDataRepository<MasterGrade>(_context));
            }
        }
        public IGenericDataRepository<Person> PersonRepository
        {
            get
            {
                return _personRepository ?? (_personRepository = new GenericDataRepository<Person>(_context));
            }
        }
        public IGenericDataRepository<NTRMInspection> NTRMInspectionRepository
        {
            get
            {
                return _ntrmInspectionRepository ?? (_ntrmInspectionRepository = new GenericDataRepository<NTRMInspection>(_context));
            }
        }
        public IGenericDataRepository<NTRMType> NTRMTypeRepository
        {
            get
            {
                return _ntrmTypeRepository ?? (_ntrmTypeRepository = new GenericDataRepository<NTRMType>(_context));
            }
        }
        public IGenericDataRepository<RejectType> RejectTypeRepository
        {
            get
            {
                return _rejectTypeRepository ?? (_rejectTypeRepository = new GenericDataRepository<RejectType>(_context));
            }
        }
        public IGenericDataRepository<ReplaceType> ReplaceTypeRepository
        {
            get
            {
                return _replaceTypeRepository ?? (_replaceTypeRepository = new GenericDataRepository<ReplaceType>(_context));
            }
        }

        public IGenericDataRepository<User> UserRepository
        {
            get
            {
                return _userRepository ?? (_userRepository = new GenericDataRepository<User>(_context));
            }
        }

        public IGenericDataRepository<Role> RoleRepository
        {
            get
            {
                return _roleRepository ?? (_roleRepository = new GenericDataRepository<Role>(_context));
            }
        }

        public IGenericDataRepository<UserRole> UserRoleRepository
        {
            get
            {
                return _userRoleRepository ?? (_userRoleRepository = new GenericDataRepository<UserRole>(_context));
            }
        }

        public IGenericDataRepository<UserUnderExtensionAgent> UserUnderExtensionAgentRepository
        {
            get
            {
                return _userUnderExtensionAgentRepository ?? (_userUnderExtensionAgentRepository = new GenericDataRepository<UserUnderExtensionAgent>(_context));
            }
        }

        public IGenericDataRepository<UserArea> UserAreaRepository
        {
            get
            {
                return _userAreaRepository ?? (_userAreaRepository = new GenericDataRepository<UserArea>(_context));
            }
        }

        public IGenericDataRepository<Area> AreaRepository
        {
            get
            {
                return _areaRepository ?? (_areaRepository = new GenericDataRepository<Area>(_context));
            }
        }

        public IGenericDataRepository<ExtensionAgentBuyingArea> ExtensionAgentBuyingAreaRepository
        {
            get
            {
                return _extensionAgentBuyingAreaRepository ?? (_extensionAgentBuyingAreaRepository = new GenericDataRepository<ExtensionAgentBuyingArea>(_context));
            }
        }

        public IGenericDataRepository<TransportationDocument> TransportationDocumentRepository
        {
            get
            {
                return _transportationDocumentRepository ?? (_transportationDocumentRepository = new GenericDataRepository<TransportationDocument>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _context.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}