﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IUserAreaBL
    {
        void Add(UserArea model);
        void Delete(UserArea model);
        UserArea GetById(string username, string areaCode);
        List<UserArea> GetByArea(string areaCode);
        List<UserArea> GetByUser(string username);
    }

    public class UserAreaBL : IUserAreaBL
    {
        UnitOfWork _unitOfWork;
        public UserAreaBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(UserArea model)
        {
            try
            {
                if (GetById(model.Username, model.AreaCode) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                _unitOfWork.UserAreaRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(UserArea model)
        {
            try
            {
                var deleteModel = GetById(model.Username, model.AreaCode);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.UserAreaRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserArea> GetByArea(string areaCode)
        {
            return _unitOfWork.UserAreaRepository
                .Get(x => x.AreaCode == areaCode,
                null, x => x.User, x => x.Area);
        }

        public UserArea GetById(string username, string areaCode)
        {
            return _unitOfWork.UserAreaRepository
               .GetSingle(x => x.Username == username && x.AreaCode == areaCode);
        }

        public List<UserArea> GetByUser(string username)
        {
            return _unitOfWork.UserAreaRepository
                .Get(x => x.Username == username,
                null,
                x => x.User,
                x => x.Area);
        }
    }
}
