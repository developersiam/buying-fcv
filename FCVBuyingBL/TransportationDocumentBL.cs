﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface ITransportationDocumentBL
    {
        void BringUp(string baleBarcode, string recordingUser, string transportationCode);
        void BringDown(string baleBarcode);
        string Add(short crop, string truckNo, DateTime createDate, string areaCode, string recordingUser);
        void Print(string transportationCode);
        void Delete(string transportationCode);
        void Update(TransportationDocument transportationDocument);
        void Finish(string transportationCode);
        void UnFinish(string transportationCode);
        List<TransportationDocument> GetByCrop(short crop);
        List<TransportationDocument> GetByCreateDate(DateTime createDate);
        List<TransportationDocument> GetByCreateDateAndArea(DateTime createDate, string areaCode);
        TransportationDocument GetSingle(string transportationCode);
    }

    public class TransportationDocumentBL : ITransportationDocumentBL
    {
        UnitOfWork uow;
        public TransportationDocumentBL()
        {
            uow = new UnitOfWork();
        }

        public void BringUp(string baleBarcode, string recordingUser, string transportationCode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new Exception("ไม่สามารถอ่านหมายเลขบาร์โค้ตนี้ได้ โปรดลองอีกครั้ง หรือติดต่อแผนกไอที");

                if (recordingUser == null)
                    throw new Exception("ไม่พบข้อมูลผู้บันทึก โปรดติดต่อแผนกไอที");

                var buying = uow.BuyingRepository.GetSingle(b => b.BaleBarcode == baleBarcode);

                if (buying == null)
                    throw new Exception("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (transportationCode == null)
                    throw new Exception("ไม่พบข้อมูลรหัสเอกสาร โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var model = GetSingle(transportationCode);

                if (model == null)
                    throw new Exception("ไม่พบเอกสารหมายเลข " + transportationCode + " โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบ");

                if (model.IsFinish == true)
                    throw new Exception("ใบนำส่งนี้ถูก Lock แล้ว ไม่สามารถดำเนินการได้ หากท่านต้องการดำเนินการใดๆ ให้ปลดล็อคเอกสารก่อน");

                buying.TransportationCode = transportationCode;
                buying.LoadBaleToTruckDate = DateTime.Now;
                buying.LoadBaleToTruckUser = recordingUser;
                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BringDown(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new Exception("ไม่สามารถอ่านป้ายหมายเลขบาร์โค้ตนี้ได้ โปรดลองสแกนใหม่อีกครั้ง");

                var buying = uow.BuyingRepository.GetSingle(b => b.BaleBarcode == baleBarcode);

                if (buying == null)
                    throw new Exception("ไม่พบข้อมูลห่อยานี้ในระบบ โปรดลองใหม่อีกครั้ง");

                if (buying.TransportationCode == null)
                    throw new Exception("ห่อยานี้ยังไม่ถูกบันทึกข้อมูลการขนขึ้นรถบรรทุก จึงไม่สามารถลบออกได้ โปรดตรวจสอบใหม่อีกครั้ง");

                var model = uow.TransportationDocumentRepository
                    .GetSingle(tr => tr.TransportationCode == buying.TransportationCode);

                if (model == null)
                    throw new Exception("ไม่พบเอกสารหมายเลข " + buying.TransportationCode + " ในระบบ โปรดตรวจสอบอีกครั้ง");

                if (model.IsFinish == true)
                    throw new Exception("ใบนำส่งนี้ถูก Lock แล้ว ไม่สามารถดำเนินการได้ หากท่านต้องการดำเนินการใดๆ ให้ปลดล็อคเอกสารก่อน");

                buying.TransportationCode = null;
                buying.LoadBaleToTruckDate = null;
                buying.LoadBaleToTruckUser = null;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(short crop, string truckNo, DateTime createDate, string areaCode, string recordingUser)
        {
            try
            {
                if (createDate == null)
                    throw new Exception("โปรดระบุวันที่สร้างเอกสาร");

                if (recordingUser == null)
                    throw new Exception("ไม่พบข้อมูลผู้ใช้งานระบบ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบ");

                if (string.IsNullOrEmpty(areaCode))
                    throw new Exception("Area code cannot be empty.");

                var prefix = "TR";

                var list = GetByCrop(crop)
                    .Select(x => new
                    {
                        runningNumber = Convert.ToInt32(x.TransportationCode
                        .Substring(x.TransportationCode.Length - 3, 3))
                    }).ToList();

                var max = list.Count > 0 ? list.Max(x => x.runningNumber) : 0;

                var transportationCode = prefix +
                    "-" +
                    createDate.Year +
                    createDate.Month.ToString().PadLeft(2, '0') +
                    createDate.Day.ToString().PadLeft(2, '0') +
                    "-" +
                    (max + 1).ToString().PadLeft(3, '0');

                TransportationDocument transportationDocument = new TransportationDocument
                {
                    TransportationCode = transportationCode,
                    Crop = crop,
                    TruckNo = truckNo == null ? "-" : truckNo,
                    CreateDate = createDate,
                    CreateUser = recordingUser,
                    AreaCode = areaCode,
                    IsFinish = false,
                    FinishDate = null,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = recordingUser
                };
                uow.TransportationDocumentRepository.Add(transportationDocument);
                uow.Save();

                return transportationCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Print(string transportationCode)
        {
            throw new NotImplementedException();
        }

        public void Delete(string transportationCode)
        {
            try
            {
                if (transportationCode == null)
                    throw new Exception("ไม่พบรหัสเอกสาร โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var model = GetSingle(transportationCode);

                if (model == null)
                    throw new Exception("ไม่พบข้อมูลใบนำส่งนี้ในระบบ โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (model.IsFinish == true)
                    throw new Exception("เอกสารนนี้ถูกเซ็ตเป็น Lock ไม่สามารถลบข้อมูลได้ หากท่านต้องการลบข้อมูล ให้กลับไปปลดล็อคก่อน");

                var buyingList = uow.BuyingRepository.Get(b => b.TransportationCode == transportationCode).ToList();

                if (buyingList.Count > 0)
                    throw new Exception("ใบนำส่งนี้มีข้อมูลห่อยาที่บันทึกขึ้นรถอยู่มากกว่า 1 ห่อ ไม่สามารถลบใบนำส่งนี้ได้");

                uow.TransportationDocumentRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(TransportationDocument transportationDocument)
        {
            try
            {
                if (transportationDocument.TruckNo == null)
                    throw new Exception("โปรดระบุหมายเลขทะเบียนรถ");

                if (transportationDocument.ModifiedUser == null)
                    throw new Exception("ไม่พบข้อมูลผู้ใช้นี้ในระบบ โปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบ");

                if (transportationDocument.IsFinish == true)
                    throw new Exception("เอกสารนนี้ถูกเซ็ตเป็น Lock ไม่สามารถลบข้อมูลได้ หากท่านต้องการลบข้อมูล ให้กลับไปปลดล็อคก่อน");

                uow.TransportationDocumentRepository.Update(transportationDocument);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string transportationCode)
        {
            try
            {
                if (transportationCode == null)
                    throw new Exception("ไม่พบข้อมูลรหัสเอกสาร โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var model = GetSingle(transportationCode);

                if (model == null)
                    throw new Exception("ไม่พบเอกสารหมายเลข " + transportationCode + " โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบ");

                model.IsFinish = true;
                model.FinishDate = DateTime.Now;

                uow.TransportationDocumentRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnFinish(string transportationCode)
        {
            try
            {
                if (transportationCode == null)
                    throw new Exception("ไม่พบข้อมูลรหัสเอกสาร โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var model = GetSingle(transportationCode);

                if (model == null)
                    throw new Exception("ไม่พบเอกสารหมายเลข " + transportationCode + " โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบ");

                model.IsFinish = false;
                model.FinishDate = null;

                uow.TransportationDocumentRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TransportationDocument> GetByCrop(short crop)
        {
            return uow.TransportationDocumentRepository
                .Get(t => t.Crop == crop).ToList();
        }

        public List<TransportationDocument> GetByCreateDate(DateTime createDate)
        {
            return uow.TransportationDocumentRepository
                    .Get(t => t.CreateDate.Day == createDate.Day &&
                    t.CreateDate.Month == createDate.Month &&
                    t.CreateDate.Year == createDate.Year,
                    null,
                    t => t.Buyings,
                    t => t.Area)
                    .ToList();
        }

        public TransportationDocument GetSingle(string transportationCode)
        {
            return uow.TransportationDocumentRepository
                .GetSingle(t => t.TransportationCode == transportationCode,
                t => t.Buyings);
        }

        public List<TransportationDocument> GetByCreateDateAndArea(DateTime createDate, string areaCode)
        {
            return uow.TransportationDocumentRepository
                .Get(x => x.CreateDate.Day == createDate.Day &&
                x.CreateDate.Month == createDate.Month &&
                x.CreateDate.Year == createDate.Year &&
                x.AreaCode == areaCode);
        }
    }
}
