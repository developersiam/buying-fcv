﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCVBuyingDAL;
using FCVBuyingEntities;

namespace FCVBuyingBL
{
    public interface IBuyingStationBL
    {
        void Add(BuyingStation model);
        void Delete(string stationCode);
        void Edit(BuyingStation model);
        BuyingStation GetSingle(string stationCode);
        List<BuyingStation> GetAll();
        List<BuyingStation> GetByArea(string areaCode);
    }

    public class BuyingStationBL : IBuyingStationBL
    {
        UnitOfWork _unitOfWork;
        public BuyingStationBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(BuyingStation model)
        {
            try
            {
                if (_unitOfWork.BuyingStationRepository
                    .Get(x => x.StationName.Contains(model.StationName)).Count > 0)
                    throw new ArgumentException("This station name is in the system already.");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;
                model.ModifiedUser = model.CreateUser;

                _unitOfWork.BuyingStationRepository.Add(model);
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string stationCode)
        {
            try
            {
                var deleteModel = _unitOfWork.BuyingStationRepository
                    .GetSingle(x => x.StationCode == stationCode);

                if (deleteModel == null)
                    throw new ArgumentException("Find not found.");

                _unitOfWork.BuyingStationRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(BuyingStation model)
        {
            try
            {
                var editModel =_unitOfWork.BuyingStationRepository
                    .GetSingle(x => x.StationCode == model.StationCode);

                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                if (_unitOfWork.BuyingStationRepository
                    .Get(x => x.StationName.Contains(model.StationName)).Count > 0)
                    throw new ArgumentException("This station name is in the system already.");

                _unitOfWork.BuyingStationRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BuyingStation GetSingle(string stationCode)
        {
            return _unitOfWork.BuyingStationRepository.GetSingle(x => x.StationCode == stationCode);
        }

        public List<BuyingStation> GetByArea(string areaCode)
        {
            return _unitOfWork.BuyingStationRepository.Get(x=>x.AreaCode == areaCode);
        }

        public List<BuyingStation> GetAll()
        {
            return _unitOfWork.BuyingStationRepository.Get();
        }
    }
}
