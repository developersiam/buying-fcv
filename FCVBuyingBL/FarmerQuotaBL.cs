﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IFarmerQuotaBL
    {
        void Add(FarmerQuota model);
        void Delete(short crop, string farmerCode);
        void Edit(FarmerQuota model);
        FarmerQuota GetSingle(short crop, string farmerCode);
        FarmerQuota GetByContractCode(string contractCode);
        List<FarmerQuota> GetByCrop(short crop);
        List<FarmerQuota> GetByExtensionAgent(short crop, string extensionAgentCode);
        List<FarmerQuota> GetByCropAndCitizenID(short crop, string citizenID);
        List<FarmerQuota> GetByCitizenID(string citizenID);
    }

    public class FarmerQuotaBL : IFarmerQuotaBL
    {
        UnitOfWork _unitOfWork;
        public FarmerQuotaBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(FarmerQuota model)
        {
            try
            {
                if (_unitOfWork.FarmerQuotaRepository
                    .GetSingle(x => x.Crop == model.Crop
                    && x.FarmerCode == model.FarmerCode) != null)
                    throw new ArgumentException("มีข้อมูลโควต้าของชาวไร่รายนี้แล้วในระบบ");

                if (model.Quota <= 0)
                    throw new ArgumentException("จำนวนโควต้าที่ระบุจะต้องมากกว่า 0");

                model.RegisterDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                _unitOfWork.FarmerQuotaRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(short crop, string farmerCode)
        {
            try
            {
                var deleteModel = GetSingle(crop, farmerCode);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (_unitOfWork.BuyingRepository.Get(x => x.Crop == crop
                 && x.FarmerCode == farmerCode).Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้มีข้อมูลการซื้อขายใบยาในระบบ ไม่สามารถลบข้อมูลโควต้าได้");

                _unitOfWork.FarmerQuotaRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(FarmerQuota model)
        {
            try
            {
                var editModel = GetSingle(model.Crop, model.FarmerCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (editModel.Quota <= 0)
                    throw new ArgumentException("จำนวนโควต้าที่ระบุจะต้องมากกว่า 0");

                editModel.GeneralCode = model.GeneralCode;
                editModel.ContractCode = model.ContractCode;
                editModel.Quota = model.Quota;
                editModel.ModifiedDate = DateTime.Now;
                editModel.ModifiedUser = model.ModifiedUser;

                _unitOfWork.FarmerQuotaRepository.Update(editModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public FarmerQuota GetSingle(short crop, string farmerCode)
        {
            return _unitOfWork.FarmerQuotaRepository
                .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);
        }
        public List<FarmerQuota> GetByCrop(short crop)
        {
            return _unitOfWork.FarmerQuotaRepository
                .Get(x => x.Crop == crop && x.Crop == crop);
        }
        public List<FarmerQuota> GetByCitizenID(string citizenID)
        {
            return _unitOfWork.FarmerQuotaRepository.Get(x => x.Farmer.CitizenID == citizenID);
        }
        public FarmerQuota GetByContractCode(string contractCode)
        {
            return _unitOfWork.FarmerQuotaRepository.GetSingle(x => x.ContractCode == contractCode);
        }
        public List<FarmerQuota> GetByCropAndCitizenID(short crop, string citizenID)
        {
            return _unitOfWork.FarmerQuotaRepository
                .Get(x => x.Crop == crop && x.Farmer.CitizenID == citizenID);
        }
        public List<FarmerQuota> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return _unitOfWork.FarmerQuotaRepository
                .Get(x => x.Crop == crop &&
                x.Farmer.ExtensionAgentCode == extensionAgentCode,
                null,
                x => x.Farmer,
                x => x.Farmer.Person);
        }
    }
}
