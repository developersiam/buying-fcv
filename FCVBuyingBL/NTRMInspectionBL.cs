﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface INTRMInspectionBL
    {
        void Add(NTRMInspection model);
        void Delete(NTRMInspection model);
        List<NTRMInspection> GetByBaleBarcode(string baleBarcode);
    }
    public class NTRMInspectionBL : INTRMInspectionBL
    {
        UnitOfWork _unitOfWork;

        public NTRMInspectionBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(NTRMInspection model)
        {
            try
            {
                if (_unitOfWork.NTRMInspectionRepository
                    .GetSingle(x => x.BaleBarcode == model.BaleBarcode
                    && x.NTRMTypeCode == model.NTRMTypeCode) != null)
                    throw new ArgumentException("มีข้อมูลนี้แล้วในระบบ");

                _unitOfWork.NTRMInspectionRepository.Add(model);
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(NTRMInspection model)
        {
            try
            {
                if (_unitOfWork.NTRMInspectionRepository
                    .GetSingle(x => x.BaleBarcode == model.BaleBarcode 
                    && x.NTRMTypeCode == model.NTRMTypeCode) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _unitOfWork.NTRMInspectionRepository.Remove(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NTRMInspection> GetByBaleBarcode(string baleBarcode)
        {
            return _unitOfWork.NTRMInspectionRepository
                .Get(x => x.BaleBarcode == baleBarcode, 
                null, x => x.NTRMType);
        }
    }
}
