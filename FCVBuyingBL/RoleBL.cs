﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IRoleBL
    {
        void Add(Role model);
        void Edit(Role model);
        void Delete(Role model);
        Role GetById(Guid roleId);
        List<Role> GetAll();
    }
    public class RoleBL : IRoleBL
    {
        UnitOfWork _unitOfWork;

        public RoleBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(Role model)
        {
            try
            {
                if (_unitOfWork.RoleRepository
                    .Get(x=>x.RoleName == model.RoleName).Count > 0)
                    throw new ArgumentException("มีชื่อ role นี้ซ้ำแล้วในระบบ");

                model.ModifiedUser = model.CreateUser;
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                _unitOfWork.RoleRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Role model)
        {
            try
            {
                var deleteModel = GetById(model.RoleID);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.RoleRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Role model)
        {
            try
            {
                if (GetById(model.RoleID) == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                model.ModifiedDate = DateTime.Now;

                _unitOfWork.RoleRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Role> GetAll()
        {
            return _unitOfWork.RoleRepository.Get();
        }

        public Role GetById(Guid roleId)
        {
            return _unitOfWork.RoleRepository.GetSingle(x => x.RoleID == roleId);
        }
    }
}
