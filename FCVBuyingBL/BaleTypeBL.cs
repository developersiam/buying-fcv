﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IBaleTypeBL
    {
        BaleType GetBaleType(int baleTypeID);
        List<BaleType> GetBaleTypes();
        void Add(BaleType model);
        void Edit(BaleType model);
        void Delete(int baleTypeID);
    }

    public class BaleTypeBL : IBaleTypeBL
    {
        UnitOfWork _unitOfWork;
        public BaleTypeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(BaleType model)
        {
            try
            {
                if (_unitOfWork.BaleTypeRepository
                    .Get(x => x.BaleTypeName.Contains(model.BaleTypeName)).Count() > 0)
                    throw new ArgumentException("This bale type name is in the system already.");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;

                _unitOfWork.BaleTypeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(int baleTypeID)
        {
            try
            {
                var model = GetBaleType(baleTypeID);
                if (model == null)
                    throw new ArgumentException("Find not found.");

                _unitOfWork.BaleTypeRepository.Remove(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(BaleType model)
        {
            try
            {
                var editModel = GetBaleType(model.BaleTypeID);
                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                editModel.BaleTypeName = model.BaleTypeName;
                editModel.ModifiedDate = DateTime.Now;
                editModel.ModifiedUser = model.ModifiedUser;

                _unitOfWork.BaleTypeRepository.Update(editModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BaleType GetBaleType(int baleTypeID)
        {
            return _unitOfWork.BaleTypeRepository.GetSingle(x => x.BaleTypeID == baleTypeID);
        }
        public List<BaleType> GetBaleTypes()
        {
            return _unitOfWork.BaleTypeRepository.Get();
        }
    }
}
