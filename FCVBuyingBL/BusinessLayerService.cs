﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public static class BusinessLayerService
    {
        public static IAreaBL AreaBL()
        {
            IAreaBL obj = new AreaBL();
            return obj;
        }
        public static IBaleTypeBL BaleTypeBL()
        {
            IBaleTypeBL obj = new BaleTypeBL();
            return obj;
        }
        public static IBuyingBL BuyingBL()
        {
            IBuyingBL obj = new BuyingBL();
            return obj;
        }
        public static IBuyingDocumentBL BuyingDocumentBL()
        {
            IBuyingDocumentBL obj = new BuyingDocumentBL();
            return obj;
        }
        public static IBuyingGradeBL BuyingGradeBL()
        {
            IBuyingGradeBL obj = new BuyingGradeBL();
            return obj;
        }
        public static IBuyingGradeGroupBL BuyingGradeGroupBL()
        {
            IBuyingGradeGroupBL obj = new BuyingGradeGroupBL();
            return obj;
        }
        public static IBuyingStationBL BuyingStationBL()
        {
            IBuyingStationBL obj = new BuyingStationBL();
            return obj;
        }
        public static ICropBL CropBL()
        {
            ICropBL obj = new CropBL();
            return obj;
        }
        public static IExtensionAgentBL ExtensionAgentBL()
        {
            IExtensionAgentBL obj = new ExtensionAgentBL();
            return obj;
        }
        public static IFarmerBL FarmerBL()
        {
            IFarmerBL obj = new FarmerBL();
            return obj;
        }
        public static IFarmerQuotaBL FarmerQuotaBL()
        {
            IFarmerQuotaBL obj = new FarmerQuotaBL();
            return obj;
        }
        public static IMasterGradeBL MasterGradeBL()
        {
            IMasterGradeBL obj = new MasterGradeBL();
            return obj;
        }
        public static INTRMInspectionBL NTRMInspectionBL()
        {
            INTRMInspectionBL obj = new NTRMInspectionBL();
            return obj;
        }
        public static INTRMTypeBL NTRMTypeBL()
        {
            INTRMTypeBL obj = new NTRMTypeBL();
            return obj;
        }
        public static IPersonBL PersonBL()
        {
            IPersonBL obj = new PersonBL();
            return obj;
        }
        public static IRejectTypeBL RejectTypeBL()
        {
            IRejectTypeBL obj = new RejectTypeBL();
            return obj;
        }
        public static IRoleBL RoleBL()
        {
            IRoleBL obj = new RoleBL();
            return obj;
        }
        public static ITransportationDocumentBL TransportationDocumentBL()
        {
            ITransportationDocumentBL obj = new TransportationDocumentBL();
            return obj;
        }
        public static IUserBL UserBL()
        {
            IUserBL obj = new UserBL();
            return obj;
        }
        public static IUserRoleBL UserRoleBL()
        {
            IUserRoleBL obj = new UserRoleBL();
            return obj;
        }
        public static IUserUnderExtensionAgentBL UserUnderExtensionAgentBL()
        {
            IUserUnderExtensionAgentBL obj = new UserUnderExtensionAgentBL();
            return obj;
        }
        public static IUserAreaBL UserAreaBL()
        {
            IUserAreaBL obj = new UserAreaBL();
            return obj;
        }
        public static IExtensionAgentBuyingAreaBL ExtensionAgentBuyingAreaBL()
        {
            IExtensionAgentBuyingAreaBL obj = new ExtensionAgentBuyingAreaBL();
            return obj;
        }
    }
}
