﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IExtensionAgentBuyingAreaBL
    {
        void Add(ExtensionAgentBuyingArea model);
        void Delete(ExtensionAgentBuyingArea model);
        ExtensionAgentBuyingArea GetSingle(string areaCode, string extensionAgentCode);
        List<ExtensionAgentBuyingArea> GetByArea(string areaCode);
        List<ExtensionAgentBuyingArea> GetByExtensionAgent(string extensionAgentCode);
    }

    public class ExtensionAgentBuyingAreaBL : IExtensionAgentBuyingAreaBL
    {
        UnitOfWork _unitOfWork;
        public ExtensionAgentBuyingAreaBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(ExtensionAgentBuyingArea model)
        {
            try
            {
                if (GetSingle(model.AreaCode, model.ExtensionAgentCode) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;

                _unitOfWork.ExtensionAgentBuyingAreaRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(ExtensionAgentBuyingArea model)
        {
            try
            {
                var deleteModel = GetSingle(model.AreaCode, model.ExtensionAgentCode);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.ExtensionAgentBuyingAreaRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ExtensionAgentBuyingArea> GetByArea(string areaCode)
        {
            return _unitOfWork.ExtensionAgentBuyingAreaRepository
                .Get(x => x.AreaCode == areaCode,
                null, x => x.ExtensionAgent);
        }

        public List<ExtensionAgentBuyingArea> GetByExtensionAgent(string extensionAgentCode)
        {
            return _unitOfWork.ExtensionAgentBuyingAreaRepository
                .Get(x => x.ExtensionAgentCode == extensionAgentCode);
        }

        public ExtensionAgentBuyingArea GetSingle(string areaCode, string extensionAgentCode)
        {
            return _unitOfWork.ExtensionAgentBuyingAreaRepository
                .GetSingle(x => x.AreaCode == areaCode && x.ExtensionAgentCode == extensionAgentCode);
        }
    }
}
