﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCVBuyingEntities;
using FCVBuyingDAL;

namespace FCVBuyingBL
{
    public interface IPersonBL
    {
        void Add(Person model);
        void Delete(string citizenID);
        void Edit(Person model);
        Person GetPerson(string citizenID);
        List<Person> GetPersons();
    }

    public class PersonBL : IPersonBL
    {
        UnitOfWork _unitOfWork;
        public PersonBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(Person model)
        {
            try
            {
                var deleteModel = _unitOfWork.PersonRepository.GetSingle(x => x.CitizenID == model.CitizenID);
                if (deleteModel != null)
                    throw new ArgumentException("มีข้อมูลชาวไร่รายนี้แล้วในระบบ");

                _unitOfWork.PersonRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string citizenID)
        {
            try
            {
                var deleteModel = _unitOfWork.PersonRepository.GetSingle(x => x.CitizenID == citizenID);

                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายนี้ในระบบ");

                if (_unitOfWork.FarmerRepository
                    .Get(x => x.CitizenID == deleteModel.CitizenID).Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้มีข้อมูลการเป็นสมาชิกกับตัวแทนในระบบ " +
                        "ไม่สามารถลบชาวไร่รายนี้ออกจากระบบได้");

                _unitOfWork.PersonRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(Person model)
        {
            try
            {
                var editModel = _unitOfWork.PersonRepository.GetSingle(x => x.CitizenID == model.CitizenID);

                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายนี้ในระบบ");

                _unitOfWork.PersonRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Person GetPerson(string citizenID)
        {
            return _unitOfWork.PersonRepository.GetSingle(x => x.CitizenID == citizenID);
        }
        public List<Person> GetPersons()
        {
            return _unitOfWork.PersonRepository.Get();
        }
    }
}
