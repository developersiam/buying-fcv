﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IRejectTypeBL
    {
        void Add(RejectType model);
        void Delete(Guid rejectTypeId);
        void Edit(RejectType model);
        RejectType GetRejectType(Guid rejectTypeId);
        List<RejectType> GetRejectTypes();
    }

    public class RejectTypeBL : IRejectTypeBL
    {
        UnitOfWork _unitOfWork;
        public RejectTypeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(RejectType model)
        {
            try
            {
                if (_unitOfWork.RejectTypeRepository
                    .Get(x => x.RejectTypeName.Contains(model.RejectTypeName)).Count > 0)
                    throw new ArgumentException("This reject type name is in the system already.");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;
                _unitOfWork.RejectTypeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid rejectTypeId)
        {
            try
            {
                var deleteModel = GetRejectType(rejectTypeId);
                if (deleteModel == null)
                    throw new ArgumentException("Find not found.");

                _unitOfWork.RejectTypeRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(RejectType model)
        {
            try
            {
                var editModel = GetRejectType(model.RejectTypeID);
                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                model.ModifiedDate = DateTime.Now;

                _unitOfWork.RejectTypeRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public RejectType GetRejectType(Guid rejectTypeId)
        {
            return _unitOfWork.RejectTypeRepository.GetSingle(x => x.RejectTypeID == rejectTypeId);
        }
        public List<RejectType> GetRejectTypes()
        {
            return _unitOfWork.RejectTypeRepository.Get();
        }
    }
}
