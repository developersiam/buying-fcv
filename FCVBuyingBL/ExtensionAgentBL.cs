﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IExtensionAgentBL
    {
        void Add(ExtensionAgent model);
        void Delete(string extensionAgentCode);
        void Edit(string code, string name, string modifiedUser);
        ExtensionAgent GetSingle(string extensionAgentCode);
        List<ExtensionAgent> GetAll();
        List<ExtensionAgent> GetByArea(string areaCode);
    }

    public class ExtensionAgentBL : IExtensionAgentBL
    {
        UnitOfWork _unitOfWork;
        public ExtensionAgentBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(ExtensionAgent model)
        {
            try
            {
                if (_unitOfWork.ExtensionAgentRepository
                    .Get(x => x.ExtensionAgentCode == model.ExtensionAgentCode).Count > 0)
                    throw new ArgumentException("This extension code is in the system already.");

                model.CreateDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;
                model.ModifiedDate = DateTime.Now;

                _unitOfWork.ExtensionAgentRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string extensionAgentCode)
        {
            try
            {
                var deleteModel = _unitOfWork.ExtensionAgentRepository
                    .GetSingle(x => x.ExtensionAgentCode == extensionAgentCode);

                if (deleteModel == null)
                    throw new ArgumentException("Find not found.");

                _unitOfWork.ExtensionAgentRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(string code, string name, string modifiedUser)
        {
            try
            {
                var editModel = _unitOfWork.ExtensionAgentRepository
                    .GetSingle(x => x.ExtensionAgentCode == code);

                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                editModel.ExtensionAgentName = name;
                editModel.ModifiedDate = DateTime.Now;
                editModel.ModifiedUser = modifiedUser;

                _unitOfWork.ExtensionAgentRepository.Update(editModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ExtensionAgent GetSingle(string extensionAgentCode)
        {
            return _unitOfWork.ExtensionAgentRepository
                .GetSingle(x => x.ExtensionAgentCode == extensionAgentCode);
        }
        public List<ExtensionAgent> GetAll()
        {
            return _unitOfWork.ExtensionAgentRepository.Get(null, null, x => x.Area);
        }
        public List<ExtensionAgent> GetByArea(string areaCode)
        {
            return _unitOfWork.ExtensionAgentRepository
                .Get(x => x.AreaCode == areaCode
                , null, x => x.Area);
        }
    }
}
