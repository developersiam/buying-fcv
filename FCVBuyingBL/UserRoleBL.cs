﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IUserRoleBL
    {
        void Add(UserRole model);
        void Delete(UserRole model);
        UserRole GetById(string username, Guid roleId);
        List<UserRole> GetByRole(string roleName);
        List<UserRole> GetByUser(string username);
    }
    public class UserRoleBL : IUserRoleBL
    {
        UnitOfWork _unitOfWork;

        public UserRoleBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(UserRole model)
        {
            try
            {
                if (GetById(model.Username, model.RoleID) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                _unitOfWork.UserRoleRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(UserRole model)
        {
            try
            {
                var deleteModel = GetById(model.Username, model.RoleID);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.UserRoleRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserRole GetById(string username, Guid roleId)
        {
            return _unitOfWork.UserRoleRepository
                .GetSingle(x => x.Username == username && x.RoleID == roleId);
        }

        public List<UserRole> GetByRole(string roleName)
        {
            return _unitOfWork.UserRoleRepository
                .Get(x => x.Role.RoleName == roleName,
                null,
                x => x.User,
                x => x.Role);
        }

        public List<UserRole> GetByUser(string username)
        {
            return _unitOfWork.UserRoleRepository
                .Get(x => x.Username == username,
                null,
                x => x.User,
                x => x.Role);
        }
    }
}
