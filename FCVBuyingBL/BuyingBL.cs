﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCVBuyingEntities;
using FCVBuyingDAL;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace FCVBuyingBL
{
    public interface IBuyingBL
    {
        void CaptureBuyingWeight(string baleBarcode, int baleNumber, short crop,
            string farmerCode, string stationCode, short documentNumber, int baleTypeID,
            decimal weight, string weightUser);
        void ChangeBuyingWeightInfo(string baleBarcode, int baleNumber, int baleTypeID,
            decimal weight, string weightUser);
        void DeleteBaleBarcode(string baleBarcode);
        void CaptureBuyingGrade(string baleBarcode, string gradeGroupCode,
            string grade, string gradeUser);
        void Add(string baleBarcode, int baleNumber, BuyingDocument document,
            int baleTypeID, decimal weight, string gradeGroupCode, string grade,
            string transportationCode, string username);
        void Edit(string baleBarcode, int baleNumber, BuyingDocument document,
            int baleTypeID, decimal weight, string gradeGroupCode, string grade,
            string transportationCode, string username);
        void RemoveBuyingGrade(string baleBarcode, string removeGradeUser);
        void RejectBale(string baleBarcode, Guid rejectTypeID, string rejectUser);
        void CencelRejectBale(string baleBarcode, string cancelRejectUser);
        void AddOrEditMoistureResult(string baleBarcode, decimal result, DateTime resultDate, string resultUser);
        void RemoveMoistureResult(string baleBarcode, string removeMoistureResultUser);
        Buying GetSingle(string baleBarcode);
        List<Buying> GetByDocument(short crop, string stationCode, string farmerCode, int documnetNumber);
        List<Buying> GetByDocumentDate(DateTime documentDate);
        List<Buying> GetByFarmer(short crop, string farmerCode);
        List<Buying> GetByExtensionAgent(short crop, string extensionAgentCode);
        List<Buying> GetByStation(short crop, string stationCode);
        List<Buying> GetMoistureResultByCrop(short crop);
        List<Buying> GetMoistureResultByResultDate(DateTime resultDate);
        List<Buying> GetMoistureResultByDateRange(DateTime from, DateTime to);
        List<Buying> GetByTransportationDocument(string transportationCode);
        List<Buying> GetByNPIStation(string stationCode, DateTime buyingDate);
        bool IsDupplicatedBaleNumber(short crop, string farmerCode, int baleNumber);
    }

    public class BuyingBL : IBuyingBL
    {
        UnitOfWork uow;
        public BuyingBL()
        {
            uow = new UnitOfWork();
        }

        public void AddOrEditMoistureResult(string baleBarcode, decimal result, DateTime resultDate, string resultUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                _editModel.MoistureResult = result;
                _editModel.MoistureResultDate = resultDate;
                _editModel.MoistureResultUser = resultUser;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CaptureBuyingGrade(string baleBarcode, string gradeGroupCode, string grade, string gradeUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_editModel.Crop, _editModel.FarmerCode, _editModel.StationCode, _editModel.DocumentNumber);

                if (document == null)
                    throw new Exception("ไม่พบใบซื้อนี้ในระบบ");

                if (document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขายแล้ว");

                _editModel.RejectType = null;
                _editModel.RejectTypeID = null;

                _editModel.GradeGroupCode = gradeGroupCode;
                _editModel.Grade = grade;
                _editModel.GradeDate = DateTime.Now;
                _editModel.GradeUser = gradeUser;
                _editModel.GradeModifiedDate = DateTime.Now;
                _editModel.GradeModifiedUser = gradeUser;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CaptureBuyingWeight(string baleBarcode, int baleNumber, short crop,
            string farmerCode, string stationCode, short documentNumber, int baleTypeID,
            decimal weight, string weightUser)
        {
            try
            {
                var model = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);
                if (model != null)
                    throw new Exception("บาร์โค้ตรหัส " + baleBarcode +
                        " ถูกใช้ไปแล้วใน Document Code : " + model.Crop +
                        "-" + model.StationCode +
                        "-" + model.FarmerCode +
                        "-" + model.DocumentNumber);

                if (baleBarcode.Length != 17)
                    throw new Exception("Bale barcode จะต้องมีจำนวน 17 หลัก");

                if (baleBarcode.Substring(0, 2) != "06")
                    throw new Exception("Bale barcode จะต้องขึ้นต้นด้วย 06 เท่านั้น");

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(baleBarcode))
                    throw new Exception("ระบบอนุญาตให้รหัสบาร์โค้ตประกอบด้วยตัวเลข, เครื่องหมายขีดกลาง (-) และตัวอักษรภาษาอังกฤษ เท่านั้น");

                if (baleNumber <= 0)
                    throw new Exception("Bale number จะต้องไม่น้อยกว่า 1");

                if (!Helper.RegularExpressionHelper.IsNumericCharacter(baleNumber.ToString()))
                    throw new Exception("หมายเลขห่อยาต้องเป็นตัวเลขเท่านั้น");

                if (baleNumber.ToString().Length > 4)
                    throw new Exception("หมายเลขห่อยา ไม่ควรเกิน 4 หลัก");

                if (!Helper.RegularExpressionHelper.IsDecimalCharacter(weight.ToString()))
                    throw new Exception("ระบบอนุญาตให้ช่องน้ำหนักประกอบด้วยตัวเลขและจุดทศนิยม เท่านั้น");

                if (weight < 1)
                    throw new Exception("น้ำหนักใบยาจะต้องมีค่าเริ่มต้นที่ 1 กก. ขึ้นไป");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(crop, farmerCode, stationCode, documentNumber);
                if (document == null)
                    throw new Exception("ไม่พบ buying document นี้ในระบบโปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบข้อมูล");

                if (document.RequestFinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ รอการยืนยัน (request finish ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (document.FinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");

                if (document.Buyings
                    .Where(x => x.BaleNumber == baleNumber)
                    .Count() > 0)
                    throw new Exception("ในใบซื้อเดียวกัน หมายเลขห่อจะต้องไม่ซ้ำกัน");

                uow.BuyingRepository.Add(new Buying
                {
                    BaleBarcode = baleBarcode,
                    Crop = crop,
                    FarmerCode = farmerCode,
                    StationCode = stationCode,
                    DocumentNumber = documentNumber,
                    BaleTypeID = baleTypeID,
                    BaleNumber = baleNumber,
                    Weight = weight,
                    WeightDate = DateTime.Now,
                    WeightUser = weightUser,
                    WeightModifiedDate = DateTime.Now,
                    WeightModifiedUser = weightUser
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CencelRejectBale(string baleBarcode, string cancelRejectUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                if (_editModel.RejectTypeID == null)
                    throw new Exception("ไม่พบข้อมูลการ reject ห่อยานี้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                _editModel.RejectType = null;
                _editModel.RejectTypeID = null;
                _editModel.RejectDate = DateTime.Now;
                _editModel.RejectUser = cancelRejectUser;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeBuyingWeightInfo(string baleBarcode, int baleNumber,
            int baleTypeID, decimal weight, string weightUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_editModel.Crop, _editModel.FarmerCode, _editModel.StationCode, _editModel.DocumentNumber);

                if (document == null)
                    throw new Exception("ไม่พบใบซื้อนี้ในระบบ");

                if (document.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะ ถูกปิดการซื้อขายแล้ว");

                if (baleNumber != _editModel.BaleNumber)
                    if (document.Buyings
                        .Where(x => x.BaleNumber == baleNumber)
                        .Count() > 0)
                        throw new Exception("ในใบซื้อเดียวกัน หมายเลขห่อจะต้องไม่ซ้ำกัน");

                _editModel.BaleType = null;
                _editModel.BaleTypeID = baleTypeID;
                _editModel.BaleNumber = baleNumber;
                _editModel.Weight = weight;
                _editModel.WeightModifiedDate = DateTime.Now;
                _editModel.WeightModifiedUser = weightUser;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteBaleBarcode(string baleBarcode)
        {
            try
            {
                var _deleteModel = GetSingle(baleBarcode);
                if (_deleteModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                /// กรณีมีการบันทึกข้อมูลน้ำหนักแล้วจะไม่อนุญาตให้ลบข้อมูลได้
                var extensionAgentCode = _deleteModel.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode;

                if (_deleteModel.Grade != null && !extensionAgentCode.Contains("NPI"))
                    throw new Exception("ห่อยานี้ได้ถูกบันทึกเกรดแล้ว ไม่อนุญาตให้ลบข้อมูลได้");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_deleteModel.Crop, _deleteModel.FarmerCode, _deleteModel.StationCode, _deleteModel.DocumentNumber);

                if (document == null)
                    throw new Exception("ไม่พบ buying document นี้ในระบบโปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบข้อมูล");

                if (document.FinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");

                uow.BuyingRepository.Remove(_deleteModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Buying GetSingle(string baleBarcode)
        {
            return uow.BuyingRepository
                .GetSingle(x => x.BaleBarcode == baleBarcode,
                x => x.RejectType,
                x => x.BaleType,
                x => x.TransportationDocument,
                x => x.BuyingDocument,
                x => x.BuyingDocument.FarmerQuota,
                x => x.BuyingDocument.FarmerQuota.Farmer);
        }

        public List<Buying> GetByDocument(short crop, string stationCode, string farmerCode, int documnetNumber)
        {
            return uow.BuyingRepository
                .Get(x => x.Crop == crop
                && x.StationCode == stationCode
                && x.FarmerCode == farmerCode
                && x.DocumentNumber == documnetNumber,
               null,
                x => x.RejectType,
                x => x.BaleType,
                x => x.BuyingGrade,
                x => x.BuyingDocument,
                x => x.BuyingDocument.FarmerQuota,
                x => x.BuyingDocument.FarmerQuota.Farmer,
                x => x.TransportationDocument);
        }

        public List<Buying> GetByDocumentDate(DateTime documentDate)
        {
            return uow.BuyingRepository
                .Get(x => x.BuyingDocument.CreateDate != null,
               null,
               x => x.RejectType,
               x => x.BaleType,
               x => x.BuyingGrade,
               x => x.BuyingDocument,
               x => x.BuyingDocument.FarmerQuota,
               x => x.BuyingDocument.FarmerQuota.Farmer)
               .ToList()
               .Where(x => Convert.ToDateTime(x.BuyingDocument.CreateDate).Date == documentDate.Date)
               .ToList();
        }

        public List<Buying> GetByFarmer(short crop, string farmerCode)
        {
            return uow.BuyingRepository
               .Get(x => x.Crop == crop
               && x.FarmerCode == farmerCode,
               null,
                x => x.RejectType,
                x => x.BaleType,
                x => x.BuyingGrade,
                x => x.BuyingDocument);
        }

        public List<Buying> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return uow.BuyingRepository
               .Get(x => x.Crop == crop
               && x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode == extensionAgentCode,
               null,
                x => x.RejectType,
                x => x.BaleType,
                x => x.BuyingGrade,
                x => x.BuyingDocument,
                x => x.BuyingDocument.FarmerQuota,
                x => x.BuyingDocument.FarmerQuota.Farmer);
        }

        public List<Buying> GetByStation(short crop, string stationCode)
        {
            return uow.BuyingRepository
               .Get(x => x.Crop == crop
               && x.StationCode == stationCode);
        }

        public List<Buying> GetMoistureResultByCrop(short crop)
        {
            return uow.BuyingRepository
               .Get(x => x.Crop == crop
               && x.MoistureResult != null,
               null,
               x => x.RejectType,
               x => x.BaleType);
        }

        public List<Buying> GetMoistureResultByResultDate(DateTime resultDate)
        {
            return uow.BuyingRepository
               .Get(x => x.MoistureResultDate != null,
               null,
               x => x.BuyingGrade,
               x => x.RejectType,
               x => x.BaleType,
               x => x.BuyingDocument,
               x => x.BuyingDocument.FarmerQuota,
               x => x.BuyingDocument.FarmerQuota.Farmer)
               .ToList()
               .Where(x => Convert.ToDateTime(x.MoistureResultDate).Date == resultDate.Date)
               .ToList();
        }

        public List<Buying> GetMoistureResultByDateRange(DateTime from, DateTime to)
        {
            return uow.BuyingRepository
                   .Get(x => x.MoistureResultDate != null,
                   null,
                   x => x.BuyingGrade,
                   x => x.RejectType,
                   x => x.BaleType,
                   x => x.BuyingDocument,
                   x => x.BuyingDocument.FarmerQuota,
                   x => x.BuyingDocument.FarmerQuota.Farmer)
                   .ToList()
                   .Where(x => Convert.ToDateTime(x.MoistureResultDate).Date >= from.Date &&
                   Convert.ToDateTime(x.MoistureResultDate).Date <= to.Date)
                   .ToList();
        }

        public void RejectBale(string baleBarcode, Guid rejectTypeID, string rejectUser)
        {
            try
            {
                var model = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูล");

                if (model.TransportationCode != null)
                    throw new Exception("ยาห่อนี้ถูกบันทึกข้อมูลในใบนำส่งแล้ว ไม่สามารถ reject ได้ " + 
                        Environment.NewLine +
                        "ท่านจะต้องลบข้อมูลยาห่อนี้ออกจากรถบรรทุกรหัสใบนำส่ง " + 
                        model.TransportationCode + " ก่อน");

                /// ห่อยาที่ถูกบันทึกเกรดซื้อแล้วจะไม่สามารถบันทึกข้อมูลการ reject ได้
                /// ห่อยาที่ยังไม่ได้บันทึกน้ำหนักจะไม่สามารถบันทึกข้อมูลการ reject ได้
                /// 
                //if (_editModel.Grade != null)
                //    throw new Exception("ห่อยานี้มีการบันทึกเกรดซื้อแล้ว ไม่สามารถทำการ reject ห่อยาได้"
                //        + Environment.NewLine
                //        + " ผู้ใช้จะต้องทำการยกเลิกเกรดซื้อก่อน");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(model.Crop,
                    model.FarmerCode,
                    model.StationCode,
                    model.DocumentNumber);

                if (document == null)
                    throw new Exception("ไม่พบ buying document นี้ในระบบ");

                if (document.FinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะปิดการขายแล้ว");

                model.Grade = null;
                model.GradeGroupCode = null;
                model.RejectTypeID = rejectTypeID;
                model.RejectDate = DateTime.Now;
                model.RejectUser = rejectUser;

                uow.BuyingRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveBuyingGrade(string baleBarcode, string removeGradeUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                var document = BusinessLayerService.BuyingDocumentBL()
                    .GetSingle(_editModel.Crop, _editModel.FarmerCode, _editModel.StationCode, _editModel.DocumentNumber);

                if (document == null)
                    throw new Exception("ไม่พบ buying document นี้ในระบบโปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบข้อมูล");

                if (document.FinishStatus == true)
                    throw new Exception("Buying document นี้อยู่ในสถานะ ถูกยืนยันและปิดการซื้อขายแล้ว (finished) ไม่สามารถลบ/แก้ไขข้อมูลได้");

                _editModel.Grade = null;
                _editModel.GradeGroupCode = null;
                _editModel.GradeModifiedDate = DateTime.Now;
                _editModel.GradeModifiedUser = removeGradeUser;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveMoistureResult(string baleBarcode, string removeMoistureResultUser)
        {
            try
            {
                var _editModel = GetSingle(baleBarcode);
                if (_editModel == null)
                    throw new Exception("ไม่พบข้อมูล");

                _editModel.MoistureResult = null;
                _editModel.MoistureResultUser = null;
                _editModel.MoistureResultDate = null;

                uow.BuyingRepository.Update(_editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Buying> GetByTransportationDocument(string transportationCode)
        {
            var list = uow.BuyingRepository
                .Get(b => b.TransportationCode == transportationCode,
                null,
                b => b.BaleType,
                b => b.RejectType,
                b => b.BuyingGrade,
                b => b.TransportationDocument)
                .ToList();

            return list;
        }

        public List<Buying> GetByNPIStation(string stationCode, DateTime buyingDate)
        {
            var list = uow.BuyingRepository
                .Get(b => b.StationCode == stationCode && b.GradeDate != null,
                null,
                b => b.BaleType,
                b => b.RejectType,
                b => b.BuyingGrade,
                b => b.TransportationDocument)
                .Where(x => Convert.ToDateTime(x.GradeDate).DayOfYear == buyingDate.DayOfYear)
                .ToList();

            return list;
        }

        public bool IsDupplicatedBaleNumber(short crop, string farmerCode, int baleNumber)
        {
            var buying = uow.BuyingRepository
                .GetSingle(x => x.Crop == crop &&
                x.FarmerCode == farmerCode &&
                x.BaleNumber == baleNumber);

            if (buying == null)
                return false;
            else
                return true;
        }

        public void Add(string baleBarcode, int baleNumber, BuyingDocument document,
            int baleTypeID, decimal weight, string gradeGroupCode, string grade,
            string transportationCode, string username)
        {
            try
            {
                if (baleBarcode.Length != 17)
                    throw new Exception("Bale barcode จะต้องมีจำนวน 17 หลัก");

                if (baleBarcode.Substring(0, 2) != "06")
                    throw new Exception("Bale barcode จะต้องขึ้นต้นด้วย 06 เท่านั้น");

                if (!Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(baleBarcode))
                    throw new Exception("ระบบอนุญาตให้รหัสบาร์โค้ตประกอบด้วยตัวเลข, เครื่องหมายขีดกลาง (-) และตัวอักษรภาษาอังกฤษ เท่านั้น");

                if (baleNumber <= 0)
                    throw new Exception("Bale number จะต้องไม่น้อยกว่า 1");

                if (!Helper.RegularExpressionHelper.IsNumericCharacter(baleNumber.ToString()))
                    throw new Exception("หมายเลขห่อยาต้องเป็นตัวเลขเท่านั้น");

                if (baleNumber.ToString().Length > 4)
                    throw new Exception("หมายเลขห่อยา ไม่ควรเกิน 4 หลัก");

                if (!Helper.RegularExpressionHelper.IsDecimalCharacter(weight.ToString()))
                    throw new Exception("ระบบอนุญาตให้ช่องน้ำหนักประกอบด้วยตัวเลขและจุดทศนิยม เท่านั้น");

                if (weight < 1)
                    throw new Exception("น้ำหนักใบยาจะต้องมีค่าเริ่มต้นที่ 1 กก. ขึ้นไป");

                var model = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);
                if (model != null)
                    throw new Exception("บาร์โค้ตรหัส " + baleBarcode +
                        " ถูกใช้ไปแล้วใน Document Code : " + model.Crop +
                        "-" + model.StationCode +
                        "-" + model.FarmerCode +
                        "-" + model.DocumentNumber);

                var doc = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == document.Crop &&
                    x.FarmerCode == document.FarmerCode &&
                    x.StationCode == document.StationCode &&
                    x.DocumentNumber == document.DocumentNumber,
                    x => x.Buyings);
                if (doc == null)
                    throw new Exception("ไม่พบใบซื้อนี้ในระบบ");

                if (doc.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขายแล้ว");

                if (doc.Buyings
                    .Where(x => x.BaleNumber == baleNumber)
                    .Count() > 0)
                    throw new Exception("ในใบซื้อเดียวกัน หมายเลขห่อจะต้องไม่ซ้ำกัน");

                //var gradeGroup = uow.BuyingGradeGroupRepository
                //    .GetSingle(x => x.Crop == doc.Crop &&
                //    x.DefaultStatus == true);
                //if (gradeGroup == null)
                //    throw new Exception("ไม่พบค่า grade group code default ในปี " + doc.Crop);

                //var buyingGrade = uow.BuyingGradeRepository
                //    .GetSingle(x => x.GradeGroupCode == gradeGroup.GradeGroupCode &&
                //    x.Grade == grade);
                //if (buyingGrade == null)
                //    throw new Exception("ไม่พบเกรด " + grade +
                //        " ใน grade group code " + gradeGroup.GradeGroupCode);

                if (!string.IsNullOrEmpty(transportationCode))
                {
                    var transportation = uow.TransportationDocumentRepository
                        .GetSingle(x => x.TransportationCode == transportationCode);
                    if (transportation == null)
                        throw new Exception("ไม่พบ transportation code " + transportationCode + " ในระบบ");

                    if (transportation.IsFinish == true)
                        throw new Exception("transportation code นี้ถูก Finish" +
                            " เพื่อทำการพิมพ์เอกสารออกจากระบบแล้ว " +
                            " โปรดเปลี่ยนไปใช้ transportation code อื่นแทน");
                }

                DateTime? transportDate = null;
                if (!string.IsNullOrEmpty(transportationCode))
                    transportDate = DateTime.Now;

                string transportUser = null;
                if (!string.IsNullOrEmpty(transportationCode))
                    transportUser = username;

                uow.BuyingRepository
                    .Add(new Buying
                    {
                        BaleBarcode = baleBarcode,
                        Crop = doc.Crop,
                        FarmerCode = doc.FarmerCode,
                        StationCode = doc.StationCode,
                        DocumentNumber = doc.DocumentNumber,
                        BaleTypeID = baleTypeID,
                        BaleNumber = baleNumber,
                        GradeGroupCode = gradeGroupCode,
                        Grade = grade,
                        Weight = weight,
                        WeightDate = DateTime.Now,
                        WeightUser = username,
                        WeightModifiedDate = DateTime.Now,
                        WeightModifiedUser = username,
                        TransportationCode = transportationCode,
                        LoadBaleToTruckDate = transportDate,
                        LoadBaleToTruckUser = transportUser
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string baleBarcode, int baleNumber, BuyingDocument document,
            int baleTypeID, decimal weight, string gradeGroupCode, string grade,
            string transportationCode, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(baleBarcode))
                    throw new Exception("Bale barcode cannot be empty.");

                var buying = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);
                if (buying == null)
                    throw new Exception("ไม่พบข้อมูลห่อยานี้ในระบบ");

                if (baleNumber <= 0)
                    throw new Exception("Bale number จะต้องไม่น้อยกว่า 1");

                if (!Helper.RegularExpressionHelper.IsNumericCharacter(baleNumber.ToString()))
                    throw new Exception("หมายเลขห่อยาต้องเป็นตัวเลขเท่านั้น");

                if (baleNumber.ToString().Length > 4)
                    throw new Exception("หมายเลขห่อยา ไม่ควรเกิน 4 หลัก");

                if (baleNumber != buying.BaleNumber)
                {
                    //Check a dupplicate bale number in the document.
                    if (uow.BuyingRepository
                        .Get(x => x.Crop == document.Crop &&
                        x.FarmerCode == document.FarmerCode &&
                        x.StationCode == document.StationCode &&
                        x.DocumentNumber == document.DocumentNumber &&
                        x.BaleNumber == baleNumber)
                        .Count() > 0)
                        throw new Exception("ในใบซื้อเดียวกัน หมายเลขห่อจะต้องไม่ซ้ำกัน");
                }

                if (!Helper.RegularExpressionHelper.IsDecimalCharacter(weight.ToString()))
                    throw new Exception("ระบบอนุญาตให้ช่องน้ำหนักประกอบด้วยตัวเลขและจุดทศนิยม เท่านั้น");

                if (weight < 1)
                    throw new Exception("น้ำหนักใบยาจะต้องมีค่าเริ่มต้นที่ 1 กก. ขึ้นไป");

                var doc = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == document.Crop &&
                    x.FarmerCode == document.FarmerCode &&
                    x.StationCode == document.StationCode &&
                    x.DocumentNumber == document.DocumentNumber);
                if (doc == null)
                    throw new Exception("ไม่พบใบซื้อนี้ในระบบ");

                if (doc.FinishStatus == true)
                    throw new Exception("ใบซื้อนี้อยู่ในสถานะปิดการขายแล้ว");

                if (buying.Crop != document.Crop ||
                    buying.FarmerCode != document.FarmerCode ||
                    buying.StationCode != document.StationCode ||
                    buying.DocumentNumber != document.DocumentNumber)
                    throw new Exception("ห่อยานี้อยู่ต่างใบซื้อ ไม่สามารถแก้ไขข้อมูลได้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                if (!string.IsNullOrEmpty(transportationCode))
                {
                    var transportation = uow.TransportationDocumentRepository
                        .GetSingle(x => x.TransportationCode == transportationCode);
                    if (transportation == null)
                        throw new Exception("ไม่พบ transportation code " + transportationCode + " ในระบบ");

                    if (transportation.IsFinish == true)
                        throw new Exception("transportation code นี้อยู่ในสถานะปิดการขาย" +
                            "เพื่อทำการพิมพ์เอกสารออกจากระบบแล้ว " +
                            "โปรดเปลี่ยนไปใช้ transportation code อื่นแทน");
                }

                DateTime? loadTruckDate;
                if (transportationCode == null)
                    loadTruckDate = null;
                else
                    loadTruckDate = DateTime.Now;

                buying.BaleTypeID = baleTypeID;
                buying.BaleNumber = baleNumber;
                buying.GradeGroupCode = gradeGroupCode;
                buying.Grade = grade;
                buying.GradeDate = grade != buying.Grade ? DateTime.Now : buying.GradeDate;
                buying.GradeModifiedDate = grade != buying.Grade ? DateTime.Now : buying.GradeDate;
                buying.GradeModifiedUser = grade != buying.Grade ? username : null;
                buying.Weight = weight;
                buying.WeightModifiedDate = DateTime.Now;
                buying.WeightModifiedUser = username;
                buying.TransportationCode = transportationCode;
                buying.LoadBaleToTruckDate = loadTruckDate;
                buying.LoadBaleToTruckUser = transportationCode == null ? null : username;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
