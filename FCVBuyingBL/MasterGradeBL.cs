﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IMasterGradeBL
    {
        void Add(MasterGrade model);
        void Delete(string grade);
        void Edit(MasterGrade model);
        MasterGrade GetMasterGrade(string grade);
        List<MasterGrade> GetMasterGrades();
    }

    public class MasterGradeBL : IMasterGradeBL
    {
        UnitOfWork _unitOfWork;
        public MasterGradeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(MasterGrade model)
        {
            try
            {
                if (_unitOfWork.MasterGradeRepository
                    .Get(x => x.Grade == model.Grade)
                    .Count() > 0)
                    throw new ArgumentException("This grade is in the system already.");

                _unitOfWork.MasterGradeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string grade)
        {
            try
            {
                var deleteModel = _unitOfWork.MasterGradeRepository.GetSingle(x => x.Grade == grade);
                if (deleteModel == null)
                    throw new ArgumentException("Find not found.");

                var list = _unitOfWork.BuyingGradeRepository.Get(x => x.Grade == grade);

                if (list.Count() > 0)
                    throw new ArgumentException("เกรดนี้มีการนำไปใช้บันทึกข้อมูลแล้ว ไม่สามารถลบได้");

                _unitOfWork.MasterGradeRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(MasterGrade model)
        {
            try
            {
                var editModel = _unitOfWork.MasterGradeRepository.GetSingle(x => x.Grade == model.Grade);
                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                if (_unitOfWork.MasterGradeRepository
                    .Get(x => x.Grade.Contains(model.Grade)).Count > 0)
                    throw new ArgumentException("This grade is in the system already.");

                _unitOfWork.MasterGradeRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public MasterGrade GetMasterGrade(string grade)
        {
            return _unitOfWork.MasterGradeRepository.GetSingle(x => x.Grade == grade);
        }
        public List<MasterGrade> GetMasterGrades()
        {
            return _unitOfWork.MasterGradeRepository.Get();
        }
    }
}
