﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IBuyingGradeBL
    {
        void Add(BuyingGrade model);
        void Delete(string gradeGroupCode, string grade);
        void Edit(BuyingGrade model);
        BuyingGrade GetSingle(string gradeGroupCode, string grade);
        List<BuyingGrade> GetByGradeGroup(string gradeGroupCode);
    }

    public class BuyingGradeBL : IBuyingGradeBL
    {
        UnitOfWork _unitOfWork;
        public BuyingGradeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(BuyingGrade model)
        {
            try
            {
                if (_unitOfWork.BuyingGradeRepository
                    .Get(x => x.GradeGroupCode == model.GradeGroupCode
                    && x.Grade == model.Grade).Count > 0)
                    throw new ArgumentException("เกรดนี้มีอยู่แล้วในชุดราคา " + model.GradeGroupCode);

                _unitOfWork.BuyingGradeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string gradeGroupCode, string grade)
        {
            try
            {
                var _editModel = GetSingle(gradeGroupCode, grade);
                if (_editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                _unitOfWork.BuyingGradeRepository.Remove(_editModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(BuyingGrade model)
        {
            try
            {
                var _editModel = GetSingle(model.GradeGroupCode, model.Grade);
                if (_editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                model.ModifiedDate = DateTime.Now;

                _unitOfWork.BuyingGradeRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BuyingGrade GetSingle(string gradeGroupCode, string grade)
        {
            return _unitOfWork.BuyingGradeRepository
                .GetSingle(x => x.GradeGroupCode == gradeGroupCode
                && x.Grade == grade);
        }
        public List<BuyingGrade> GetByGradeGroup(string gradeGroupCode)
        {
            return _unitOfWork.BuyingGradeRepository
                .Get(x => x.GradeGroupCode == gradeGroupCode);
        }
    }
}
