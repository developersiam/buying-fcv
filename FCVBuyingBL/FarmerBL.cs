﻿using FCVBuyingEntities;
using FCVBuyingDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IFarmerBL
    {
        Farmer Add(Person person, string extensionAgentCode, string createUser);
        void ChangeFarmerCitizenID(string farmerCode, string citizenID, string modifiedUser);
        void Delete(string farmerCode);
        Farmer GetSingle(string farmerCode);
        List<Farmer> GetByExtensionAgent(string extensionAgentCode);
        List<Farmer> GetByCitizenID(string citizenID);
    }

    public class FarmerBL : IFarmerBL
    {
        UnitOfWork _unitOfWork;
        public FarmerBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public Farmer Add(Person person, string extensionAgentCode, string createUser)
        {
            try
            {
                var model = _unitOfWork.PersonRepository
                    .Get(x => x.CitizenID == person.CitizenID);

                ///update person if an infomation changed.
                if (model.Count > 0)
                {
                    var editModel = BusinessLayerService.PersonBL()
                        .GetPerson(person.CitizenID);

                    editModel.Prefix = person.Prefix;
                    editModel.FirstName = person.FirstName;
                    editModel.LastName = person.LastName;
                    editModel.TelephoneNumber = person.TelephoneNumber;
                    editModel.BirthDate = person.BirthDate;
                    editModel.HouseNumber = person.HouseNumber;
                    editModel.Village = person.Village;
                    editModel.Tumbon = person.Tumbon;
                    editModel.Amphur = person.Amphur;
                    editModel.Province = person.Province;
                    editModel.ModifiedDate = DateTime.Now;
                    editModel.ModifiedUser = createUser;

                    //_unitOfWork.PersonRepository.Update(editModel);
                    //_unitOfWork.Save();
                }
                else
                {
                    person.CreateDate = DateTime.Now;
                    person.ModifiedDate = DateTime.Now;
                    person.CreateUser = createUser;
                    person.ModifiedUser = createUser;
                    _unitOfWork.PersonRepository.Add(person);
                    _unitOfWork.Save();
                }

                var memberList = _unitOfWork.FarmerRepository
                    .Get(x => x.CitizenID == person.CitizenID
                    && x.ExtensionAgentCode == extensionAgentCode);

                if (memberList.Count >= 1)
                    throw new ArgumentException("ชาวไร่หมายเลขประจำตัวประชาชน "
                        + person.CitizenID
                        + " ได้ลงทะเบียนเป็นสมาชิกกับตัวแทน " + extensionAgentCode
                        + " แล้ว ไม่สามารถบันทึกข้อมูลได้");

                var extensionAgent = BusinessLayerService.ExtensionAgentBL()
                    .GetSingle(extensionAgentCode);
                if (extensionAgent == null)
                    throw new ArgumentException("ไม่พบข้อมูล extension agent นี้ในระบบ");

                var farmerList = BusinessLayerService.FarmerBL()
                    .GetByExtensionAgent(extensionAgentCode)
                    .Select(x => new
                    {
                        RunningNumber = Convert.ToInt32(x.FarmerCode
                        .Substring(2, x.FarmerCode.Length - 2))
                    })
                    .ToList();

                int max = 1;
                if(farmerList.Count() > 0)
                    max = farmerList.Max(x => x.RunningNumber) + 1;
                string farmerCode = extensionAgent.Prefix + max.ToString().PadLeft(5, '0');

                Farmer newFarmer = new Farmer
                {
                    FarmerCode = farmerCode,
                    CitizenID = person.CitizenID,
                    ExtensionAgentCode = extensionAgentCode,
                    CreateDate = DateTime.Now,
                    CreateUser = createUser,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = createUser
                };

                _unitOfWork.FarmerRepository.Add(newFarmer);
                _unitOfWork.Save();

                return newFarmer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ChangeFarmerCitizenID(string farmerCode, string citizenID, string modifiedUser)
        {
            try
            {
                if (_unitOfWork.PersonRepository.Get(x => x.CitizenID == citizenID).Count < 1)
                    throw new ArgumentException("ไม่พบข้อมูลประวัติตามบัตรประชาชนของชาวไร่รายนี้ในระบบ");

                var farmerModel = _unitOfWork.FarmerRepository
                    .GetSingle(x => x.FarmerCode == farmerCode,
                    x => x.ExtensionAgent);

                if (farmerModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + farmerCode + " นี้ในระบบ");

                farmerModel.CitizenID = citizenID;
                farmerModel.ModifiedDate = DateTime.Now;
                farmerModel.ModifiedUser = modifiedUser;

                _unitOfWork.FarmerRepository.Update(farmerModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string farmerCode)
        {
            try
            {
                var farmerModel = _unitOfWork.FarmerRepository
                    .GetSingle(x => x.FarmerCode == farmerCode);

                if (farmerModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + farmerCode + " นี้ในระบบ");

                if (_unitOfWork.FarmerQuotaRepository
                    .Get(x => x.FarmerCode == farmerModel.FarmerCode).Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้มีข้อมูลโควต้าในระบบ ไม่สามารถลบชาวไร่รายนี้ออกจากระบบได้");

                _unitOfWork.FarmerRepository.Remove(farmerModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Farmer> GetByExtensionAgent(string extensionAgentCode)
        {
            return _unitOfWork.FarmerRepository
                .Get(x => x.ExtensionAgentCode == extensionAgentCode, null,
                x => x.FarmerQuotas,
                x => x.Person);
        }

        public Farmer GetSingle(string farmerCode)
        {
            return _unitOfWork.FarmerRepository
                .GetSingle(x => x.FarmerCode == farmerCode,
                x => x.Person);
        }
        public List<Farmer> GetByCitizenID(string citizenID)
        {
            return _unitOfWork.FarmerRepository.Get(x => x.CitizenID == citizenID, null, x => x.Person);
        }
    }
}
