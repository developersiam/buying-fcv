﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IReplaceTypeBL
    {
        void Add(ReplaceType model);
        void Delete(Guid replaceTypeId);
        void Edit(ReplaceType model);
        ReplaceType GetReplaceType(Guid replaceTypeId);
        List<ReplaceType> GetReplaceTypes();
    }

    public class ReplaceTypeBL : IReplaceTypeBL
    {
        UnitOfWork _unitOfWork;
        public ReplaceTypeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(ReplaceType model)
        {
            try
            {
                if (_unitOfWork.ReplaceTypeRepository
                    .Get(x => x.ReplaceTypeName.Contains(model.ReplaceTypeName)).Count > 0)
                    throw new ArgumentException("This replace type name is in the system already.");

                _unitOfWork.ReplaceTypeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid replaceTypeId)
        {
            try
            {
                var deleteModel = GetReplaceType(replaceTypeId);
                if (deleteModel == null)
                    throw new ArgumentException("Find not found.");

                _unitOfWork.ReplaceTypeRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(ReplaceType model)
        {
            try
            {
                var editModel = GetReplaceType(model.ReplaceTypeID);
                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                if (_unitOfWork.ReplaceTypeRepository
                    .Get(x => x.ReplaceTypeName.Contains(model.ReplaceTypeName)).Count > 0)
                    throw new ArgumentException("This replace type name is in the system already.");

                _unitOfWork.ReplaceTypeRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReplaceType GetReplaceType(Guid replaceTypeId)
        {
            return _unitOfWork.ReplaceTypeRepository.GetSingle(x => x.ReplaceTypeID == replaceTypeId);
        }
        public List<ReplaceType> GetReplaceTypes()
        {
            return _unitOfWork.ReplaceTypeRepository.Get();
        }
    }
}
