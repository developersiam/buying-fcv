﻿using FCVBuyingEntities;
using FCVBuyingDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface INTRMTypeBL
    {
        void Add(NTRMType model);
        void Delete(NTRMType model);
        void Edit(NTRMType model);
        NTRMType GetNtrmType(string ntrmTypeCode);
        List<NTRMType> GetNtrmTypes();
    }

    public class NTRMTypeBL : INTRMTypeBL
    {
        UnitOfWork _unitOfWork;
        public NTRMTypeBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(NTRMType model)
        {
            try
            {
                var list = _unitOfWork.NTRMTypeRepository.Get();
                if (list.Where(x => x.NTRMTypeCode == model.NTRMTypeCode).Count() > 0)
                    throw new ArgumentException("This NTRM type code is in the system already.");

                if (list.Where(x => x.NTRMTypeName == model.NTRMTypeName).Count() > 0)
                    throw new ArgumentException("This NTRM type name is in the system already.");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;

                _unitOfWork.NTRMTypeRepository.Add(model);
                _unitOfWork.Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(NTRMType model)
        {
            try
            {
                _unitOfWork.NTRMTypeRepository.Remove(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(NTRMType model)
        {
            try
            {
                var editModel = GetNtrmType(model.NTRMTypeCode);
                if (editModel == null)
                    throw new ArgumentException("Find not found.");

                model.ModifiedDate = DateTime.Now;
                _unitOfWork.NTRMTypeRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NTRMType GetNtrmType(string ntrmTypeCode)
        {
            return _unitOfWork.NTRMTypeRepository.GetSingle(x => x.NTRMTypeCode == ntrmTypeCode);
        }
        public List<NTRMType> GetNtrmTypes()
        {
            return _unitOfWork.NTRMTypeRepository.Get();
        }
    }
}
