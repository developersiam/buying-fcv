﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IUserBL
    {
        void Add(User model);
        void ChangePassword(string username, string oldPassword, string newPassword, string editUser);
        void Delete(User model);
        User GetById(string username);
        List<User> GetAll();
    }

    public class UserBL : IUserBL
    {
        UnitOfWork _unitOfWork;

        public UserBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(User model)
        {
            try
            {
                if (GetById(model.Username) != null)
                    throw new ArgumentException("มีชื่อ username นี้ซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = model.CreateUser;

                _unitOfWork.UserRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangePassword(string username, string oldPassword, string newPassword, string editUser)
        {
            try
            {
                var model = GetById(username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลของ username นี้ในระบบ");

                if (model.Password != oldPassword)
                    throw new ArgumentException("รหัสผ่านเดิมไม่ตรงกับที่บันทึกไว้ในระบบ");

                model.Password = newPassword;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedUser = editUser;

                _unitOfWork.UserRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(User model)
        {
            try
            {
                var deleteModel = GetById(model.Username);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลของ username นี้ในระบบ");

                _unitOfWork.UserRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<User> GetAll()
        {
            return _unitOfWork.UserRepository.Get();
        }

        public User GetById(string username)
        {
            return _unitOfWork.UserRepository
                .GetSingle(x => x.Username == username,
                x => x.UserRoles,
                x => x.UserUnderExtensionAgents,
                x => x.UserAreas);
        }
    }
}
