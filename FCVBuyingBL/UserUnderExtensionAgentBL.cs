﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IUserUnderExtensionAgentBL
    {
        void Add(UserUnderExtensionAgent model);
        void Delete(UserUnderExtensionAgent model);
        UserUnderExtensionAgent GetById(string username, string extensionAgentCode);
        List<UserUnderExtensionAgent> GetByExtensionAgent(string extensionAgentCode);
        List<UserUnderExtensionAgent> GetByUser(string username);
    }

    public class UserUnderExtensionAgentBL : IUserUnderExtensionAgentBL
    {
        UnitOfWork _unitOfWork;

        public UserUnderExtensionAgentBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(UserUnderExtensionAgent model)
        {
            try
            {
                if (GetById(model.Username, model.ExtensionAgentCode) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                _unitOfWork.UserUnderExtensionAgentRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(UserUnderExtensionAgent model)
        {
            try
            {
                var deleteModel = GetById(model.Username, model.ExtensionAgentCode);
                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.UserUnderExtensionAgentRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserUnderExtensionAgent GetById(string username, string extensionAgentCode)
        {
            return _unitOfWork.UserUnderExtensionAgentRepository
                .GetSingle(x => x.Username == username && x.ExtensionAgentCode == extensionAgentCode);
        }

        public List<UserUnderExtensionAgent> GetByExtensionAgent(string extensionAgentCode)
        {
            return _unitOfWork.UserUnderExtensionAgentRepository
                .Get(x => x.ExtensionAgentCode == extensionAgentCode,
                null, x => x.User, x => x.ExtensionAgent);
        }

        public List<UserUnderExtensionAgent> GetByUser(string username)
        {
            return _unitOfWork.UserUnderExtensionAgentRepository
                .Get(x => x.Username == username,
                null, x => x.User, x => x.ExtensionAgent);
        }
    }
}
