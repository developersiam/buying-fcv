﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IBuyingGradeGroupBL
    {
        void Add(DateTime assigeDate, bool defaultStatus, string createUser);
        void Delete(string gradeGroupCode);
        void SetDefaultStatus(string gradeGroupCode, string editUser);
        BuyingGradeGroup GetSingle(string gradeGroupCode);
        List<BuyingGradeGroup> GetByCrop(int crop);
    }

    public class BuyingGradeGroupBL : IBuyingGradeGroupBL
    {
        UnitOfWork _unitOfWork;
        public BuyingGradeGroupBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void Add(DateTime assigeDate, bool defaultStatus, string createUser)
        {
            try
            {
                var _defaultCrop = _unitOfWork.CropRepository.GetSingle(x => x.DefaultStatus == true);
                if (_defaultCrop == null)
                    throw new ArgumentException("ในระบบยังไม่มีการตั้งค่า default crop");

                var _groupList = _unitOfWork.BuyingGradeGroupRepository
                    .Get(x => x.Crop == _defaultCrop.Crop1);
                string groupCode = _defaultCrop.Crop1 + "-" + (_groupList.Count + 1);

                _unitOfWork.BuyingGradeGroupRepository
                    .Add(new BuyingGradeGroup
                {
                    GradeGroupCode = groupCode,
                    Crop = _defaultCrop.Crop1,
                    AssignDate = assigeDate,
                    DefaultStatus = defaultStatus,
                    CreateDate = DateTime.Now,
                    CreateUser = createUser,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = createUser
                });
                _unitOfWork.Save();

                if (defaultStatus == true)
                    SetDefaultStatus(groupCode, createUser);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string gradeGroupCode)
        {
            try
            {
                var _deleteModel = _unitOfWork.BuyingGradeGroupRepository
                    .GetSingle(x => x.GradeGroupCode == gradeGroupCode);
                if (_deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var _gradeList = _unitOfWork.BuyingGradeRepository
                    .Get(x => x.GradeGroupCode == gradeGroupCode);
                if (_gradeList.Count() > 0)
                    throw new ArgumentException("มีเกรดที่เชื่อมโยงกับกลุ่มเกรดนี้ ไม่สามารถลบข้อมูลได้");

                _unitOfWork.BuyingGradeGroupRepository.Remove(_deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BuyingGradeGroup GetSingle(string gradeGroupCode)
        {
            return _unitOfWork.BuyingGradeGroupRepository
                .GetSingle(x => x.GradeGroupCode == gradeGroupCode, x => x.BuyingGrades);
        }
        public List<BuyingGradeGroup> GetByCrop(int crop)
        {
            return _unitOfWork.BuyingGradeGroupRepository.Get(x => x.Crop == crop);
        }
        public void SetDefaultStatus(string gradeGroupCode, string editUser)
        {
            try
            {
                foreach(var item in _unitOfWork.BuyingGradeGroupRepository.Get())
                {
                    if(item.GradeGroupCode == gradeGroupCode)
                        item.DefaultStatus = true;
                    else
                        item.DefaultStatus = false;
                    
                    item.ModifiedDate = DateTime.Now;
                    item.ModifiedUser = editUser;

                    _unitOfWork.BuyingGradeGroupRepository.Update(item);
                }

                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
