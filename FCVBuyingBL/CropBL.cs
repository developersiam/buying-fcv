﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCVBuyingEntities;
using FCVBuyingDAL;

namespace FCVBuyingBL
{
    public interface ICropBL
    {
        List<Crop> GetAll();
        Crop GetSingle(int crop);
        Crop GetDefault();
        bool IsCrop(int crop);
        void Add(Crop crop);
        void SetDefault(int crop);
        void Edit(Crop crop);
        void Delete(int crop);
    }

    public class CropBL : ICropBL
    {
        private UnitOfWork _unitOfWork;
        public CropBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(Crop crop)
        {
            try
            {
                _unitOfWork.CropRepository
                    .Add(new Crop
                    {
                        Crop1 = crop.Crop1,
                        DefaultStatus = crop.DefaultStatus,
                        CreateDate = DateTime.Now,
                        CreateUser = crop.CreateUser,
                        ModifiedUser = crop.CreateUser,
                        ModifiedDate = DateTime.Now
                    });
                _unitOfWork.Save();

                if (crop.DefaultStatus == true)
                    SetDefault(crop.Crop1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int crop)
        {
            try
            {
                var _crop = _unitOfWork.CropRepository
                    .GetSingle(c => c.Crop1 == crop);
                if (_crop == null)
                    throw new ArgumentNullException("Find not found.");

                _unitOfWork.CropRepository.Remove(_crop);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Crop crop)
        {
            try
            {
                if (crop.DefaultStatus == true)
                    foreach (var item in _unitOfWork.CropRepository.Get())
                    {
                        item.DefaultStatus = false;
                        _unitOfWork.CropRepository.Update(item);
                        _unitOfWork.Save();
                    }

                var _crop = _unitOfWork.CropRepository
                    .GetSingle(c => c.Crop1 == crop.Crop1);
                if (_crop == null)
                    throw new ArgumentNullException("Find not found.");

                _crop.DefaultStatus = crop.DefaultStatus;
                _crop.ModifiedDate = crop.ModifiedDate;
                _crop.ModifiedUser = crop.ModifiedUser;

                _unitOfWork.CropRepository.Update(_crop);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Crop GetSingle(int crop)
        {
            return _unitOfWork.CropRepository
                .GetSingle(c => c.Crop1 == crop);
        }

        public List<Crop> GetAll()
        {
            return _unitOfWork.CropRepository.Get();
        }

        public Crop GetDefault()
        {
            return _unitOfWork.CropRepository
                .Query(c => c.DefaultStatus == true)
                .FirstOrDefault();
        }

        public bool IsCrop(int crop)
        {
            if (_unitOfWork.CropRepository
                .GetSingle(c => c.Crop1 == crop) == null)
                return false;
            else
                return true;
        }

        public void SetDefault(int crop)
        {
            try
            {
                foreach (var item in this.GetAll())
                {
                    if (item.Crop1 == crop)
                        item.DefaultStatus = true;
                    else
                        item.DefaultStatus = false;

                    _unitOfWork.CropRepository.Update(item);
                }
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
