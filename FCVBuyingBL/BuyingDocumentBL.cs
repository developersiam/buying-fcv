﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IBuyingDocumentBL
    {
        BuyingDocument Add(short crop, string farmerCode, string stationCode,
            DateTime createDate, string createUser);
        void Delete(BuyingDocument model);
        void SendRequest(BuyingDocument model);
        void CancelRequest(BuyingDocument model);
        void Finish(BuyingDocument model);
        void UnFinish(BuyingDocument model);
        BuyingDocument GetSingle(short crop, string farmerCode, string stationCode, int documentNumber);
        List<BuyingDocument> GetByFarmer(short crop, string farmerCode);
        List<BuyingDocument> GetByCrop(short crop);
        List<BuyingDocument> GetByFarmerAndStaion(short crop, string stationCode, string farmerCode);
        List<BuyingDocument> GetByStaion(DateTime createDate, string stationCode);
        List<BuyingDocument> GetByStatus(short crop, string stationCode, bool status);
    }

    public class BuyingDocumentBL : IBuyingDocumentBL
    {
        UnitOfWork _unitOfWork;
        public BuyingDocumentBL()
        {
            _unitOfWork = new UnitOfWork();
        }
        public BuyingDocument Add(short crop, string farmerCode, string stationCode,
            DateTime createDate, string createUser)
        {
            try
            {
                var farmerQuota = _unitOfWork.FarmerQuotaRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode);
                if (farmerQuota.Count() <= 0)
                    throw new Exception("ยังไม่ได้บันทึกข้อมูลโควต้าในปีปัจจุบันให้กับชาวไร่หรือผู้บ่มรายนี้");

                int max = 0;
                var list = GetByFarmerAndStaion(crop, stationCode, farmerCode);
                if (list.Count > 0)
                    max = list.Max(x => x.DocumentNumber);

                BuyingDocument model = new BuyingDocument
                {
                    Crop = crop,
                    FarmerCode = farmerCode.ToUpper(),
                    StationCode = stationCode,
                    DocumentNumber = Convert.ToInt16(max + 1),
                    CreateDate = createDate,
                    CreateUser = createUser,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = createUser,
                    FinishStatus = false,
                    RequestFinishStatus = false
                };

                _unitOfWork.BuyingDocumentRepository.Add(model);
                _unitOfWork.Save();

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(BuyingDocument model)
        {
            try
            {
                var _deleteModel = GetSingle(model.Crop, model.FarmerCode,
                    model.StationCode, model.DocumentNumber);

                if (_deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (_deleteModel.FinishStatus == true)
                    throw new ArgumentException("ใบเวาเชอร์นี้ถูก finish ไม่สามารถลบได้");

                if (_deleteModel.Buyings.Count > 0)
                    throw new ArgumentException("ใบเวาเชอร์นี้มีรายการห่อยาที่ซื้อขายเชื่อมโยงอยู่ ไม่สามารถลบได้");

                _unitOfWork.BuyingDocumentRepository.Remove(_deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SendRequest(BuyingDocument model)
        {
            try
            {
                var bd = GetSingle(model.Crop, model.FarmerCode, model.StationCode, model.DocumentNumber);

                if (bd == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                bd.ModifiedDate = DateTime.Now;
                bd.RequestFinishStatus = true;

                _unitOfWork.BuyingDocumentRepository.Update(bd);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CancelRequest(BuyingDocument model)
        {
            try
            {
                var bd = GetSingle(model.Crop, model.FarmerCode, model.StationCode, model.DocumentNumber);
                if (bd == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                if (bd.FinishStatus == true)
                    throw new ArgumentException("เอกสารนี้ถูก Manager approve แล้ว ไม่สามารถ cancel request ได้");

                bd.ModifiedDate = DateTime.Now;
                bd.RequestFinishStatus = false;

                _unitOfWork.BuyingDocumentRepository.Update(bd);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Finish(BuyingDocument model)
        {
            try
            {
                var bd = GetSingle(model.Crop, model.FarmerCode, model.StationCode, model.DocumentNumber);

                if (bd == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                bd.ModifiedDate = DateTime.Now;
                bd.FinishTime = DateTime.Now;
                bd.FinishStatus = true;
                bd.RequestFinishStatus = true;

                _unitOfWork.BuyingDocumentRepository.Update(bd);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UnFinish(BuyingDocument model)
        {
            try
            {
                var bd = GetSingle(model.Crop, model.FarmerCode, model.StationCode, model.DocumentNumber);
                if (bd == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                bd.ModifiedDate = DateTime.Now;
                bd.UnfinishTime = DateTime.Now;
                bd.FinishStatus = false;
                bd.RequestFinishStatus = false;

                _unitOfWork.BuyingDocumentRepository.Update(bd);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BuyingDocument GetSingle(short crop, string farmerCode,
            string stationCode, int documentNumber)
        {
            return _unitOfWork.BuyingDocumentRepository
                .GetSingle(x => x.Crop == crop
                && x.FarmerCode == farmerCode
                && x.StationCode == stationCode
                && x.DocumentNumber == documentNumber,
                x => x.Buyings,
                x => x.BuyingStation,
                x => x.FarmerQuota,
                x => x.FarmerQuota.Farmer);
        }
        public List<BuyingDocument> GetByFarmer(short crop, string farmerCode)
        {
            return _unitOfWork.BuyingDocumentRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode,
                null, x => x.Buyings);
        }
        public List<BuyingDocument> GetByCrop(short crop)
        {
            return _unitOfWork.BuyingDocumentRepository
                .Get(x => x.Crop == crop,
                null,
                x => x.Buyings,
                x => x.FarmerQuota,
                x => x.FarmerQuota.Farmer);
        }
        public List<BuyingDocument> GetByFarmerAndStaion(short crop, string stationCode, string farmerCode)
        {
            return _unitOfWork.BuyingDocumentRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode &&
                x.StationCode == stationCode,
                null,
                x => x.Buyings);
        }
        public List<BuyingDocument> GetByStaion(DateTime createDate, string stationCode)
        {
            return _unitOfWork.BuyingDocumentRepository
                .Get(x => x.CreateDate.Day == createDate.Day &&
                x.CreateDate.Month == createDate.Month &&
                x.CreateDate.Year == createDate.Year &&
                x.StationCode == stationCode,
                null,
                x => x.FarmerQuota,
                x => x.FarmerQuota.Farmer,
                x => x.FarmerQuota.Farmer.Person,
                x => x.Buyings,
                x => x.BuyingStation);
        }
        public List<BuyingDocument> GetByStatus(short crop, string stationCode, bool status)
        {
            return _unitOfWork.BuyingDocumentRepository
                .Get(x => x.Crop == crop
                && x.StationCode == stationCode
                && x.FinishStatus == status);
        }
    }
}
