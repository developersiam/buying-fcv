﻿using FCVBuyingDAL;
using FCVBuyingEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingBL
{
    public interface IAreaBL
    {
        void Add(Area model);
        void Edit(Area model);
        void Delete(Area model);
        List<Area> GetAll();
    }

    public class AreaBL : IAreaBL
    {
        UnitOfWork _unitOfWork;
        public AreaBL()
        {
            _unitOfWork = new UnitOfWork();
        }

        public void Add(Area model)
        {
            try
            {
                var list = GetAll();

                if (list.Where(x => x.AreaCode == model.AreaCode).Count() > 0)
                    throw new ArgumentException("มีชื่อ area code นี้ซ้ำแล้วในระบบ");

                if (list.Where(x => x.AreaName == model.AreaName).Count() > 0)
                    throw new ArgumentException("มีชื่อ area name นี้ซ้ำแล้วในระบบ");

                model.ModifiedUser = model.CreateUser;
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                _unitOfWork.AreaRepository.Add(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Area model)
        {
            try
            {
                var deleteModel = _unitOfWork.AreaRepository
                    .GetSingle(x => x.AreaCode == model.AreaCode);

                if (deleteModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitOfWork.AreaRepository.Remove(deleteModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Area model)
        {
            try
            {
                if (_unitOfWork.AreaRepository
                    .GetSingle(x => x.AreaCode == model.AreaCode) == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                model.ModifiedDate = DateTime.Now;

                _unitOfWork.AreaRepository.Update(model);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Area> GetAll()
        {
            return _unitOfWork.AreaRepository.Get();
        }
    }
}
