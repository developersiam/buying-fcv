﻿using FCVBuyingBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FCVBuyingWebApp.Controllers
{
    public class NPIBuyingController : Controller
    {
        // GET: NPIBuying
        public ActionResult Index(string stationCode,DateTime buyingDate)
        {
            if (buyingDate == null)
                ViewBag.BuyingDate = DateTime.Now.ToString("yyyy-MM-dd");
            else
                ViewBag.BuyingDate = buyingDate.ToString("yyyy-MM-dd");

            ViewBag.Station = BusinessLayerService.BuyingStationBL().GetSingle(stationCode);
            ViewBag.StationList = BusinessLayerService.BuyingStationBL().GetByArea("4");
            
            return View(BusinessLayerService.BuyingBL()
                .GetByNPIStation(stationCode, buyingDate));
        }
    }
}