﻿using FCVBuyingWebApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FCVBuyingWebApp.Controllers
{
    [Authorize(Roles = "Admin,Buyer")]
    public class BuyingDocumentsController : Controller
    {
        // GET: BuyingDocuments
        public ActionResult Index()
        {
            var list = Helper.BuyingDocumentHelper.GetByTW(user_setting.Crop.Crop1)
                .Where(x => x.RequestFinishStatus == true)
                .OrderByDescending(x => x.FinishStatus)
                .ThenByDescending(x => x.StationCode)
                .ThenByDescending(x => x.BuyingDocumentCode);

            return View(list);
        }
    }
}
