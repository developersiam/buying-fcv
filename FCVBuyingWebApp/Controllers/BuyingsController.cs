﻿using FCVBuyingBL;
using FCVBuyingEntities;
using FCVBuyingWebApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FCVBuyingWebApp.Controllers
{
    [Authorize(Roles = "Admin,Buyer")]
    public class BuyingsController : Controller
    {
        // GET: Buyings
        public ActionResult Index(short crop, string stationCode,
            string farmerCode, short documentNumber)
        {
            ViewBag.BuyingDocument = Helper.BuyingDocumentHelper
                .GetSingle(crop, farmerCode, stationCode, documentNumber);

            return View(Helper.BuyingHelper
                .GetBuyingDetailByDocument(crop,
                farmerCode,
                stationCode,
                documentNumber));
        }

        [HttpPost]
        public ActionResult Index(string status, short crop, string stationCode,
            string farmerCode, short documentNumber)
        {
            if (status == "Finish")
            {
                BusinessLayerService.BuyingDocumentBL()
                    .Finish(new BuyingDocument
                    {
                        Crop = crop,
                        FarmerCode = farmerCode,
                        StationCode = stationCode,
                        DocumentNumber = documentNumber
                    });
            }
            else
            {
                BusinessLayerService.BuyingDocumentBL()
                    .UnFinish(new BuyingDocument
                    {
                        Crop = crop,
                        FarmerCode = farmerCode,
                        StationCode = stationCode,
                        DocumentNumber = documentNumber
                    });
            }

            return RedirectToAction("Index", new
            {
                crop = crop,
                stationCode = stationCode,
                farmerCode = farmerCode,
                documentNumber = documentNumber
            });
        }
    }
}
