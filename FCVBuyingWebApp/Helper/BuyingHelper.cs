﻿using FCVBuyingBL;
using FCVBuyingWebApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCVBuyingWebApp.Helper
{
    public static class BuyingHelper
    {
        public static List<vm_Buying> GetBuyingDetailByDocument(short crop,
            string farmerCode, string stationCode, short documentNumber)
        {
            var list = BusinessLayerService.BuyingBL()
                .GetByDocument(crop, stationCode, farmerCode, documentNumber)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality,
                    ExtensionAgentCode = x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode
                }).ToList();
            return list;
        }

        public static List<vm_Buying> GetBuyingDetailByDate(DateTime documentDate)
        {
            return BusinessLayerService.BuyingBL()
                .GetByDocumentDate(documentDate)
                .Select(x => new vm_Buying
                {
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    StationCode = x.StationCode,
                    DocumentNumber = x.DocumentNumber,
                    BaleBarcode = x.BaleBarcode,
                    BaleTypeName = x.BaleType.BaleTypeName,
                    BaleTypeID = x.BaleTypeID,
                    BaleNumber = x.BaleNumber,
                    Weight = x.Weight,
                    WeightDate = x.WeightDate,
                    WeightUser = x.WeightUser,
                    WeightModifiedDate = x.WeightModifiedDate,
                    WeightModifiedUser = x.WeightModifiedUser,
                    Grade = x.Grade,
                    GradeDate = x.GradeDate,
                    GradeGroupCode = x.GradeGroupCode,
                    GradeModifiedDate = x.GradeModifiedDate,
                    GradeModifiedUser = x.GradeModifiedUser,
                    GradeUser = x.GradeUser,
                    RejectDate = x.RejectDate,
                    RejectType = x.RejectType,
                    RejectTypeID = x.RejectTypeID,
                    RejectTypeName = x.RejectTypeID == null ? null : x.RejectType.RejectTypeName,
                    Price = x.Grade == null ? 0 : x.Weight * x.BuyingGrade.UnitPrice,
                    UnitPrice = x.Grade == null ? 0 :
                    x.FarmerCode.Substring(0, 2) == "CR" || x.FarmerCode.Substring(0, 2) == "PR" ?
                    x.BuyingGrade.UnitPrice1 : x.BuyingGrade.UnitPrice,
                    Quality = x.Grade == null ? null : x.BuyingGrade.Quality,
                    ExtensionAgentCode = x.BuyingDocument.FarmerQuota.Farmer.ExtensionAgentCode
                }).ToList();
        }
    }
}
