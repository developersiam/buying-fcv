﻿using System.Web;
using System.Web.Optimization;

namespace FCVBuyingWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/Site.css"));


            /// Data table bundle.
            /// 
            bundles.Add(new StyleBundle("~/bundles/dataTablescss").Include(
                        //"~/Plugins/DataTables/css/dataTables.foundation.min.css",
                        //"~/Plugins/DataTables/css/dataTables.jqueryui.min.css",
                        //"~/Plugins/DataTables/css/dataTables.semanticui.min.css",
                        "~/Plugins/DataTables/css/jquery.dataTables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/dataTablesjs").Include(
                        //"~/Plugins/DataTables/js/datatables.foundation.min.js",
                        //"~/Plugins/DataTables/js/datatables.jqueryui.min.js",
                        //"~/Plugins/DataTables/js/datatables.semanticui.min.js",
                        "~/Plugins/DataTables/js/jquery.dataTables.min.js"));
        }
    }
}
